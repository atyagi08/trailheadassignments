trigger SequenceNumberManagerOnContact on Contact (after insert, after update, after delete, after undelete) {
    if(TriggerHandler.isFirstTime){                
        
        TriggerHandler.isFirstTime = false;
        
        if(Trigger.isInsert){ TriggerHandler.insertMethod(Trigger.new, Trigger.newMap); }       
        
        else if(Trigger.isUpdate){ TriggerHandler.updateMethod(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap); }       
        
        else if(Trigger.isDelete){ TriggerHandler.deleteMethod(Trigger.old); }
        
        else if(Trigger.isUndelete){ TriggerHandler.undeleteMethod(Trigger.new); }
    } 
}