import { LightningElement, track, wire, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class TestQuickActionController extends NavigationMixin( LightningElement ) {
    @api  recId;
    activeSections = [];
    eventFormTitle = '';
    eventForm = {
        Name : 'EF-14369',
        AccountName : 'Ziker Elementary School',
        City : 'Austin',
        State : 'Texas',
        PreferredStartTrainingDate : '05/11/2020',
        PreferredEndTrainingDate : '05/13/2020',
        Address : '1900 BlueBonnet Lane',
        Zip : '78704'
    };

    week = [];

    getInitData(){
        console.log( "@@@@ IN LWC..." );

        this.eventFormTitle = (this.eventForm.Name + ' - ' + this.eventForm.AccountName + ' - ' + this.eventForm.City + ', ' + this.eventForm.State);

        let curr = new Date;
        let weekList = [];
        for ( let a = 0; a < 7; a++ ) {
            let first = curr.getDate() - curr.getDay() + a; 
            let day = new Date( curr.setDate( first ) );
            weekList.push( day.toString().substring(0,3) + ' ' + ('0'+(day.getMonth()+1)).slice(-2) + '/' + ('0'+day.getDate()).slice(-2) );
        }
        this.week = weekList;
    }
    connectedCallback(){
        this.getInitData();
        // this.spinnerVisible = true;
    }
}