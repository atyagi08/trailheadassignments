import { LightningElement, api, track } from 'lwc';

export default class ClientSidePaginationChild extends LightningElement {
    @api recordsTitle;
    @api clmHeaders = [];
    @api recordsList = [];
    @api disPreButtons;
    @api disNxtButtons;
    @api pageOptions = [];
    
    pageSize = '10';

    createRecord(){
        this.dispatchEvent( new CustomEvent( 'createnewrecord' ) );
    }

    editRecord( event ){
        let rcrdId = event.target.id;
        rcrdId     = rcrdId.split( '-' )[0];
        this.dispatchEvent( new CustomEvent( 'editrecord', 
            {
                detail : rcrdId
            }
        ) );
    }

    deleteRecord( event ){
        let rcrdId = event.target.id;
        rcrdId     = rcrdId.split( '-' )[0];
        this.dispatchEvent( new CustomEvent( 'deleterecord', 
            {
                detail : {
                    recordId: rcrdId,
                    pageSize: this.pageSize
                }
            }
        ) );
    }

    handlePageSize( event ){
        this.pageSize = event.detail.value;
        this.dispatchEvent( new CustomEvent( 'changepagesize',
            {
                detail : this.pageSize
            } 
        ) );
    }

    handlePage( event ){
        this.dispatchEvent( new CustomEvent( 'pagechange',
            {
                detail : {
                    buttonLabel : event.target.label,
                    pageSize : this.pageSize
                }
            }
        ) );
    }
}