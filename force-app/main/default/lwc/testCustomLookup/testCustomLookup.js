import { LightningElement, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import objectList   from '@salesforce/apex/TestCustomLookupController.getObjectList'
import fieldsList   from '@salesforce/apex/TestCustomLookupController.getFieldsList'

export default class TestCustomLookupController extends NavigationMixin( LightningElement ) {
    
    selectedObject;
    objectList     = [];
    selectedField;
    @track filedList = [];
    
    spinnerVisible = false;

    getInitData(){
        objectList({
        })
        .then( result => {
            console.log( '@@@@ Result Object => ', result );
            this.objectList      = this.sortObjectArray( result, 'label' );
            this.spinnerVisible  = false;
        })
        .catch( error => {
            this.spinnerVisible = false;
        })
    }
    connectedCallback(){
        this.getInitData();
        this.spinnerVisible = true;
    }

    hanlechange( event ){
        this.spinnerVisible = true;
        this.selectedField  = '';
        this.selectedObject = event.target.value;

        fieldsList({
            objectName : this.selectedObject
        })
        .then( result => {
            console.log( '@@@@ Result Fields => ', result );
            this.filedList        = this.sortObjectArray( result, 'label' );
            this.spinnerVisible   = false;
        })
        .catch( error => {
            this.spinnerVisible = false;
        })
    }

    handlechangeFields( event ){
        this.selectedField = event.target.value;
        console.log( "@@@@ Selected Field ==>> ", this.selectedField );
    }


    sortObjectArray( objectArray, attribute ){
        objectArray.sort( function( a, b ) {
            var elemA = a[ attribute ];
            var elemB = b[ attribute ];
            if ( typeof a[ attribute ] === String ){
                elemA = elemA.toUpperCase();
                elemB = elemB.toUpperCase();
            }
            var res = ( elemA < elemB ) ? -1 : ( elemA > elemB ) ? 1 : 0;
            return res;
        });
        // console.log( '@@@@Sorted Array ==>> ', objectArray );
        return objectArray;
    }

    showToast( msg, vrnt) {
        console.log( 'Console 2' );
        this.dispatchEvent( new ShowToastEvent( 
            {
                title: 'Hello',
                message: msg,
                variant: vrnt,
            }
        ));
    }
}