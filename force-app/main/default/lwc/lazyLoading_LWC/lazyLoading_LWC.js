import { LightningElement, track } from 'lwc';

import objectList   from '@salesforce/apex/LazyLoading_LWC_Controller.getObjectList'
import fieldsList   from '@salesforce/apex/LazyLoading_LWC_Controller.getFieldsList'
import recordsList  from '@salesforce/apex/LazyLoading_LWC_Controller.recordsList'
import loadMoreRecords  from '@salesforce/apex/LazyLoading_LWC_Controller.LoadRecords'

export default class LazyLoading_LWC_Controller extends LightningElement {
    
    selectedObject;
    objectList         = [];
    spinnerVisible     = false;
    fieldListVisible   = false;
    tableVisible       = false;
    lastRecordId       = "";
    sortField          = "";    
    sortDirection      = "asc";
    totalNumberOfRows  = 0;
    
    @track filedList        = [];
    @track selectedFields   = [];
    @track columns          = [];
    @track data             = [];

    getInitData(){

        objectList({
        })
        .then( result => {
            // console.log( '@@@@ Result Object => ', result );
            this.objectList      = this.sortObjectArray( result, 'label', "ASC" );
            this.spinnerVisible  = false;
        })
        .catch( error => {
            this.spinnerVisible = false;
        })
    }
    connectedCallback(){
        this.getInitData();
        this.spinnerVisible = true;
    }

    hanlechange( event ){
        console.log( "@@@@ hanlechange Called... " );

        this.spinnerVisible   = true;
        this.tableVisible     = false;
        this.selectedFields   = [];
        this.selectedObject   = event.target.value;

        fieldsList({
            objectName : this.selectedObject
        })
        .then( result => {
            // console.log( '@@@@ Result Fields => ', result );
            this.filedList        = this.sortObjectArray( result, 'label', "ASC" );
            this.fieldListVisible = true;
            this.spinnerVisible   = false;
        })
        .catch( error => {
            this.spinnerVisible = false;
        })
    }

    handlechangeFields( event ){
        console.log( "@@@@ handlechangeFields Called... " );
        this.selectedFields = event.detail.value;
    }

    processRecords(){
        console.log( "@@@@ processRecords Called... " );

        if( this.selectedFields.length != 0 ){
            this.spinnerVisible = true;
            this.tableVisible   = true;
            var queryString     = this.selectedFields.join( ', ' );

            recordsList({
                objectName : this.selectedObject,   
                fields : queryString
            })
            .then( result => {
                this.data = result.recordsList;
                // console.log( "@@@@ Records ==>> ", this.data );
                this.lastRecordId       = this.data[ this.data.length - 1 ].Id;
                this.totalNumberOfRows  = result.count;
                this.columns            = result.columnsList;
                this.spinnerVisible     = false;
            })
            .catch( error => {
                this.spinnerVisible = false;
            })
        } else{
            console.log( "Error" );
            // this.showToast( 'Select at least one field', 'error'  );
        }
    }

    sortObjectArray( objectArray, attribute, order ){
        console.log( "@@@@ sortObjectArray Called... " );

        objectArray.sort( function( a, b ) {
            var elemA = a[ attribute ];
            // console.log( "@@@ elemA ==>> ", elemA );
            var elemB = b[ attribute ];
            // console.log( "@@@ elemB ==>> ", elemB );
            if ( typeof a[ attribute ] === String ){
                elemA = elemA.toUpperCase();
                elemB = elemB.toUpperCase();
            }
            var res = ( elemA < elemB ) ? -1 : ( elemA > elemB ) ? 1 : 0;
            res = ( order == "DESC" ) ? res * -1 : res;
            return res;
        });
        // console.log( '@@@@Sorted Array ==>> ', objectArray );
        return objectArray;
    }

    loadMoreData( event ){
        console.log( "@@@@ loadMoreData Called... " );

        var dataTable       = event.target;
        dataTable.isLoading = true
        this.loadMoreStatus = 'Loading';

        loadMoreRecords({
            objectName  : this.selectedObject,
            fields      : this.selectedFields.join( ', ' ),
            recordId    : this.lastRecordId
        })
        .then( result => {
            
            this.lastRecordId  = result[ result.length - 1 ].Id;
            var resultantArray = this.data.concat( result );
            var srtFld         = ( this.sortField ) ?  this.sortField : "Id";
            this.data          = this.sortObjectArray( resultantArray, srtFld, this.sortDirection );

            if ( this.data.length >= this.totalNumberOfRows ) {
                this.loadMoreStatus = 'No more data to load';
                dataTable.enableInfiniteLoading = false;
            } else {
                this.loadMoreStatus = '';
            }
            dataTable.isLoading  = false;
        })
        .catch( error => {
            console.log('-------error-------------'+error);
            console.log(error);
        })
    }

    onHandleSort( event ){
        console.log( "@@@@ onHandleSort Called... " );

        // var srtFld = event.detail.fieldName;
        // var srtOdr = event.detail.sortDirection;
        // if( srtFld == this.sortField ){
        //     this.sortDirection = ( srtOdr == "asc" ) ? "DESC" : "ASC";
        // } else{
        //     this.sortField      = srtFld;
        //     this.sortDirection  = "DESC"; 
        // }

        // console.log( "@@@@ this.sortField ==>> ", this.sortField );
        // console.log( "@@@@ this.sortDirection ==>> ", this.sortDirection );
        // var sortedArray = this.sortObjectArray( this.data, this.sortField, this.sortDirection );
        // this.data       = sortedArray;
        // console.log( "@@@@ this.data ==>> ", this.data );

        const { fieldName: sortedBy, sortDirection } = event.detail;
        const cloneData = [...this.data];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.data = cloneData;
        this.sortDirection = sortDirection;
        this.sortField = sortedBy;
    }

    sortBy(field, reverse, primer) {
        const key = primer
            ? function(x) {
                return primer(x[field]);
            }
            : function(x) {
                return x[field];
            };

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }
}