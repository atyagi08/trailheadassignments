import { LightningElement, track } from 'lwc';

export default class HelloWorld extends LightningElement {

    @track greeting = 'World';
    contacts = [
        {   
            Id : '87467233',
            Name : 'Akhil Tyagi',
            Title : 'hello'
        },
        {
            Id : '87467234',
            Name : 'Shiv Tyagi',
            Title : 'hello'
        },
        {
            Id : '87467235',
            Name : 'Tarun Tyagi',
            Title : 'hello'
        },
        {
            Id : '87467236',
            Name : 'Ankur Tyagi',
            Title : 'hello'
        }
    ];
    changeHandler(event) {
        this.greeting = event.target.value;
    }
}