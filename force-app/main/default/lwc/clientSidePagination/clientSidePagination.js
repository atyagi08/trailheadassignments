import { LightningElement, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import objectList   from '@salesforce/apex/ClientSidePagination.getObjectList'
import fieldsList   from '@salesforce/apex/ClientSidePagination.getFieldsList'
import recordsList  from '@salesforce/apex/ClientSidePagination.recordsList'

export default class ClientSidePagination extends NavigationMixin( LightningElement ) {
    
    selectedObject;
    recordsCardTitle;
    objectList         = [];
    pageSizeOptions    = [];
    pageSizeValue      = '10';
    sObjectRecordsList = [];
    spinnerVisible     = false;
    fieldListVisible   = false;
    tableVisible       = false;
    disPreButtons      = true;
    disNxtButtons      = false;
    
    @track filedList       = [];
    @track selectedFields  = [];
    @track columnHeaders   = [];
    @track showRecordsList = [];
    
    getInitData(){
        objectList({
        })
        .then( result => {
            console.log( '@@@@ Result Object => ', result );
            this.objectList      = this.sortObjectArray( result.objectList, 'label' );
            this.pageSizeOptions = result.pageSizeOptions;
            this.spinnerVisible  = false;
        })
        .catch( error => {
            this.spinnerVisible = false;
        })
    }
    connectedCallback(){
        this.getInitData();
        this.spinnerVisible = true;
    }

    hanlechange( event ){
        this.spinnerVisible   = true;
        this.tableVisible     = false;
        this.selectedFields   = [];
        this.selectedObject   = event.target.value;
        var cardTitle         = this.objectList.filter( rec => rec.value === this.selectedObject )[0].label;
        this.recordsCardTitle = cardTitle + ' Records';

        fieldsList({
            objectName : this.selectedObject
        })
        .then( result => {
            console.log( '@@@@ Result Fields => ', result );
            this.filedList        = this.sortObjectArray( result, 'label' );
            this.fieldListVisible = true;
            this.spinnerVisible   = false;
        })
        .catch( error => {
            this.spinnerVisible = false;
        })
    }

    handlechangeFields( event ){
        this.selectedFields = event.detail.value;
        console.log( "@@@@ Selected Fields ==>> ", this.selectedFields );
    }

    processRecords(){
        if( this.selectedFields.length != 0 ){
            this.spinnerVisible = true;
            this.tableVisible   = true;
            var queryString     = this.selectedFields.join( ', ' );

            recordsList({
                objectName : this.selectedObject,   
                fields : queryString
            })
            .then( result => {
                this.sObjectRecordsList = result;
                for( var a = 0; a < this.sObjectRecordsList.length; a++ ){
                    var values = [];
                    for( var b = 0; b < this.selectedFields.length; b++ ){
                        values.push( this.sObjectRecordsList[a][ this.selectedFields[b] ] );
                    }
                    this.sObjectRecordsList[a].values = values;
                }
                
                var columns = [];
                for( var a = 0; a < this.selectedFields.length; a++ ){
                    var label = this.filedList.filter( rec => rec.value === this.selectedFields[a] );
                    columns.push( label[0] ); 
                }
                this.columnHeaders = columns;
    
                var strtIndx = 0;
                var lastIndx = ( this.sObjectRecordsList.length < parseInt( this.pageSizeValue ) ) ? 
                            this.sObjectRecordsList.length : 
                            parseInt( this.pageSizeValue );
                this.displayRecords( strtIndx, lastIndx );
            })
            .catch( error => {
                this.spinnerVisible = false;
            })
        } else{
            console.log( 'Console 1' );
            this.showToast( 'Select at least one field', 'error'  );
        }
    }

    handlePageSize( event ){
        this.spinnerVisible = true;

        var strtIndx = 0;
        var lastIndx = ( this.sObjectRecordsList.length < parseInt( event.detail ) ) ? 
                    this.sObjectRecordsList.length : 
                    parseInt( event.detail );
        this.displayRecords( strtIndx, lastIndx );
    }

    handlePage( event ){
        var strtIndx;
        var lastIndx;
        var rcrdId;
        
        this.spinnerVisible = true;
        var buttonLabel     = event.detail.buttonLabel;
        var pageSize        = event.detail.pageSize;
        if( buttonLabel === 'First' ){
            strtIndx = 0;
            lastIndx = ( this.sObjectRecordsList.length < parseInt( pageSize ) ) ? this.sObjectRecordsList.length : parseInt( pageSize );
        }
        if( buttonLabel === 'Previous' ){
            rcrdId   = this.showRecordsList[0].Id;
            lastIndx = this.sObjectRecordsList.findIndex( rec => rec.Id === rcrdId );
            strtIndx = lastIndx - parseInt( pageSize );
        }
        if( buttonLabel === 'Next' ){
            rcrdId   = this.showRecordsList[ this.showRecordsList.length - 1 ].Id;
            strtIndx = this.sObjectRecordsList.findIndex( rec => rec.Id === rcrdId ) + 1;
            lastIndx = ( this.sObjectRecordsList.length > strtIndx + parseInt( pageSize ) ) ? 
                        parseInt( pageSize ) + strtIndx : 
                        this.sObjectRecordsList.length;
        }
        if( buttonLabel === 'Last' ){
            strtIndx = this.sObjectRecordsList.length % parseInt( pageSize ) === 0 ? 
                        this.sObjectRecordsList.length - parseInt( pageSize ) : 
                        Math.floor( this.sObjectRecordsList.length / parseInt( pageSize ) ) * parseInt( pageSize );
            lastIndx = this.sObjectRecordsList.length;
        }
        this.displayRecords( strtIndx, lastIndx );
    }

    editRecord( event ){
        let navRecordId = event.detail;
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: navRecordId,
                objectApiName: this.selectedObject,
                actionName: 'edit'
            },
        }).then(url => {
            window.open(url);
        });
    }

    createNewRecord(){
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: this.selectedObject,
                actionName: 'new'
            }
        }).then(url => {
            window.open( url );
        });
    }

    deleteRecord( event ){
        this.spinnerVisible = true;
        let navRecordId     = event.detail.recordId;
        let pageSize        = event.detail.pageSize;

        deleteRecord({
            recordId : navRecordId,
            objectName : this.selectedObject
        })
        .then( result => {
            // console.log( '@@@@ Delete Record Result ==>> ', result );
            var strtId   = this.showRecordsList[0].Id;
            var strtIndx = this.sObjectRecordsList.findIndex( rec => rec.Id === strtId );
            var lastId   = this.showRecordsList[ this.showRecordsList.length - 1 ].Id;
            var lastIndx = this.sObjectRecordsList.findIndex( rec => rec.Id === lastId );
            var rcrdIndx = this.sObjectRecordsList.findIndex( rec => rec.Id === navRecordId );
            this.sObjectRecordsList.splice( rcrdIndx, 1 );
            strtIndx = ( strtIndx === this.sObjectRecordsList.length ) ? strtIndx - parseInt( pageSize ) : strtIndx;
            lastIndx = ( lastIndx === this.sObjectRecordsList.length ) ? lastIndx : lastIndx + 1;
            this.displayRecords( strtIndx, lastIndx );
        })
        .catch( error => {
            console.log( '@@@@ Delete Record Error ==>> ', error );
            this.spinnerVisible = false;
        })
    }

    displayRecords( strtIndx, lastIndx ){
        // console.log( '@@@@ Start Index ==>> ', strtIndx );
        // console.log( '@@@@ Last Index ==>> ', lastIndx );
        this.showRecordsList = [];
        for( var a = strtIndx; a < lastIndx; a++ ){
            this.showRecordsList.push( this.sObjectRecordsList[a] );
        }
        this.disPreButtons  = strtIndx === 0 ? true : false;
        this.disNxtButtons  = lastIndx === this.sObjectRecordsList.length ? true : false;
        this.spinnerVisible = false;
    }

    sortObjectArray( objectArray, attribute ){
        objectArray.sort( function( a, b ) {
            var elemA = a[ attribute ];
            var elemB = b[ attribute ];
            if ( typeof a[ attribute ] === String ){
                elemA = elemA.toUpperCase();
                elemB = elemB.toUpperCase();
            }
            var res = ( elemA < elemB ) ? -1 : ( elemA > elemB ) ? 1 : 0;
            return res;
        });
        // console.log( '@@@@Sorted Array ==>> ', objectArray );
        return objectArray;
    }

    showToast( msg, vrnt) {
        console.log( 'Console 2' );
        this.dispatchEvent( new ShowToastEvent( 
            {
                title: 'Hello',
                message: msg,
                variant: vrnt,
            }
        ));
    }
}