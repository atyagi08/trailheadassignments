public class PaginationWrap {
	public Boolean checked {get;set;} 
    public sObject obj {get;set;}
    public Decimal quantity {get;set;}
    public PaginationWrap(sObject o){
        obj = o;
        checked = false; 
        quantity = 0;
    }
}