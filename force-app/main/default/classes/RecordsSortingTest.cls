@isTest
public class RecordsSortingTest {

    @isTest(seeAllData = true)
    static void startPageName(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String recordId = '';
        String fieldValue = '';
        String order = 'ASC';
        String buttonLabel = 'First';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }

    @isTest(seeAllData = true)
    static void changePageSizeNameASC(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String recordId = '';
        String fieldValue = '';
        String order = 'ASC';
        String buttonLabel = 'First';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void changePageSizeAccountNumberASC(){
        Integer pageSize = 10;
        String fieldName = 'AccountNumber';
        String recordId = '';
        String fieldValue = '';
        String order = 'ASC';
        String buttonLabel = 'First';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void changePageSizeNameDESC(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String recordId = '';
        String fieldValue = '';
        String order = 'DESC';
        String buttonLabel = 'First';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void changePageSizeAccountNumberDESC(){
        Integer pageSize = 10;
        String fieldName = 'AccountNumber';
        String recordId = '';
        String fieldValue = '';
        String order = 'DESC';
        String buttonLabel = 'First';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }

    @isTest(seeAllData = true)
    static void firstPageNameASC(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String recordId = '';
        String fieldValue = '';
        String order = 'ASC';
        String buttonLabel = 'First';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void firstPageAccountNumberASC(){
        Integer pageSize = 10;
        String fieldName = 'AccountNumber';
        String recordId = '';
        String fieldValue = '';
        String order = 'ASC';
        String buttonLabel = 'First';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void firstPageNameDESC(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String recordId = '';
        String fieldValue = '';
        String order = 'DESC';
        String buttonLabel = 'First';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void firstPageAccountNumberDESC(){
        Integer pageSize = 10;
        String fieldName = 'AccountNumber';
        String recordId = '';
        String fieldValue = '';
        String order = 'DESC';
        String buttonLabel = 'First';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }

    @isTest(seeAllData = true)
    static void nextPageNameASC(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String order = 'ASC';
        String buttonLabel = 'Next';
        List<Account> accList1 = [SELECT Id, Name FROM Account WHERE Name = null ORDER BY Name ASC, Id ASC LIMIT :pageSize];
        Integer count = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
        List<Account> accList2 = new List<Account>();
        if(count != 0){
            accList2 = [SELECT Id, Name FROM Account WHERE Name > '' ORDER BY Name ASC, Id ASC LIMIT :count];
        }
        List<Account> accList = new List<Account>();
        for(Account a : accList1){ accList.add(a); }
        for(Account a : accList2){ accList.add(a); }
        String recordId = accList[9].Id;
        String fieldValue = accList[9].Name;
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void nextPageAccountNumberASC(){
        Integer pageSize = 10;
        String fieldName = 'AccountNumber';
        String order = 'ASC';
        String buttonLabel = 'Next';
        List<Account> accList1 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber = null ORDER BY AccountNumber ASC, Id ASC LIMIT :pageSize];
        Integer count = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
        List<Account> accList2 = new List<Account>();
        if(count != 0){
            accList2 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber > '' ORDER BY AccountNumber ASC, Id ASC LIMIT :count];
        }
        List<Account> accList = new List<Account>();
        for(Account a : accList1){ accList.add(a); }
        for(Account a : accList2){ accList.add(a); }
        String recordId = accList[9].Id;
        String fieldValue = accList[9].AccountNumber;
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void nextPageNameDESC(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String order = 'DESC';
        String buttonLabel = 'Next';
        List<Account> accListNotNull = [SELECT Id, Name FROM Account WHERE Name > '' ORDER BY Name DESC, Id DESC LIMIT :pageSize];
        Integer count = pageSize - accListNotNull.size();
        List<Account> accListNull = new List<Account>();
        if(count != 0){
            accListNull = [SELECT Id, Name FROM Account WHERE Name = null ORDER BY Id DESC LIMIT :count];
        }
        List<Account> accList = new List<Account>();
        for(Account a : accListNotNull){ accList.add(a); }
        for(Account a : accListNull){ accList.add(a); }
        String recordId = accList[9].Id;
        String fieldValue = accList[9].Name;
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void nextPageAccountNumberDESC(){
        Integer pageSize = 10;
        String fieldName = 'AccountNumber';
        String order = 'DESC';
        String buttonLabel = 'Next';
        List<Account> accListNotNull = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber > '' ORDER BY AccountNumber DESC, Id DESC LIMIT :pageSize];
        Integer count = pageSize - accListNotNull.size();
        List<Account> accListNull = new List<Account>();
        if(count != 0){
            accListNull = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber = null ORDER BY Id DESC LIMIT :count];
        }
        List<Account> accList = new List<Account>();
        for(Account a : accListNotNull){ accList.add(a); }
        for(Account a : accListNull){ accList.add(a); }
        String recordId = accList[9].Id;
        String fieldValue = accList[9].AccountNumber;
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }

    @isTest(seeAllData = true)
    static void previousPageNameASC(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String order = 'ASC';
        String buttonLabel = 'Previous';
        List<Account> accList1 = [SELECT Id, Name FROM Account WHERE Name = null ORDER BY Name ASC, Id ASC LIMIT :pageSize];
        System.debug('@@@Records 1-10 ==>>');
        Integer count = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
        List<Account> accList2 = new List<Account>();
        if(count != 0){
            accList2 = [SELECT Id, Name FROM Account WHERE Name > '' ORDER BY Name ASC, Id ASC LIMIT :count];
        }
        List<Account> accList = new List<Account>();
        for(Account a : accList1){ accList.add(a); }
        for(Account a : accList2){ accList.add(a); }
        //========================================================================================
        System.debug('@@@Records 11-20 ==>>');
        List<Account> accList3 = new List<Account>();
        List<Account> accList4 = new List<Account>();
        Id checkId = accList[accList.size()-1].Id;
        String accNumber = accList[accList.size()-1].Name;
        accList.clear();
        if(accNumber == null){
            accList1 = [SELECT Id, Name FROM Account WHERE Name = null AND Id > :checkId ORDER BY Name ASC, Id ASC LIMIT :pageSize];
            count = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
            if(count != 0){
                accList2 = [SELECT Id, Name FROM Account WHERE Name > '' ORDER BY Name ASC, Id ASC LIMIT :count];
            }
            for(Account a : accList1){ accList.add(a); }
            for(Account a : accList2){ accList.add(a); }
        }
        else{
            accList3 = [SELECT Id, Name FROM Account WHERE Name = :accNumber AND Id > :checkId ORDER BY Id ASC LIMIT :pageSize];
            count = pageSize - accList3.size();
            accList4 = [SELECT Id, Name FROM Account WHERE Name > :accNumber ORDER BY Name ASC, Id ASC LIMIT :count];
            for(Account a : accList3){ accList.add(a); }
            for(Account a : accList4){ accList.add(a); }
        }
        String recordId = accList[0].Id;
        String fieldValue = accList[0].Name;
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void previousPageAccountNumberASC(){
        Integer pageSize = 10;
        String fieldName = 'AccountNumber';
        String order = 'ASC';
        String buttonLabel = 'Previous';
        List<Account> accList1 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber = null ORDER BY AccountNumber ASC, Id ASC LIMIT :pageSize];
        System.debug('@@@Records 1-10 ==>>');
        Integer count = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
        List<Account> accList2 = new List<Account>();
        if(count != 0){
            accList2 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber > '' ORDER BY AccountNumber ASC, Id ASC LIMIT :count];
        }
        List<Account> accList = new List<Account>();
        for(Account a : accList1){ accList.add(a); }
        for(Account a : accList2){ accList.add(a); }
        //========================================================================================
        System.debug('@@@Records 11-20 ==>>');
        List<Account> accList3 = new List<Account>();
        List<Account> accList4 = new List<Account>();
        Id checkId = accList[accList.size()-1].Id;
        String accNumber = accList[accList.size()-1].AccountNumber;
        accList.clear();
        if(accNumber == null){
            accList1 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber = null AND Id > :checkId ORDER BY AccountNumber ASC, Id ASC LIMIT :pageSize];
            count = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
            if(count != 0){
                accList2 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber > '' ORDER BY AccountNumber ASC, Id ASC LIMIT :count];
            }
            for(Account a : accList1){ accList.add(a); }
            for(Account a : accList2){ accList.add(a); }
        }
        else{
            accList3 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber = :accNumber AND Id > :checkId ORDER BY Id ASC LIMIT :pageSize];
            count = pageSize - accList3.size();
            accList4 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber > :accNumber ORDER BY AccountNumber ASC, Id ASC LIMIT :count];
            for(Account a : accList3){ accList.add(a); }
            for(Account a : accList4){ accList.add(a); }
        }
        String recordId = accList[0].Id;
        String fieldValue = accList[0].AccountNumber;
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void previousPageNameDESC(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String order = 'DESC';
        String buttonLabel = 'Previous';
        List<Account> accListNotNull = [SELECT Id, Name FROM Account WHERE Name > '' ORDER BY Name DESC, Id DESC LIMIT :pageSize];
        Integer count = pageSize - accListNotNull.size();
        List<Account> accListNull = new List<Account>();
        if(count != 0){
            accListNull = [SELECT Id, Name FROM Account WHERE Name = null ORDER BY Id DESC LIMIT :count];
        }
        System.debug('@@@Records 01-10 ==>>');
        List<Account> accList = new List<Account>();
        for(Account a : accListNotNull){ accList.add(a); }
        for(Account a : accListNull){ accList.add(a); }
        //========================================================================================
        System.debug('@@@Records 11-20 ==>>');
        List<Account> accList1 = new List<Account>();
        List<Account> accList2 = new List<Account>();
        List<Account> accList3 = new List<Account>();
        Id checkId = accList[accList.size()-1].Id;
        String accNumber = accList[accList.size()-1].Name;
        accList.clear();
        if(accNumber != null){
            accList1 = [SELECT Id, Name FROM Account WHERE Name = :accNumber AND Id < :checkId ORDER BY Id DESC LIMIT :pageSize];
            count = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
            if(count != 0){
                accList2 = [SELECT Id, Name FROM Account WHERE Name < :accNumber AND Name != null ORDER BY Name DESC, Id DESC LIMIT :count];
            }
            count = pageSize - accList1.size() - accList2.size();
            if(count != 0){
                accList3 = [SELECT Id, Name FROM Account WHERE Name = null ORDER BY Id DESC LIMIT :count];
            }
            for(Account a : accList1){ accList.add(a); }
            for(Account a : accList2){ accList.add(a); }
            for(Account a : accList3){ accList.add(a); }
        }
        else{
            accList1 = [SELECT Id, Name FROM Account WHERE Name = null AND Id < :checkId ORDER BY Id DESC LIMIT :pageSize];
            for(Account a : accList1){ accList.add(a); }
        }
        String recordId = accList[0].Id;
        String fieldValue = accList[0].Name;
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void previousPageAccountNumberDESC(){
        Integer pageSize = 10;
        String fieldName = 'AccountNumber';
        String order = 'DESC';
        String buttonLabel = 'Previous';
        List<Account> accListNotNull = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber > '' ORDER BY AccountNumber DESC, Id DESC LIMIT :pageSize];
        Integer count = pageSize - accListNotNull.size();
        List<Account> accListNull = new List<Account>();
        if(count != 0){
            accListNull = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber = null ORDER BY Id DESC LIMIT :count];
        }
        System.debug('@@@Records 01-10 ==>>');
        List<Account> accList = new List<Account>();
        for(Account a : accListNotNull){ accList.add(a); }
        for(Account a : accListNull){ accList.add(a); }
        //========================================================================================
        System.debug('@@@Records 11-20 ==>>');
        List<Account> accList1 = new List<Account>();
        List<Account> accList2 = new List<Account>();
        List<Account> accList3 = new List<Account>();
        Id checkId = accList[accList.size()-1].Id;
        String accNumber = accList[accList.size()-1].AccountNumber;
        accList.clear();
        if(accNumber != null){
            accList1 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber = :accNumber AND Id < :checkId ORDER BY Id DESC LIMIT :pageSize];
            count = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
            if(count != 0){
                accList2 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber < :accNumber AND AccountNumber != null ORDER BY AccountNumber DESC, Id DESC LIMIT :count];
            }
            count = pageSize - accList1.size() - accList2.size();
            if(count != 0){
                accList3 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber = null ORDER BY Id DESC LIMIT :count];
            }
            for(Account a : accList1){ accList.add(a); }
            for(Account a : accList2){ accList.add(a); }
            for(Account a : accList3){ accList.add(a); }
        }
        else{
            accList1 = [SELECT Id, Name, AccountNumber FROM Account WHERE AccountNumber = null AND Id < :checkId ORDER BY Id DESC LIMIT :pageSize];
            for(Account a : accList1){ accList.add(a); }
        }
        String recordId = accList[0].Id;
        String fieldValue = accList[0].AccountNumber;
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }

    @isTest(seeAllData = true)
    static void lastPageNameASC(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String recordId = '';
        String fieldValue = '';
        String order = 'ASC';
        String buttonLabel = 'Last';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void lastPageAccountNumberASC(){
        Integer pageSize = 10;
        String fieldName = 'AccountNumber';
        String recordId = '';
        String fieldValue = '';
        String order = 'ASC';
        String buttonLabel = 'Last';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void lastPageNameDESC(){
        Integer pageSize = 10;
        String fieldName = 'Name';
        String recordId = '';
        String fieldValue = '';
        String order = 'DESC';
        String buttonLabel = 'Last';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
    @isTest(seeAllData = true)
    static void lastPageAccountNumberDESC(){
        Integer pageSize = 10;
        String fieldName = 'AccountNumber';
        String recordId = '';
        String fieldValue = '';
        String order = 'DESC';
        String buttonLabel = 'Last';
        Test.startTest();
        RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, order, buttonLabel);
        Test.stopTest();
    }
}