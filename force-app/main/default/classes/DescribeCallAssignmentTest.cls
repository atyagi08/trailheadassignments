@isTest
public class DescribeCallAssignmentTest {


    //DescribeCallAssignmentController Class Test Methods----------------------------------------------------------
    
    @isTest
    static void fieldNamesMethodTestNone(){        
        Test.startTest();
            DescribeCallAssignmentController dca = new DescribeCallAssignmentController();
            dca.fieldNamesMethod();
        Test.stopTest();        
    }
    
    @isTest
    static void fieldNamesMethodTestAccount(){        
        Test.startTest();
            DescribeCallAssignmentController dca = new DescribeCallAssignmentController();
            dca.objectName = 'Account';
            dca.fieldNamesMethod();
        Test.stopTest();        
    }
    
    @isTest
    static void pageResetTest(){        
        Test.startTest();
            DescribeCallAssignmentController dca = new DescribeCallAssignmentController();
            dca.pageReset();
        Test.stopTest();        
    }
    
    @isTest
    static void moethodOnAddButtonTest(){        
        Test.startTest();
            DescribeCallAssignmentController dca = new DescribeCallAssignmentController();
            dca.selectedFieldsFromObject.add('name');
            dca.fieldNamesList.add(new SelectOption('name', 'Name'));
            dca.moethodOnAddButton();
        Test.stopTest();        
    }
    @isTest
    static void moethodOnRemoveButtonTest(){        
        Test.startTest();
            DescribeCallAssignmentController dca = new DescribeCallAssignmentController();
            dca.selectedFieldsFromSelect.add('name');
            dca.selectedNamesList.add(new SelectOption('name', 'Name'));
            dca.moethodOnRemoveButton();
        Test.stopTest();        
    }
    
    @isTest
    static void renderMethodTest(){        
        Test.startTest();
            DescribeCallAssignmentController dca = new DescribeCallAssignmentController();
            dca.rendermethod();
        Test.stopTest();        
    }
    
    @isTest
    static void hasSelectedValueTestTrue(){        
        Test.startTest();
            DescribeCallAssignmentController dca = new DescribeCallAssignmentController();
            dca.selectedNamesList.clear();
            System.assertEquals(false, dca.hasSelectedValue);
        Test.stopTest();        
    }
    
    @isTest
    static void hasSelectedValueTestFalse(){        
        Test.startTest();
            DescribeCallAssignmentController dca = new DescribeCallAssignmentController();
            dca.selectedNamesList.add(new SelectOption('name', 'Name'));
            System.assertEquals(true, dca.hasSelectedValue);
        Test.stopTest();        
    }
    
    //Component Controller Class Test Methods----------------------------------------------------------
    
    @isTest
    static void getretrieveRecordsTest0(){
        
        List<SelectOption> fieldNames = new List<SelectOption>();
        fieldNames.add(new SelectOption('name', 'Name'));
        fieldNames.add(new SelectOption('accountnumber', 'Account Number'));
        
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;

        
        List<sObject> obj = Database.query('SELECT name, accountnumber from account where name like \'%\' order by name limit 5 offset 0');
       
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.fieldNamesController = fieldNames;
            cc.checkboxState = new Map<Id, Boolean>();
            cc.numberOfRecords = 5;
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = 0;
            cc.toggleCounter = '0';
            System.assertEquals(obj, cc.getretrieveRecords());
        Test.stopTest();        
    }
    
    @isTest
    static void getretrieveRecordsTest1(){
        
        List<SelectOption> fieldNames = new List<SelectOption>();
        fieldNames.add(new SelectOption('name', 'Name'));
        fieldNames.add(new SelectOption('accountnumber', 'Account Number'));
        
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;

        List<sObject> obj = Database.query('SELECT name, accountnumber from account where name like \'%\' order by name limit 5 offset 0');
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.fieldNamesController = fieldNames;
            cc.checkboxState = new Map<Id, Boolean>();
            cc.numberOfRecords = 5;
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = 0;
            cc.toggleCounter = '1';
            System.assertEquals(obj, cc.getretrieveRecords());
        Test.stopTest();        
    }
    
    @isTest
    static void getretrieveColumnsTest(){
        
       
        List<SelectOption> fieldNames = new List<SelectOption>();
        fieldNames.add(new SelectOption('name', 'Name'));
        fieldNames.add(new SelectOption('accountnumber', 'Account Number'));
        
        List<String> colList = new List<String>();
        colList.add('name');
        colList.add('accountnumber');        
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.fieldNamesController = fieldNames;
            System.assertEquals(colList, cc.getretrieveColumns());
        Test.stopTest();        
    }
    
    @isTest
    static void toggleSortTest(){
        
        
        List<SelectOption> fieldNames = new List<SelectOption>();
        fieldNames.add(new SelectOption('name', 'Name'));
        fieldNames.add(new SelectOption('accountnumber', 'Account Number'));
        
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;

        List<sObject> obj = Database.query('SELECT name, accountnumber from account where name like \'%\' order by name asc limit 5 offset 0');
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.fieldNamesController = fieldNames;
            cc.sortField = 'name';
            cc.recordPerPage = 5;
            cc.offset = 0;
            System.assertEquals(obj, cc.toggleSort());
        Test.stopTest();        
    }
    
    @isTest
    static void newRecordTest(){

        PageReference page = new  PageReference('/001/e');
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            System.assertEquals(page, cc.newRecord());
        Test.stopTest();        
    }
    
    @isTest
    static void deleteSelectedRecordsTest(){        
        
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        
        Set<id> idSet = new Set<Id>();
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
            idSet.add(obj.Id);
        }

        List<sObject> obj = Database.query('SELECT id from account where id in :idSet');
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.checkboxState = accMap;
            cc.deleteSelectedRecords();
        Test.stopTest();      
    }
    
    @isTest
    static void newRecordCSVTest(){

        List<SelectOption> fieldNames = new List<SelectOption>();
        fieldNames.add(new SelectOption('name', 'Name'));
        fieldNames.add(new SelectOption('accountnumber', 'Account Number'));
        String passQuery = '*';
        
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            passQuery += (String.valueOf(obj.Id)+',');
        }
        String str = passQuery;
        passQuery += '**Account***';
        passQuery += 'name, accountnumber****';
        PageReference page = new  PageReference('/apex/csvDownloadPage?records='+passQuery);
       
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.fieldNamesController = fieldNames;
        	cc.passQuery = str;
            System.assertEquals(page, cc.newRecordCSV());
        Test.stopTest();       
    }
    
    @isTest
    static void NumerOfRecordsOnFilterTest(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Test.startTest();
        ComponentController cc = new ComponentController();
       	cc.objectNameController = 'Account';
        cc.sortField = 'name';
        cc.characterForSort = '';
		cc.NumerOfRecordsOnFilter();
        System.assertEquals(5, accList.size());
        Test.stopTest();        
    }
    
    @isTest
    static void headerCheckboxMethodTestTrue(){        
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, false);
        }
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = 0;
            cc.checked = true;
        	cc.checkboxState = accMap;
            cc.passQuery = '*';
			cc.headerCheckboxMethod();
        Test.stopTest();       
    }
    
    @isTest
    static void headerCheckboxMethodTestFalse(){        
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = 0;
            cc.checked = false;
        	cc.checkboxState = accMap;
            cc.passQuery = '*';
			cc.headerCheckboxMethod();
        Test.stopTest();       
    }

    @isTest
    static void checkboxStateMethodTestFalse(){        
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        accMap.remove(a.Id);
        accMap.put(a.Id, false);
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = 0;
            cc.checked = false;
        	cc.recordId = a.Id;
        	cc.checkboxState = accMap;
            cc.passQuery = '*';
			cc.checkboxStateMethod();
        Test.stopTest();        
    }
    
    @isTest
    static void checkboxStateMethodTestTrue(){        
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = 0;
            cc.checked = false;
        	cc.recordId = a.Id;
        	cc.checkboxState = accMap;
            cc.passQuery = '*';
			cc.checkboxStateMethod();
        Test.stopTest();        
    }
    
    @isTest
    static void deleteRecordTest(){
        Account a = new Account(Name = 'Testing');
        insert a;
        Test.startTest();
            ComponentController cc = new ComponentController();            
            cc.objectNameController = 'Account';
            cc.RecordToDelete = a.Id;
            cc.deleteRecord();
        Test.stopTest();        
    }
    
    @isTest
    static void FirstPageTestFalse(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        accMap.remove(a.Id);
        accMap.put(a.Id, false);
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = 0;
        	cc.checkboxState = accMap;
            cc.Firstpage();
        Test.stopTest();        
    }
    
    @isTest
    static void FirstPageTestTrue(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = 0;
        	cc.checkboxState = accMap;
            cc.Firstpage();
        Test.stopTest();        
    }
    
     @isTest
    static void PreviousPageTestFalse(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        accMap.remove(a.Id);
        accMap.put(a.Id, false);
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = 5;
        	cc.checkboxState = accMap;
            cc.Firstpage();
        Test.stopTest();        
    }
    
    @isTest
    static void PreviousPageTestTrue(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = 5;
        	cc.checkboxState = accMap;
            cc.Firstpage();
        Test.stopTest();        
    }
    
     @isTest
    static void NextPageTestFalse(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        accMap.remove(a.Id);
        accMap.put(a.Id, false);
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = -5;
        	cc.checkboxState = accMap;
            cc.Firstpage();
        Test.stopTest();        
    }
    
    @isTest
    static void NextPageTestTrue(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = -5;
        	cc.checkboxState = accMap;
            cc.Firstpage();
        Test.stopTest();        
    }
    
     @isTest
    static void LastPageTestFalse(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        accMap.remove(a.Id);
        accMap.put(a.Id, false);
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = -5;
        	cc.checkboxState = accMap;
            cc.Firstpage();
        Test.stopTest();        
    }
    
    @isTest
    static void LastPageTestTrueMod0(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = -5;
        	cc.numberOfRecords = 5;
        	cc.checkboxState = accMap;
            cc.Firstpage();
        Test.stopTest();        
    }
    
    @isTest
    static void LastPageTestTrueMod1(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        Map<Id, Boolean> accMap = new Map<Id, Boolean>();
        for(sObject obj : Database.query('SELECT Id FROM Account')){
            accMap.put(obj.Id, true);
        }
        
        Account a = [SELECT Id FROM Account LIMIT 1 OFFSET 0];
        
        Test.startTest();
            ComponentController cc = new ComponentController();
            cc.objectNameController = 'Account';
            cc.sortField = 'name';
            cc.characterForSort = '';
            cc.recordPerPage = 5;
            cc.offset = -5;
        	cc.numberOfRecords = 3;
        	cc.checkboxState = accMap;
            cc.Firstpage();
        Test.stopTest();        
    }
    
    @isTest
    static void setOffsetTest(){
        Test.startTest();
        ComponentController cc = new ComponentController();
        cc.setOffset();
        Test.stopTest();        
    }
    
    @isTest
    static void HasPreviousPageTestTrue(){
        Test.startTest();
        ComponentController cc = new ComponentController();
        cc.offset = 0;
        System.assertEquals(true, cc.HasPreviousPage);
        Test.stopTest();        
    }
    
    @isTest
    static void HasPreviousPageTestFalse(){
        Test.startTest();
        ComponentController cc = new ComponentController();
        cc.offset = 5;
        System.assertEquals(false, cc.HasPreviousPage);
        Test.stopTest();        
    }
    
    @isTest
    static void HasNextPageTestTrue(){
        Test.startTest();
        ComponentController cc = new ComponentController();
        cc.offset = 5;
        cc.recordPerPage = 5;
        cc.numberOfRecords = 9;
        System.assertEquals(true, cc.HasPreviousPage);
        Test.stopTest();        
    }
    
    @isTest
    static void HasNextPageTestFalse(){
        Test.startTest();
        ComponentController cc = new ComponentController();
        cc.offset = 5;
        cc.recordPerPage = 5;
        cc.numberOfRecords = 16;
        System.assertEquals(false, cc.HasNextPage);
        Test.stopTest();        
    }
    
    
    
    
    
     
    
    
    
    //csvPageController Class Test Methods----------------------------------------------------------
    
    @isTest
    static void fetchTest(){
        List<Account> accList = new List<Account>();
        String allIds = '';
        String objectName = 'Account';
        String allFields = 'name, accountnumber';
        for(Integer i=1; i<6; i++){
            Account a = new Account(Name = ('Testing '+i) );
            accList.add(a);
        }
        insert accList;
        
        for(sObject obj : Database.query('SELECt Id FROM Account')){
            allIds += (String.valueOf(obj.Id)+',');
        }        
        
        Test.startTest();
        csvPageController csv = new csvPageController();
        csv.query = ('*' + allIds + '**' + objectName + '***' + allFields + '****');
        System.assertEquals(null, csv.fetch());
        Test.stopTest();        
    }
	
}