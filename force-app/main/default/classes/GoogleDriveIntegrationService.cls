public with sharing class GoogleDriveIntegrationService {

    //Method to return access token--------------------------------------------------------------------------
    public static String AccessToken(String code){
        Integration_Tokens__c itc = [SELECT Access_Token__c, Refresh_Token__c, Expire_Time__c FROM Integration_Tokens__c WHERE Name__c = 'GDrive'];
        String accessToken        = itc.Access_Token__c;
        String refreshToken       = itc.Refresh_Token__c;
        Integration_Key__mdt keys = [SELECT Client_Id__c, Client_Secret__c, Redirect_URl__c FROM Integration_Key__mdt WHERE MasterLabel = 'GDrive Keys'];
        String clientId           = keys.Client_Id__c;
        String clientSecret       = keys.Client_Secret__c;
        String redirectURI        = keys.Redirect_URl__c;
        GoogleDriveIntegrationWrapper.WrapperToken token;

        if(itc.Access_Token__c !=null && System.now() < itc.Expire_Time__c){            //When accesss token is not expired
            // System.debug('@@@Time Not Expired');
            return String.valueOf(accessToken);
        }
        else if(itc.Access_Token__c !=null){                                            //When access token is expired
            // System.debug('@@@Time Expired');
            String messageBody = 'refresh_token='+refreshToken+'&client_id='+clientId+'&client_secret='+clientSecret+'&grant_type=refresh_token';
            String endpoint = 'https://www.googleapis.com/oauth2/v4/token';
            HttpRequest request = new HttpRequest();
            request.setMethod('POST');
            request.setEndpoint(endpoint);
            request.setHeader('Content-type', 'application/x-www-form-urlencoded');
            request.setHeader('Content-length', String.valueOf(messageBody.length()));
            request.setBody(messageBody);
            request.setTimeout(60*1000);
            HttpResponse response = new Http().send(request);
            // System.debug('@@@Access Token after expire time === \n' + response.getBody());
            token = (GoogleDriveIntegrationWrapper.WrapperToken)JSON.deserialize(response.getBody(), GoogleDriveIntegrationWrapper.WrapperToken.class);
            itc.Access_Token__c = token.access_token;
            itc.Expire_Time__c = System.now().addSeconds(token.expires_in);
            accessToken = String.valueOf(token.access_token);
            update itc;
            return accessToken;
        }

        String messageBody = 'code='+code+'&client_id='+clientId+'&client_secret='+clientSecret+'&redirect_uri='+redirectURI+'&grant_type=authorization_code';
        HttpRequest request = new HttpRequest();                                        //First authorization without access token
        request.setMethod('POST');
        request.setEndpoint('https://accounts.google.com/o/oauth2/token');
        request.setHeader('Content-type', 'application/x-www-form-urlencoded');
        request.setHeader('Content-length', String.valueOf(messageBody.length()));
        request.setBody(messageBody);
        request.setTimeout(60*1000);
        HttpResponse response = new Http().send(request);
        // System.debug('@@@Access Token on first authorization === \n' + response.getBody());
        token = (GoogleDriveIntegrationWrapper.WrapperToken)JSON.deserialize(response.getBody(), GoogleDriveIntegrationWrapper.WrapperToken.class);
        itc.Access_Token__c = token.access_token;
        itc.Refresh_Token__c = token.refresh_token;        
        itc.Expire_Time__c = System.now().addSeconds(token.expires_in);
        accessToken = String.valueOf(token.access_token);
        update itc;
        return accessToken;
    }

    //Method for GET Requests--------------------------------------------------------------------------------
    public Static GoogleDriveIntegrationWrapper.WrapperWhole GetResponse(String endpoint, String accToken){
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Accept', 'application/json');
        HttpResponse response = new Http().send(request);
        // System.debug('@@@Service Class GetResponse Method === \n'+response.getBody());
        GoogleDriveIntegrationWrapper.WrapperWhole wrapperinstance = (GoogleDriveIntegrationWrapper.WrapperWhole)JSON.deserialize(response.getBody(), GoogleDriveIntegrationWrapper.WrapperWhole.class);
        return wrapperinstance;
    }

    //Method for POST Requests-------------------------------------------------------------------------------
    public Static GoogleDriveIntegrationWrapper.WrapperFileList PostResponse(String endpoint, String accToken, String contentType, String body){
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Content-type', contentType);
        request.setHeader('Content-length', String.valueOf(body.length()));
        request.setBody(body);
        request.setTimeout(60 * 1000);  
        HttpResponse response = new Http().send(request);
        // System.debug('@@@Service Class PostResponse Method === \n'+response.getBody());
        GoogleDriveIntegrationWrapper.WrapperFileList wrapperinstance = (GoogleDriveIntegrationWrapper.WrapperFileList)JSON.deserialize(response.getBody(), GoogleDriveIntegrationWrapper.WrapperFileList.class);
        return wrapperinstance;
    }

    //Method for DELETE Requests-----------------------------------------------------------------------------
    public Static Integer DeleteResponse(String endpoint, String accToken){
        HttpRequest request = new HttpRequest();
        request.setMethod('DELETE');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Accept', 'application/json');
        HttpResponse response = new Http().send(request);
        // System.debug('@@@Service Class DeleteResponse Method === \n'+response.getBody());
        return response.getStatusCode();
    }
}