/**
**@Description: Apex Controller Class for communityGoogleDrive Component
**
**@Author: Akhil Tyagi
**@Date: 07 January 2020
**
**@ChangeLog:
**15 January 2020: Created FileUpload Method
***/

public class commGoogleDriveController {

    /***************************************************************************************************************
    **@Description: Generate Access Token or Authorization URL based on condition.                                **
    **                                                                                                            **
    **@Parameter:                                                                                                 **
    **                                                                                                            **
    **@Return String with access token or authorization URL                                                       **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String getAccessToken(){
        String accessToken = commGoogleDriveService.AccessToken();
        System.debug('@@@Access Token ==>> ' + accessToken);
        if(accessToken != 'AuthorizationRequired') {
            return accessToken;
        }
        User usr                          = [SELECT Username FROM User WHERE Id = :UserInfo.getUserId()];
        CommunityIntegrationKey__mdt keys = [SELECT Client_Id__c, Redirect_URl__c FROM CommunityIntegrationKey__mdt WHERE MasterLabel = :usr.Username AND Cloud_Name__c = 'GoogleDrive'];
        String client_id                  = EncodingUtil.urlEncode(keys.Client_Id__c,'UTF-8');
        String redirect_uri               = EncodingUtil.urlEncode(keys.Redirect_URl__c,'UTF-8');
        String pageUrl                    = 'https://accounts.google.com/o/oauth2/auth?'+
        'client_id='+client_id+
        '&response_type=code'+
        '&scope=https://www.googleapis.com/auth/drive'+
        '&redirect_uri='+redirect_uri+
        '&state=security_token%3D138r5719ru3e1%26url%3Dhttps://oa2cb.example.com/myHome/google&'+
        'access_type=offline';
        return pageUrl;
    }

    /***************************************************************************************************************
    **@Description: Generate Access Token for first time authorization.                                           **
    **                                                                                                            **
    **@Parameter code: Code used to generate access token                                                         **
    **                                                                                                            **
    **@Return String with access token                                                                            **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String AuthorizeUser(String code) {
        System.debug('@@@Code ==>> ' + code);
        return commGoogleDriveService.AuthUser(code);
    }

    /***************************************************************************************************************
    **@Description: Generate List of file and folders in current directory.                                       **
    **                                                                                                            **
    **@Parameter:                                                                                                 **
    **                                                                                                            **
    **@Return JSON String with list of file and folder in root directory                                          **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String RecordsList(){
        System.debug('@@@RecordsList Method @@@');
        String accToken = getAccessToken();
        // System.debug('@@@AccessToken RecordList Method ==>> \n' + accToken);
        String endpoint = 'https://www.googleapis.com/drive/v3/files?q=\'root\'+in+parents&orderBy=folder';
        commGoogleDriveWrapper.WrapperWhole wrapperinstance = commGoogleDriveService.GetResponse(endpoint, accToken);
        System.debug('@@@Wrapperinstance RecordList Method ==>> \n' + wrapperinstance);
        return JSON.serialize(wrapperinstance);
    }

    /***************************************************************************************************************
    **@Description: Generate List of file and folders in clicked folder and get file download link.               **
    **                                                                                                            **
    **@Parameter fileId: Id of the clicked file/folder.                                                           **
    **@Parameter fileType: MimeType of the clicked file/folder.                                                   **
    **                                                                                                            **
    **@Return JSON String with list of file and folder in current directory or download link of file.             **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String ClickOnFileFolder(String fileId, String fileType) {
        System.debug('@@@ClickOnFileFolder Method @@@');
        String accToken = getAccessToken();
        // System.debug('@@@AccessToken ClickOnFileFolder Method ==>> \n' + accToken);
        commGoogleDriveWrapper.WrapperWhole wrapperinstance = new commGoogleDriveWrapper.WrapperWhole();
        if(fileType.equals('application/vnd.google-apps.folder')){                     //Open folder
            String endpoint = 'https://www.googleapis.com/drive/v3/files?q=\''+fileId+'\'+in+parents&orderBy=folder';
            wrapperinstance = commGoogleDriveService.GetResponse(endpoint, accToken);
            // System.debug('@@@Wrapperinstance ClickOnFileFolder Method ==>> \n' + wrapperinstance);
        }
        else{                                                                                   //Download file
            String endpoint = 'https://www.googleapis.com/drive/v3/files/'+fileId+'?fields=webContentLink';
            wrapperinstance = commGoogleDriveService.GetResponse(endpoint, accToken);
            // System.debug('@@@Wrapperinstance ClickOnFileFolder Method ==>> \n' + wrapperinstance);        
        }
        return JSON.serialize(wrapperinstance);
    }

    /***************************************************************************************************************
    **@Description: Delete the file or folder.                                                                    **
    **                                                                                                            **
    **@Parameter fileId: Id of the file/folder to be deleted.                                                     **
    **                                                                                                            **
    **@Return String to state wether the file/folder is deleted or not.                                           **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String DeleteFile(String fileId) {
        System.debug('@@@DeleteFile Method @@@');
        String accToken = getAccessToken();
        // System.debug('@@@AccessToken DeleteFile Method ==>> \n' + accToken);
        String endpoint = 'https://www.googleapis.com/drive/v3/files/'+fileId;
        Integer statusCode = commGoogleDriveService.DeleteResponse(endpoint, accToken);
        if(statusCode == 204){
            return 'Successfully Deleted';
        }
        return 'File not deleted successfully';
    }

    /***************************************************************************************************************
    **@Description: Upload file to current directory(folder).                                                     **
    **                                                                                                            **
    **@Parameter fileBody: Body of the file.                                                                      **
    **@Parameter fileName: Name of the file.                                                                      **
    **@Parameter fileType: MimeType of the file.                                                                  **
    **@Parameter dirId: Id of the current directory(folder).                                                      **
    **                                                                                                            **
    **@Return JSON String with details of the uploaded file.                                                      **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String FileUpload(String fileName, String fileBody, String fileType, String dirId) {
        System.debug('@@@FileUpload Method @@@');
        String accToken    = getAccessToken();
        // System.debug('@@@AccessToken FileUpload Method ==>> \n' + accToken);
        String boundary    = '----------9889464542212';
        String delimiter   = '\r\n--' + boundary + '\r\n';
        String close_delim = '\r\n--' + boundary + '--';
        String bodyEncoded = fileBody;
        String endpoint    = 'https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart';
        String contentType = 'multipart/mixed; boundary=\"' + boundary + '\"';
        String body        = '';
        
        body = delimiter + 
        'Content-Type: application/json\r\n\r\n' +
        '{\"name\": \"' + fileName + '\",' + '\"mimeType\": \"' + fileType + '\",' + '\"parents\":[\"'+ dirId +'\"]}' +
        delimiter +
        'Content-Type: ' + fileType + '\r\n' +
        'Content-Transfer-Encoding: base64\r\n' + '\r\n' +
        bodyEncoded +
        close_delim;

        commGoogleDriveWrapper.WrapperFileList wrapper = commGoogleDriveService.PostResponse(endpoint, accToken, contentType, body);
        // System.debug('@@@Wrapper FileUpload Method ==>> \n' + wrapper);
        return JSON.serialize(wrapper);
    }

    /***************************************************************************************************************
    **@Description: Create folder in current directory(folder).                                                   **
    **                                                                                                            **
    **@Parameter folderName: Name of the folder to be created.                                                    **
    **@Parameter dirId: Id of the current directory(folder).                                                      **
    **                                                                                                            **
    **@Return JSON String with details of the created folder.                                                     **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String CreateFolder(String folderName, String dirId){
        System.debug('@@@CreateFolder Method @@@');
        String accToken    = getAccessToken();
        // System.debug('@@@AccessToken CreateFolder Method ==>> \n' + accToken);
        String endpoint    = 'https://www.googleapis.com/drive/v3/files';
        String contentType = 'application/json';
        String body = '{\"mimeType\": \"application/vnd.google-apps.folder\",\"name\": \"'+folderName+'\",\"parents\": [\"'+dirId+'\"]}';
        commGoogleDriveWrapper.WrapperFileList wrapper = commGoogleDriveService.PostResponse(endpoint, accToken, contentType, body);
        // System.debug('@@@Wrapper CreateFolder Method ==>> \n' + wrapper);
        return JSON.serialize(wrapper);
    }
}