/**
**@Description: Apex Service Class for integration with Dropbox
**
**@Author: Akhil Tyagi
**@Date: 16 January 2020
**
**@ChangeLog:
**20 January 2020: Created UploadFile Method.
***/

public class commDropboxService {

    /***************************************************************************************************************
    **@Description: Query Access Token from custom setting if available.                                          **
    **                                                                                                            **
    **@Parameter:                                                                                                 **
    **                                                                                                            **
    **@Return String with access token                                                                            **
    ***************************************************************************************************************/

    public static String AccessToken() {
        // System.debug('@@@AccessToken Method@@@');
        User usr = [SELECT Username FROM User WHERE Id = :UserInfo.getUserId()];
        List<CommIntegrationTknDrpbx__c> itcList = [SELECT Access_Token__c FROM CommIntegrationTknDrpbx__c WHERE UserName__c = :usr.Username];
        if(itcList.isEmpty()) {
            return 'AuthorizationRequired';
        }
        String accessToken = itcList[0].Access_Token__c;
        return accessToken;
    }

    /***************************************************************************************************************
    **@Description: Generate Access Token for first time authorization and create custom setting for it.          **
    **                                                                                                            **
    **@Parameter code: Code used to generate access token.                                                        **
    **                                                                                                            **
    **@Return String with access token                                                                            **
    ***************************************************************************************************************/

    public static String AuthUser(String code) {
        // System.debug('@@@AuthUser Method@@@');
        String accessToken = '';
        User usr = [SELECT Username FROM User WHERE Id = :UserInfo.getUserId()];
        CommunityIntegrationKey__mdt keys = [SELECT Client_Id__c, Client_Secret__c, Redirect_URl__c
                                            FROM CommunityIntegrationKey__mdt 
                                            WHERE MasterLabel = :usr.Username AND Cloud_Name__c = 'Dropbox'];

        String messageBody = 'code=' + code +
                             '&client_id=' + keys.Client_Id__c +
                             '&client_secret=' + keys.Client_Secret__c +
                             '&redirect_uri=' + keys.Redirect_URl__c +
                             '&grant_type=authorization_code';

        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint('https://api.dropboxapi.com/oauth2/token');
        request.setHeader('Content-type', 'application/x-www-form-urlencoded');
        request.setBody(messageBody);
        request.setTimeout(60*1000);
        HttpResponse response = new Http().send(request);

        commDropboxWrapper.WrapperToken token = (commDropboxWrapper.WrapperToken)JSON.deserialize(response.getBody(), commDropboxWrapper.WrapperToken.class);
        
        CommIntegrationTknDrpbx__c itc = new CommIntegrationTknDrpbx__c();
        itc.UserName__c                = usr.Username;
        itc.SetupOwnerId               = UserInfo.getUserId();
        itc.Access_Token__c            = token.access_token;
        insert itc;
        accessToken = String.valueOf(token.access_token);
        return accessToken;
    }

    /***************************************************************************************************************
    **@Description: Method for all POST Requests.                                                                 **
    **                                                                                                            **
    **@Parameter endpoint: Endpoint for the request.                                                              **
    **@Parameter accToken: Access Token.                                                                          **
    **@Parameter body: Request body.                                                                              **
    **@Parameter contentType: Value of header Content-type.                                                       **
    **                                                                                                            **
    **@Return JSON String of type commDropboxWrapper.WrapperWhole                                                 **
    ***************************************************************************************************************/
    
    public Static commDropboxWrapper.WrapperWhole PostResponse(String endpoint, String accToken, String contentType, String body) {
        // System.debug('@@@PostResponse Method@@@');
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Content-type', contentType);
        request.setHeader('Content-length', String.valueOf(body.length()));
        request.setBody(body);
        request.setTimeout(60 * 1000);  
        HttpResponse response = new Http().send(request);

        String jsonString                               = response.getBody().replace('".tag":', '"tag":');
        commDropboxWrapper.WrapperWhole wrapperinstance = (commDropboxWrapper.WrapperWhole)JSON.deserialize(jsonString, commDropboxWrapper.WrapperWhole.class);
        wrapperinstance.status                          = response.getStatusCode();
        return wrapperinstance;
    }

    /***************************************************************************************************************
    **@Description: Method for Upload File Request.                                                               **
    **                                                                                                            **
    **@Parameter endpoint: Endpoint for the request.                                                              **
    **@Parameter accToken: Access Token.                                                                          **
    **@Parameter body: Request body.                                                                              **
    **@Parameter path: Path of the uploading file.                                                                **
    **                                                                                                            **
    **@Return JSON String of type commDropboxWrapper.WrapperFileList                                              **
    ***************************************************************************************************************/

    public Static commDropboxWrapper.WrapperFileList UploadFile(String endpoint, String accToken, String path, String body) {
        // System.debug('@@@UploadFile Method@@@');
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Content-Type', 'text/plain; charset=dropbox-cors-hack');
        request.setHeader('Dropbox-API-Arg', '{"path": "'+path+'", "mode": "add", "autorename": true, "mute": false, "strict_conflict": false}');
        request.setHeader('Content-length', String.valueOf(body.length()));
        request.setBodyAsBlob(EncodingUtil.base64Decode(body));
        request.setTimeout(60 * 1000);
        HttpResponse response = new Http().send(request);

        commDropboxWrapper.WrapperFileList wrapper = (commDropboxWrapper.WrapperFileList)JSON.deserialize(response.getBody(), commDropboxWrapper.WrapperFileList.class);
        return wrapper;
    }
}