public class DescribeCallAssignmentController{
    
    //List which have the value of all Objects
    public List<SelectOption> objectNamesList{ get; set; }
    
    //List which have field Names corrosponding to the selected object
    public List<SelectOption> fieldNamesList{ get; set; }
	
    //List which have selected field Names
    public List<SelectOption> selectedNamesList{ get; set; }
    
    //Variable for Selected Object's Name
    public String objectName{ get; set; }
    
    //Variable for Selected Field Name's from fieldNamesList
    public List<String> selectedFieldsFromObject{ get; set;}
    
    //Variable for Selected Field Name's from selectedNamesList
    public List<String> selectedFieldsFromSelect{ get; set;}

    public Boolean componentrendervariable{ get; set; }
    

    //Constructor of class DescribeCallAssignmentController 
    public DescribeCallAssignmentController(){
        
        //Initializing Object Name's List
        objectNamesList = new List<SelectOption>();
        objectNamesList.add(new SelectOption('None','------None------'));
        Map<string, SObjectType> objectNamesMap = Schema.getGlobalDescribe();
        for(String key : objectNamesMap.keySet()){
            objectNamesList.add(new SelectOption(key, objectNamesMap.get(key).getDescribe().getName()));
        }
        objectNamesList.sort();
        
        fieldNamesList = new List<SelectOPtion>();
        selectedNamesList = new List<SelectOption>();
        objectName = 'None';
        selectedFieldsFromObject = new List<String>();
        selectedFieldsFromSelect = new List<String>();        
        componentrendervariable = false;
    }
    
    //Method which populate Field Name for the corrosponding Object Selected in Object Picklist
    public void fieldNamesMethod(){              
        selectedNamesList = new List<SelectOption>();
        componentrendervariable = false;
        fieldNamesList.clear();
        if(objectName != 'None'){
            Map<string, SObjectField> fieldNamesMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
            for(String key : fieldNamesMap.keySet()){
                if( !(fieldNamesMap.get(key).getDescribe().getType() == Schema.DisplayType.ADDRESS) ){
                	fieldNamesList.add(new SelectOption(key, fieldNamesMap.get(key).getDescribe().getLabel()));
                }
            }
            fieldNamesList.sort();                
        }
        else if( objectName == 'None'){
            fieldNamesList  = new List<SelectOption>();
        } 
    }
        
    //Method for Resetting the Page
    public void pageReset(){
        objectName = 'None';
        fieldNamesList  = new List<SelectOption>();
        selectedNamesList = new List<SelectOption>();
        componentrendervariable = false;
    }
    
    //Method on Add Button to add Selected fields from left picklist to right picklist
    public void moethodOnAddButton(){
        if(selectedFieldsFromObject != NULL){
            for(SelectOption soption : fieldNamesList){            
                if(selectedFieldsFromObject.contains(soption.getValue())){
                    selectedNamesList.add(soption);
                    selectedFieldsFromObject.remove(selectedFieldsFromObject.indexOf(soption.getValue()));
                }
            }
            for(SelectOption soption : selectedNamesList){
                if(fieldNamesList.contains(soption))
                    fieldNamesList.remove(fieldNamesList.indexOf(soption));
            }
        }
        componentrendervariable = false;
    }
	
    //Method on Remove Button to add Selected fields from right picklist to left picklist
	public void moethodOnRemoveButton(){
        if(selectedFieldsFromSelect != NULL){
            for(SelectOption soption : selectedNamesList){            
                if(selectedFieldsFromSelect.contains(soption.getValue())){
                    fieldNamesList.add(soption);
                    selectedFieldsFromSelect.remove(selectedFieldsFromSelect.indexOf(soption.getValue()));
                }                
            }
            fieldNamesList.sort();
            for(SelectOption soption : fieldNamesList){
                if(selectedNamesList.contains(soption))
                    selectedNamesList.remove(selectedNamesList.indexOf(soption));
            }
        }
        componentrendervariable = false;
    }  
    
    public void rendermethod(){ componentrendervariable = true; } 
    
    public Boolean hasSelectedValue{ 
        get{
            if(selectedNamesList.size() == 0)
                return true;
            else
                return false;
        } 
        set;
    }     
}