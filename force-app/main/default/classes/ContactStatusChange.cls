global class ContactStatusChange implements Database.Batchable<SObject>, Database.Stateful{

    //Variables whose state needs to be maintained-----------------------------------------------------------
    global Integer totalRecords;                                                        //Total records meant to be updated

    //Constructor--------------------------------------------------------------------------------------------
    public ContactStatusChange() {                          
        totalRecords  = 0;
    }

    //Implementation of start method-------------------------------------------------------------------------
    global Database.QueryLocator start(Database.batchableContext batchContext){
        return Database.getQueryLocator('Select CreatedDate, Status__c FROM Contact WHERE CreatedDate = YESTERDAY');
    }

    //Implementation of execute method-----------------------------------------------------------------------
    global void execute(Database.BatchableContext batchContext, List<Contact> scope){
        List<Contact> contactList = new List<Contact>();
        for(Contact con : scope){                                                       //Updating status of contacts created on previous day
            con.Status__c = 'Ready For Approval';
            contactList.add(con);
        }
        if(contactList.size() > 0){
            Database.SaveResult[] saveResultList = Database.update(contactList, false);     //Acheiving partial updation
            totalRecords += saveResultList.size();
        }
    }

    //Implementation of finish method------------------------------------------------------------------------
    global void finish(Database.BatchableContext batchContext){
        String header = 'LastName,CreatedDate,Status__c,Testing_Date__c,Testing_Boolean__c,Testing_TextArea__c\n';
        List<Contact> conList = [SELECT LastName, CreatedDate, Status__c, Testing_Date__c, Testing_Boolean__c, Testing_TextArea__c FROM Contact WHERE CreatedDate = YESTERDAY];
        String recordDetail = '';
        for(Contact c : conList){
            recordDetail += (c.LastName + ',' + String.valueOf(c.CreatedDate) + ',' + c.Status__c + ',' + String.valueOf(c.Testing_Date__c) +',' + String.valueOf(c.Testing_DateTime__c) + c.Testing_TextArea__c + '\n');
        }
        String csvFileBody = header + recordDetail;
        String csvFileName = 'Status Updation Details.csv';
        Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
        csvAttachment.setFileName(csvFileName);
        csvAttachment.setBody(Blob.valueOf(csvFileBody));
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();         //Sending status email to the user
        mail.setToAddresses(new List<String>{'atyagi.sre@gmail.com'});
        mail.setSenderDisplayName('Salesforce Support');
        mail.setSubject('Contact\'s Status Updation');
        mail.setPlainTextBody('\nTotal Records Updated: ' + totalRecords);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment});
        Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        if (results[0].success){
            System.debug('The email was sent successfully.');
        }
    }
}