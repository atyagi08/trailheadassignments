@isTest
global with sharing class ContactStatusChangeTestClass {

    @isTest
    static void method01(){
        List<Contact> conList = new List<Contact>();
        for(Integer i=1; i<=100; i++){
            Contact con = new Contact(LastName = 'Date_Test_' + i);
            conList.add(con);
        }
        insert conList;
        conList.clear();
        conList = [SELECT Id FROM Contact WHERE LastName LIKE 'Date_Test_%'];
        Datetime yesterday = Datetime.now().addDays(-1);
        for(Contact con : conList){
            Test.setCreatedDate(con.Id, yesterday);
        }

        Test.startTest();
            ContactStatusChange conStatusChange = new ContactStatusChange();
            Id batchId = Database.executeBatch(conStatusChange);
        Test.stopTest();

        List<Contact> contactList = [SELECT LastName, Status__c FROM Contact WHERE LastName LIKE 'Date_Test_%' ORDER BY ID];
        System.assertEquals(100, contactList.size());
        for(Integer i=0; i<contactList.size(); i++){
            System.assertEquals('Date_Test_'+(i+1), contactList[i].LastName); System.assertEquals('Ready For Approval', contactList[i].Status__c);
        }
    }

    @isTest
    static void method02(){
        List<Contact> conList = new List<Contact>();
        for(Integer i=1; i<=100; i++){
            Contact con = new Contact(LastName = 'Date_Test_' + i);
            conList.add(con);
        }
        insert conList;
        Test.startTest();
            ContactStatusChange conStatusChange = new ContactStatusChange();
            Id batchId = Database.executeBatch(conStatusChange);
        Test.stopTest();

        List<Contact> contactList = [SELECT LastName FROM Contact WHERE Status__c = 'Ready For Approval'];
        System.assertEquals(0, contactList.size());
    }

    @isTest
    static void method03(){ 
        Test.startTest();
            String jobId = System.schedule('Update Record Status','00 00 1 * * ?', new ContactStatusChangeScheduler());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals('00 00 1 * * ?', ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            System.assertEquals('2019-11-19 01:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();

        //Check schedulable is in the job list
        List<AsyncApexJob> jobsScheduled = [SELECT ApexClass.Name FROM AsyncApexJob WHERE JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size());
        System.assertEquals('ContactStatusChangeScheduler', jobsScheduled[0].ApexClass.Name);

        //Check apex batch is in the job list
        List<AsyncApexJob> jobsApexBatch = [SELECT ApexClass.Name FROM AsyncApexJob WHERE JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size());
        System.assertEquals('ContactStatusChange', jobsApexBatch[0].ApexClass.Name);
    }

    @isTest
    static void method04(){
        List<Contact> conList = new List<Contact>();
        for(Integer i=1; i<=100; i++){
            Contact con = new Contact(LastName = 'Date_Test_' + i);
            conList.add(con);
        }
        insert conList;
        conList.clear();
        conList = [SELECT Id FROM Contact WHERE LastName LIKE 'Date_Test_%'];
        Datetime yesterday = Datetime.now().addDays(-1);
        for(Contact con : conList){
            Test.setCreatedDate(con.Id, yesterday);
        }

        Test.startTest();
            ContactStatusChange conStatusChange = new ContactStatusChange();
            Id batchId = Database.executeBatch(conStatusChange);
            System.assertEquals(1, Limits.getEmailInvocations());
        Test.stopTest();

        List<Contact> contactList = [SELECT LastName, Status__c FROM Contact WHERE LastName LIKE 'Date_Test_%' ORDER BY ID];
        System.assertEquals(100, contactList.size());
        for(Integer i=0; i<contactList.size(); i++){
            System.assertEquals('Date_Test_'+(i+1), contactList[i].LastName); System.assertEquals('Ready For Approval', contactList[i].Status__c);
        }
    }
}