/**
**@Description: Apex Wrapper Class for integration with Google Drive
**
**@Author: Akhil Tyagi
**@Date: 07 January 2020
**
**@ChangeLog:
**
***/

public with sharing class commGoogleDriveWrapper {
    
    //Wrapper Class for Access Token
    public class WrapperToken{
        public String  access_token{ get; set; }
        public String  refresh_token{ get; set; }
        public Integer expires_in{ get; set; }
    }

    //Wrapper Class for file details
    public class WrapperFileList{
        public String id{get; set; }
        public String name{get; set; }
        public String mimeType{get; set; }

        public WrapperFileList(){ }            
    }

    //Wrapper Calss for filel/folder and webContentLink details
    public class WrapperWhole{
        public List<WrapperFileList> files{get; set; }
        public String webContentLink{ get; set; }
    }
}