@isTest
private class StandardPaginationSortingTest {
	
    @isTest static void paginationvaluetrue(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
        List<SelectOption> list1 = sc.getPaginationValue();
        System.assertEquals(4, list1.size());
        Test.stopTest();
    }
    
    @isTest static void accounttrue(){
        Test.startTest();
        List<Account> accountList = new List<Account>();
        for(Integer i=1; i<10; i++){
            Account a = new Account(Name = 'Test '+ i, Industry = 'Apparel', Phone = '(336) 222-700' + i, Description = 'DESC' + i);
            accountList.add(a);
        }
        insert accountList;
        StandardPaginationSorting sc = new StandardPaginationSorting();
        System.assertEquals(accountList, sc.getAccounts());
        Test.stopTest();
    }
    
    @isTest static void toggleSortTest(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
        sc.toggleSort();
        Test.stopTest();
    }
    
    @isTest static void hasNextTest(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
        System.assertEquals(false, sc.hasNext);
        Test.stopTest();
    }
    
     @isTest static void hasPreviousTest(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
        System.assertEquals(false, sc.hasPrevious);
        Test.stopTest();
    }
    
    @isTest static void pageSizeTest(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
        System.assertEquals(10, sc.pageSize);
        Test.stopTest();
    }
    
     @isTest static void pageNumberTest(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
        System.assertEquals(1, sc.pageNumber);
        Test.stopTest();
    }
    
    @isTest static void firstTest(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
        sc.first();
        Test.stopTest();
    }
    
    @isTest static void previousTest(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
        sc.previous();
        Test.stopTest();
    }
    
    @isTest static void nextTest(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
        sc.next();
        Test.stopTest();
    }
    
    @isTest static void lastTest(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
        sc.last();
        Test.stopTest();
    }
    
     @isTest static void acntTest(){
        Test.startTest();
        StandardPaginationSorting sc = new StandardPaginationSorting();
		sc.Size = 10;
        sc.acnt();
        Test.stopTest();
    }
}