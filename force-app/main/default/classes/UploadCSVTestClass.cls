@isTest
public with sharing class UploadCSVTestClass {

//-------------------------------------Cases for ReadFromFile Method which converts file body to string------------------------------------

	//CASE 1 - File Size greater than 500 KB
    @isTest
    static void ReadFromFile01(){
        Test.startTest();
        UploadCSVController csv = new UploadCSVController();
        csv.fileSize = 780760;
        csv.ReadFromFile();
        Test.stopTest();
    }

    //CASE 2 - File extension not csv
    @isTest
    static void ReadFromFile02(){
        Test.startTest();
        UploadCSVController csv = new UploadCSVController();
        csv.fileSize = 780760;
        csv.fileName = 'csv_1.txt';
        csv.ReadFromFile();
        Test.stopTest();
    }

	//CASE 3 - Error reading CSV File
    @isTest
    static void ReadFromFile03(){
        Test.startTest();
        UploadCSVController csv = new UploadCSVController();
        csv.fileSize = 36000;
        Blob b = null;
        csv.fileBody = b;
        csv.fileName = 'csv_1.csv';
        csv.ReadFromFile();
        Test.stopTest();
    }
    
	//CASE 4 - Readable CSV File
    @isTest
    static void ReadFromFile04(){
        Test.startTest();
        UploadCSVController csv = new UploadCSVController();
        csv.fileSize = 36000;
        Blob b = Blob.valueOf('');
        csv.fileBody = b;
        csv.fileName = 'csv_1.csv';
        csv.ReadFromFile();
        Test.stopTest();
    }

//-------------------------------------Cases for ReadCSVFile Method which checks exception in Date, DateTime, Boolean field----------------
    
    //CASE 1 - Uploaded file with no exception
    @isTest
    static void ReadCSVFile01(){
        Test.startTest();
        UploadCSVController csv = new UploadCSVController();
        csv.fileSize = 36000;
        String str = 'LastName,Testing_Date__c,Testing_DateTime__c,Testing_Boolean__c,Testing_TextArea__c\nTest_1,1999-11-25,1999-11-25 01:55:40,TRUE,\"hello doing, my\"\nTest_2,2000-10-15,2000-10-15 02:50:30,FALSE,\"hi, what, you\"\nTest_3,2001-09-05,2001-09-05 03:45:20,TRUE,what can you';
        csv.fileBody = Blob.valueOf(str);
        csv.fileName = 'csv_1.csv';
        csv.ReadFromFile();
        Test.stopTest();
    }

    //CASE 2 - Uploaded file with exception in Date type Field
    @isTest
    static void ReadCSVFile02(){
        Test.startTest();
        UploadCSVController csv = new UploadCSVController();
        csv.fileSize = 36000;
        String str = 'Testing_Date__c,Testing_DateTime__c,Testing_Boolean__c,Testing_TextArea__c\nTest_1,25-11-1999,1999-11-25 01:55:40,TRUE,\"hello doing, my\"\nTest_2,2000/10/15,2000-10-15 02:50:30,FALSE,\"hi, what, you\"\nTest_3,2001-15-05,2001-09-05 03:45:20,TRUE,what can you\nTest_4,1999-11-25,1999-11-25 01:55:40,TRUE,\"hello doing, my\"';
        csv.fileBody = Blob.valueOf(str);
        csv.fileName = 'csv_1.csv';
        csv.ReadFromFile();
        Test.stopTest();
    }

//------------------------------------------------------------Cases for Batch Class--------------------------------------------------------
    //CASE 1 - Uploaded file with no exception
    @isTest
    static void Batch01(){
        Test.startTest();
        List<String> fieldNamesList = new List<String>();
        fieldNamesList.add('LastName');
        fieldNamesList.add('Testing_Date__c');
        fieldNamesList.add('Testing_DateTime__c');
        fieldNamesList.add('Testing_Boolean__c');
        fieldNamesList.add('Testing_TextArea__c');
        String fileBodyAsString = 'Test_1,1999-11-25,1999-11-25 01:55:40,TRUE,\"hello doing, my\"\nTest_2,2000-10-15,2000-10-15 02:50:30,FALSE,\"hi, what, you\"\nTest_3,2001-09-05,2001-09-05 03:45:20,TRUE,what can you';
        ReadAndPopulateBatch readBatch = new ReadAndPopulateBatch(fieldNamesList, fileBodyAsString);
        Id batchId = Database.executeBatch(readBatch, 1);
        Test.stopTest();

        List<Contact> conList = [SELECT LastName FROM Contact ORDER By LastName];
        System.assertEquals(3, conList.size());
        System.assertEquals('Test_1', conList[0].LastName);
        System.assertEquals('Test_2', conList[1].LastName);
        System.assertEquals('Test_3', conList[2].LastName);
    }

    //CASE 2 - Uploaded file with exception in Date type field
    @isTest
    static void Batch02(){
        Test.startTest();
        List<String> fieldNamesList = new List<String>();
        fieldNamesList.add('LastName');
        fieldNamesList.add('Testing_Date__c');
        fieldNamesList.add('Testing_DateTime__c');
        fieldNamesList.add('Testing_Boolean__c');
        fieldNamesList.add('Testing_TextArea__c');
        String fileBodyAsString = 'Test_1,1999/11/25,1999-11-25 01:55:40,TRUE,\"hello doing, my\"\nTest_2,2000-15-15,2000-10-15 02:50:30,FALSE,\"hi, what, you\"\nTest_3,2001-09-05,2001-09-05 03:45:20,TRUE,what can you';
        ReadAndPopulateBatch readBatch = new ReadAndPopulateBatch(fieldNamesList, fileBodyAsString);
        Id batchId = Database.executeBatch(readBatch, 1);
        Test.stopTest();

        List<Contact> conList = [SELECT LastName, Testing_Date__c FROM Contact ORDER By LastName];
        System.assertEquals(3, conList.size());
        System.assertEquals('Test_1', conList[0].LastName);     System.assertEquals(null, conList[0].Testing_Date__c);
        System.assertEquals('Test_2', conList[1].LastName);     System.assertEquals(null, conList[1].Testing_Date__c);
        System.assertEquals('Test_3', conList[2].LastName);     System.assertEquals(Date.valueOf('2001-09-05'), conList[2].Testing_Date__c);
    }
    
    //CASE 3 - Uploaded file with exception in DateTime type Field
    @isTest
    static void Batch03(){
        Test.startTest();
        List<String> fieldNamesList = new List<String>();
        fieldNamesList.add('LastName');
        fieldNamesList.add('Testing_Date__c');
        fieldNamesList.add('Testing_DateTime__c');
        fieldNamesList.add('Testing_Boolean__c');
        fieldNamesList.add('Testing_TextArea__c');
        String fileBodyAsString = 'Test_1,1999-11-25,1999-11-2501:55:40,TRUE,\"hello doing, my\"\nTest_2,2000-10-15,2000/10/15 02:50:01,FALSE,\"hi, what, you\"\nTest_3,2001-09-05,2001-09-05 03:45,TRUE,what can you\nTest_4,1999-11-25,1999-11-25 01:55:40,TRUE,\"hello doing, my\"';
        ReadAndPopulateBatch readBatch = new ReadAndPopulateBatch(fieldNamesList, fileBodyAsString);
        Id batchId = Database.executeBatch(readBatch, 1);
        Test.stopTest();

        List<Contact> conList = [SELECT LastName, Testing_DateTime__c FROM Contact ORDER By LastName];
        System.assertEquals(4, conList.size());
        System.assertEquals('Test_1', conList[0].LastName);    System.assertEquals(null, conList[0].Testing_DateTime__c);
        System.assertEquals('Test_2', conList[1].LastName);    System.assertEquals(null, conList[1].Testing_DateTime__c);
        System.assertEquals('Test_3', conList[2].LastName);    System.assertEquals(null, conList[2].Testing_DateTime__c);
        System.assertEquals('Test_4', conList[3].LastName);    System.assertEquals(DateTime.valueOf('1999-11-25 01:55:40'), conList[3].Testing_DateTime__c);

    }

    //CASE 4 - Uploaded file with exception in Boolean type Field
    @isTest
    static void Batch04(){
        Test.startTest();
        List<String> fieldNamesList = new List<String>();
        fieldNamesList.add('LastName');
        fieldNamesList.add('Testing_Date__c');
        fieldNamesList.add('Testing_DateTime__c');
        fieldNamesList.add('Testing_Boolean__c');
        fieldNamesList.add('Testing_TextArea__c');
        String fileBodyAsString = 'Test_1,1999-11-25,1999-11-25 01:55:40,,\"hello doing, my\"\nTest_2,2000-10-15,2000-10-15 02:50:01,hello,\"hi, what, you\"\nTest_3,2001-09-05,2001-09-05 03:45:50,null,what can you\nTest_4,1999-11-25,1999-11-25 01:55:40,TRUE,\"hello doing, my\"';
        ReadAndPopulateBatch readBatch = new ReadAndPopulateBatch(fieldNamesList, fileBodyAsString);
        Id batchId = Database.executeBatch(readBatch, 1);
        Test.stopTest();

        List<Contact> conList = [SELECT LastName, Testing_Boolean__c FROM Contact ORDER By LastName];
        System.assertEquals(4, conList.size());
        System.assertEquals('Test_1', conList[0].LastName);    System.assertEquals(false, conList[0].Testing_Boolean__c);
        System.assertEquals('Test_2', conList[1].LastName);    System.assertEquals(false, conList[1].Testing_Boolean__c);
        System.assertEquals('Test_3', conList[2].LastName);    System.assertEquals(false, conList[2].Testing_Boolean__c);
        System.assertEquals('Test_4', conList[3].LastName);    System.assertEquals(true, conList[3].Testing_Boolean__c);
    }

    //CASE 5 - Uploaded file with textArea type field which include comma and also without comma
    @isTest
    static void Batch05(){
        Test.startTest();
        List<String> fieldNamesList = new List<String>();
        fieldNamesList.add('LastName');
        fieldNamesList.add('Testing_Date__c');
        fieldNamesList.add('Testing_DateTime__c');
        fieldNamesList.add('Testing_Boolean__c');
        fieldNamesList.add('Testing_TextArea__c');
        String fileBodyAsString = 'Test_1,1999-11-25,1999-11-25 01:55:40,true,\"hello how are you doing, my lord\"\nTest_2,2000-10-15,2000-10-15 02:50:01,false,\nTest_3,2001-09-05,2001-09-05 03:45:50,true,what can you';
        ReadAndPopulateBatch readBatch = new ReadAndPopulateBatch(fieldNamesList, fileBodyAsString);
        Id batchId = Database.executeBatch(readBatch, 1);
        Test.stopTest();
        
        List<Contact> conList = [SELECT LastName, Testing_TextArea__c FROM Contact ORDER BY LastName];
        System.assertEquals(3, conList.size());
        System.assertEquals('Test_1', conList[0].LastName);    System.assertEquals('hello how are you doing, my lord', conList[0].Testing_TextArea__c);
        System.assertEquals('Test_2', conList[1].LastName);    System.assertEquals(null, conList[1].Testing_TextArea__c);
        System.assertEquals('Test_3', conList[2].LastName);    System.assertEquals('what can you', conList[2].Testing_TextArea__c);
    }

    //CASE 6 - Uploaded file with exception in Date, DateTime, Boolean type Field
    @isTest
    static void Batch06(){
        Test.startTest();
        List<String> fieldNamesList = new List<String>();
        fieldNamesList.add('LastName');
        fieldNamesList.add('Testing_Date__c');
        fieldNamesList.add('Testing_DateTime__c');
        fieldNamesList.add('Testing_Boolean__c');
        fieldNamesList.add('Testing_TextArea__c');
        String fileBodyAsString = 'Test_1,1999/11/25,1999-11-25 01:55:40,True,\"hello how are you doing, my lord\"\nTest_2,2000-10-15,2000-10-1502:50:01,false,\"hi, what, you\"\nTest_3,2001-09-05,2001-09-05 03:45:50,null,what can you\nTest_4,1999-11-25,1999-11-25 01:55:40,TRUE,';
        ReadAndPopulateBatch readBatch = new ReadAndPopulateBatch(fieldNamesList, fileBodyAsString);
        Id batchId = Database.executeBatch(readBatch, 1);
        Test.stopTest();

        List<Contact> conList = [SELECT LastName, Testing_Date__c, Testing_DateTime__c, Testing_Boolean__c, Testing_TextArea__c FROM Contact ORDER BY LastName];
        System.assertEquals(4, conList.size());
        System.assertEquals('Test_1', conList[0].LastName);    System.assertEquals(null, conList[0].Testing_Date__c);
        System.assertEquals('Test_2', conList[1].LastName);    System.assertEquals(null, conList[1].Testing_DateTime__c);
        System.assertEquals('Test_3', conList[2].LastName);    System.assertEquals(false, conList[2].Testing_Boolean__c);
        System.assertEquals('Test_4', conList[3].LastName);    System.assertEquals(null, conList[3].Testing_TextArea__c);
    }

    //CASE 7 - file with Lastname missing in some records
    @isTest
    static void Batch07(){
        Test.startTest();
        List<String> fieldNamesList = new List<String>();
        fieldNamesList.add('LastName');
        fieldNamesList.add('Testing_Date__c');
        fieldNamesList.add('Testing_DateTime__c');
        fieldNamesList.add('Testing_Boolean__c');
        fieldNamesList.add('Testing_TextArea__c');
        String fileBodyAsString = ',1999-11-25,1999-11-25 01:55:40,TRUE,\"hello doing, my\"\nTest_2,2000-10-15,2000-10-15 02:50:30,FALSE,\"hi, what, you\"\n,2001-09-05,2001-09-05 03:45:20,TRUE,what can you';
        ReadAndPopulateBatch readBatch = new ReadAndPopulateBatch(fieldNamesList, fileBodyAsString);
        Id batchId = Database.executeBatch(readBatch, 1);
        Test.stopTest();

        List<Contact> conList = [SELECT LastName FROM Contact ORDER By LastName];
        System.assertEquals(1, conList.size());
        System.assertEquals('Test_2', conList[0].LastName);
    }
}