global with sharing class ReadAndPopulateBatch implements Database.batchable<String>, Database.Stateful{
    
    //Private Variables--------------------------------------------------------------------------------------------------------------------
    private String csvFile;                                             //File body
    private List<String> fieldList = new List<String>();                //List of fields available in CSV file
    
    //Stateful Variables-------------------------------------------------------------------------------------------------------------------
    global List<Contact> conList    = new List<Contact>();              //List of contacts to be inserted
    global Integer totRecords       = 0;                                //Total records in CSV file
    global Integer insRecords       = 0;                                //Successful inserted records
    global Integer failRecords      = 0;                                //Failed records
    global List<String> errorList   = new List<String>();               //List of exceptions in record insertion
   
    //Constructor-------------------------------------------------------------------------------------------------------------------------
    public ReadAndPopulateBatch(List<String> fldList, String fileBody){
        fieldList = fldList;
        csvFile = fileBody;
    }

    //Implementing start method------------------------------------------------------------------------------------------------------------
    global Iterable<String> start(Database.batchableContext batchableContext){ 
        return new CSVIterator(csvFile);                                //Calling Itereable class for the file
    }

    //Implementing execute method----------------------------------------------------------------------------------------------------------
    global void execute(Database.BatchableContext batchableContext, List<String> scope){
        System.debug('@@@Scope Size === ' + scope.size());

        String  contactDetails  = '';
        Contact con             = new Contact();
        Map<string, SObjectField> fieldNamesMap = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap();
        String str = scope[0];
        scope.clear();
        scope = str.split('\n');

        for(Integer i=0; i<scope.size(); i++){                          //Itereation for each contact detail
            contactDetails = scope[i];
            String lastnameValue = contactDetails.substringBefore(',').trim();
            for(Integer j=0; j<fieldList.size(); j++){                  //Iterating over the fields & assigning value to them for each contact 
                Schema.DisplayType fieldType = fieldNamesMap.get(fieldList[j]).getDescribe().getType();
                if( fieldType == Schema.DisplayType.DATE ){             //Setting value of fields which are of Date Type
                    try{
                        con.put(fieldList[j].trim(), Date.valueOf(contactDetails.substringBefore(',').trim()));
                    }
                    catch(Exception e){
                        errorList.add('Empty ' + fieldList[j].trim() + ' field or Date is not in format \"YYYY-MM-DD\" for record with LastName = ' + lastnameValue);
                    }
                }
                else if( fieldType == Schema.DisplayType.DATETIME ){    //Setting value of fields which are of DateTime Type
                    try{
                        con.put(fieldList[j].trim(), Datetime.valueOf(contactDetails.substringBefore(',').trim()));
                    }
                    catch(Exception e){
                        errorList.add('Empty ' + fieldList[j].trim() + ' field or DateTime is not in format \"YYYY-MM-DD HH:MM:SS\" for record with LastName = ' + lastnameValue);
                    }
                }
                else if( fieldType == Schema.DisplayType.BOOLEAN ){     //Setting value of fields which are of Boolean Type
                    if(contactDetails.substringBefore(',').trim().equalsIgnoreCase('true') || contactDetails.substringBefore(',').trim().equalsIgnoreCase('false') || contactDetails.substringBefore(',').trim() == ''){
                        con.put(fieldList[j].trim(), Boolean.valueOf(contactDetails.substringBefore(',').trim()));
                    }
                    else{
                        errorList.add('Acceptable value for ' + fieldList[j].trim() + ' field is \"TRUE/FALSE\" for record with LastName = ' + lastnameValue);
                    }
                }
                else if( fieldType == Schema.DisplayType.TEXTAREA ){    //Setting value of fields which are of TextArea Type
                    if(contactDetails.left(1) != '\"'){                 //Setting value of fields which are of TextArea Type with comma
                        con.put(fieldList[j].trim(), contactDetails.substringBefore(',').trim());
                    }
                    else{                                               //Setting value of fields which are of TextArea Type without comma
                        con.put(fieldList[j].trim(), contactDetails.substringBetween('\"', '\"') .trim());
                        contactDetails = contactDetails.substringAfter('\"').substringAfter('\"');
                    }
                }
                else{                                                   //Setting value of all other type of fields
                    // try{
                        con.put(fieldList[j].trim(), contactDetails.substringBefore(',').trim());
                    // }
                    // catch(Exception e){}
                }
                contactDetails = contactDetails.substringAfter(',');
            }
            conList.add(con);
            con = new Contact();
        }
        System.debug('@@@ConList Size === ' + conList.size());

        Database.SaveResult[] srList = Database.insert(conList, false); //Inserting records acheiving partial insertion
        conList.clear();
        for (Database.SaveResult sr : srList) {
            ++totRecords;
            if (!sr.isSuccess()) {
                ++failRecords;
                for(Database.Error err : sr.getErrors()) {
                    errorList.add(err.getStatusCode() + ': ' + err.getMessage() + ' for Record at Line Number: ' + (totRecords + 1));
                }
            }
            else{
                ++insRecords;
            }
        }
    }

    //Implementing finish method-----------------------------------------------------------------------------------------------------------
    global void finish(Database.BatchableContext batchableContext){
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems FROM AsyncApexJob WHERE Id = :batchableContext.getJobId()];
        System.debug(a);
        System.debug('@@@Toatal Records === ' + totRecords + ' @@@Inserted Records === ' + insRecords + '@@@Failed Records === ' + failRecords);
        System.debug('@@@Error List Size === ' + errorList.size());
        for(String str : errorList){ System.debug(str); }
    }
}