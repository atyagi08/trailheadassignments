public class ImportObjectCSVController {
    public string csvRecords {get;set;}

    public void insertCsvRecords(){
        List<ImportObjectCSVController.ObjectWrapper> objectRecords = (List<ImportObjectCSVController.ObjectWrapper>)JSON.deserialize(csvRecords, List<ImportObjectCSVController.ObjectWrapper>.class);
        List<Lead> objectListToInsert = new List<Lead>();
        csvRecords = '';
        // Collecting
        Set<String> emailSet = new Set<String>();
        Set<String> firstNameSet = new Set<String>();
        Set<String> lastNameSet = new Set<String>();
        Set<String> companySet = new Set<String>();
        for(ObjectWrapper objRow : objectRecords){
            emailSet.add(objRow.email);
            firstNameSet.add(objRow.firstName);
            lastNameSet.add(objRow.lastName);
            companySet.add(objRow.company);
        }
        Map<String,Id> emailDupMap = new Map<String,Id>();
        Map<String,Id> firstNameDupMap = new Map<String,Id>();
        Map<String,Id> lastNameDupMap = new Map<String,Id>();
        Map<String,Id> companyDupMap = new Map<String,Id>();

        for(Lead ld : [SELECT id,email,firstName,lastName,company FROM Lead WHERE email IN :emailSet OR company IN : companySet OR firstName IN :firstNameSet OR lastName IN :lastNameSet LIMIT 100]){
            if(emailSet.contains(ld.email)){
                emailDupMap.put(ld.email,ld.Id);
            }
            if(firstNameSet.contains(ld.firstName)){
                firstNameDupMap.put(ld.firstName,ld.Id);
            }
            if(lastNameSet.contains(ld.lastName)){
                lastNameDupMap.put(ld.lastName,ld.Id);
            }
            if(companySet.contains(ld.company)){
                companyDupMap.put(ld.company,ld.Id);
            }
        }
        // releasing memory of variables
        emailSet = new Set<String>();
        firstNameSet = new Set<String>();
        lastNameSet = new Set<String>();
        companySet = new Set<String>();

        // Adding New Lead to Insert
        for(ObjectWrapper objRow : objectRecords){
            Lead newLead = new Lead();
            newLead.lastName = objRow.lastName;
            newLead.Title = objRow.title;
            newLead.company = objRow.company;
            newLead.firstName = objRow.firstName;
            newLead.email = objRow.email == 'N' ? '' : objRow.email;

            //Check all required fields has value
            if(newLead.Company != null && newLead.company.trim().length() > 0 && newLead.LastName != null && newLead.lastName.trim().length() > 0){
                objectListToInsert.add(newLead);
            }
        }
        objectRecords = null;

        //Insert all Object records
        insert objectListToInsert;
    }
    public class ObjectWrapper{
        public String firstName {get;set;}
        public String lastName {get;set;}
        public String title {get;set;}
        public String company {get;set;}
        public String email{get;set;}
        // You can add more fields according to your data file.
    }

}