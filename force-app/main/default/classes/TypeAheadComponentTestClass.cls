@isTest
public with sharing class TypeAheadComponentTestClass {

//-----------------------------------------------Cases for SearchListMethod which creates list for TypeAhead-----------------------------------------------

//CASE 1 - Records with last digit different
    @isTest
    static void SearchListMethod01(){
        Test.startTest();
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<=5; i++){
            Account a = new Account(Name='Test_'+i);
            accList.add(a);
        }
        insert accList;
        List<String> strList = TypeAheadComponentController.SearchListMethod('Account', 'Name', 'Te');
        System.assertEquals(5, strList.size());
        for(Integer i=1; i<=5; i++){
            System.assertEquals('Test_'+i, strList[i-1].substringBefore(' ||'));
        }
        Test.stopTest();
    }

//CASE 2 - Records with starting some digits same
    @isTest
    static void SearchListMethod02(){
        Test.startTest();
        List<Account> accList = new List<Account>();
        accList.add(new Account(Name='Univ'));
        accList.add(new Account(Name='University'));
        accList.add(new Account(Name='University of'));
        accList.add(new Account(Name='University of Arizona'));
        insert accList;
        List<String> strList = new List<String>();
        strList = TypeAheadComponentController.SearchListMethod('Account', 'Name', 'Univ');
        System.assertEquals(4, strList.size());
        System.assertEquals('Univ', strList[0].substringBefore(' ||'));
        System.assertEquals('University', strList[1].substringBefore(' ||'));
        System.assertEquals('University of', strList[2].substringBefore(' ||'));
        System.assertEquals('University of Arizona', strList[3].substringBefore(' ||'));

        strList = TypeAheadComponentController.SearchListMethod('Account', 'Name', 'University');
        System.assertEquals(3, strList.size());
        System.assertEquals('University', strList[0].substringBefore(' ||'));
        System.assertEquals('University of', strList[1].substringBefore(' ||'));
        System.assertEquals('University of Arizona', strList[2].substringBefore(' ||'));

        strList = TypeAheadComponentController.SearchListMethod('Account', 'Name', 'University of');
        System.assertEquals(2, strList.size());
        System.assertEquals('University of', strList[0].substringBefore(' ||'));
        System.assertEquals('University of Arizona', strList[1].substringBefore(' ||'));

        strList = TypeAheadComponentController.SearchListMethod('Account', 'Name', 'University of Arizona');
        System.assertEquals(1, strList.size());
        System.assertEquals('University of Arizona', strList[0].substringBefore(' ||'));
        Test.stopTest();
    }

//CASE 3 - When there are no records corrosponding to the input text
    @isTest
    static void SearchListMethod03(){
        Test.startTest();
        List<String> strList = TypeAheadComponentController.SearchListMethod('Account', 'Name', 'Te');
        System.assertEquals(0, strList.size());
        Test.stopTest();
    }

//-----------------------------------------------Cases for RecordDetailsMethod which fetches the Record Details-----------------------------------------------

//CASE 1 - Success case where the record is available in the object where detais are fetched
    @isTest
    static void RecordDetailsMethod01(){
        Test.startTest();
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<=5; i++){
            Account a = new Account(Name='Test_'+i);
            accList.add(a);
        }
        insert accList;
        Id tempId = [SELECT Id FROM Account WHERE Name = 'Test_1'].Id;
        List<String> strList        = new List<String>();
        TypeAheadComponentController typeAhead = new TypeAheadComponentController();
        typeAhead.renderVariable    = true;
        typeAhead.inputText         = 'Test_1';
        typeAhead.objectName        = 'Account';
        typeAhead.fieldName         = 'Name';
        typeAhead.RecordDetailsMethod();
        System.assertEquals(tempId, typeAhead.recordId);
        Test.stopTest();
    }

//CASE 2 - Failure case where the record is not available in the object
    @isTest
    static void RecordDetailsMethod02(){
        Test.startTest();
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<=5; i++){
            Account a = new Account(Name='Test_'+i);
            accList.add(a);
        }
        insert accList;
        List<String> strList        = new List<String>();
        TypeAheadComponentController typeAhead = new TypeAheadComponentController();
        typeAhead.renderVariable    = true;
        typeAhead.objectName        = 'Account';
        typeAhead.fieldName         = 'Name';
        typeAhead.RecordDetailsMethod();
        System.assertEquals(null, typeAhead.recordId);
        Test.stopTest();
    }
}