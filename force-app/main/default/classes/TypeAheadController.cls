public with sharing class TypeAheadController {
    
    public String objectName{ get; set; }                                       //Selected object
    public List<SelectOption> objectNameList{ get; set; }                       //SelectOption List of all the objects
    public String fieldName{ get; set; }                                        //Selected field
    public List<SelectOption> fieldNameList{get; set; }                         //SelectOption List of all the fields of the selected object
    public Boolean componentrendervariable{ get; set; }                         //Variable to render component on the page
 
    public TypeAheadController() {                                              //Constructor
        objectName = 'None';
        objectNameList = new List<SelectOption>();
        objectNameList.add(new SelectOption('None','------------------None------------------')); 
        Map<string, SObjectType> objectNamesMap = Schema.getGlobalDescribe();
        for(String key : objectNamesMap.keySet()){
            objectNameList.add(new SelectOption(key, objectNamesMap.get(key).getDescribe().getName()));
        }
        objectNameList.sort();
        fieldName = '';
        fieldNameList = new List<SelectOption>();
        componentrendervariable = false;
    }
    
    public void fieldNamesMethod(){                                             //Method which populate Field Name for the corrosponding Object Selected in Object Picklist
        componentrendervariable = false;
        fieldNameList.clear();
        fieldNameList.add(new SelectOption('None','------------------None------------------'));       
        if(objectName != 'None'){
            Map<string, SObjectField> fieldNamesMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
            for(String key : fieldNamesMap.keySet()){
                if( !(fieldNamesMap.get(key).getDescribe().getType() == Schema.DisplayType.ADDRESS) ){
                	fieldNameList.add(new SelectOption(key, fieldNamesMap.get(key).getDescribe().getLabel()));
                }
            }
            fieldNameList.sort();                
        }
        else if( objectName == 'None'){
            fieldNameList.clear();
            } 
    }

    public void componentShowMethod(){                                          //Method to render vf component on the page
        if(fieldName != 'None'){
            componentrendervariable =true;
        }   
        else{
            componentrendervariable = false;
        }
    }
}