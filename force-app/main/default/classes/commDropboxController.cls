/**
**@Description: Apex Controller Class for communityDropbox Component
**
**@Author: Akhil Tyagi
**@Date: 16 January 2020
**
**@ChangeLog:
**17 January 2020: Created DwnldFile, DeleteFile and CreateFolder Method
**20 January 2020: Created FileUpload Method
***/

public class commDropboxController {

    /***************************************************************************************************************
    **@Description: Generate Access Token or Authorization URL based on condition.                                **
    **                                                                                                            **
    **@Parameter:                                                                                                 **
    **                                                                                                            **
    **@Return String with access token or authorization URL                                                       **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String getAccessToken(){
        // System.debug('@@@getAccessToken Method @@@');
        String accessToken = commDropboxService.AccessToken();
        if(accessToken != 'AuthorizationRequired') {
            return accessToken;
        }
        User usr                          = [SELECT Username FROM User WHERE Id = :UserInfo.getUserId()];
        CommunityIntegrationKey__mdt keys = [SELECT Client_Id__c, Redirect_URl__c 
                                            FROM CommunityIntegrationKey__mdt 
                                            WHERE MasterLabel = :usr.Username AND Cloud_Name__c = 'Dropbox'];
                                            
        String client_id    = EncodingUtil.urlEncode(keys.Client_Id__c,'UTF-8');
        String redirect_uri = EncodingUtil.urlEncode(keys.Redirect_URl__c,'UTF-8');
        String pageUrl      = 'https://www.dropbox.com/oauth2/authorize?'+
                              'client_id='+client_id+
                              '&redirect_uri='+redirect_uri+
                              '&response_type=code'+
                              '&state=security_token%3D138r5719ru3e1%26url%3Dhttps://oa2cb.example.com/myHome/dropbox';
        return pageUrl;
    }

    /***************************************************************************************************************
    **@Description: Generate Access Token for first time authorization.                                           **
    **                                                                                                            **
    **@Parameter code: Code used to generate access token                                                         **
    **                                                                                                            **
    **@Return String with access token                                                                            **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String AuthorizeUser(String code) {
        // System.debug('@@@AuthorizeUser Method @@@');
        return commDropboxService.AuthUser(code);
    }

    /***************************************************************************************************************
    **@Description: Generate List of file and folders in current directory.                                       **
    **                                                                                                            **
    **@Parameter path: path of the current directory(folder)                                                      **
    **                                                                                                            **
    **@Return JSON String with list of file and folder in current directory                                       **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String RecordsList(String path){
        // System.debug('@@@RecordsList Method @@@');
        String accToken    = getAccessToken();
        String endpoint    = 'https://api.dropboxapi.com/2/files/list_folder';
        String contentType = 'application/json';
        String body        = (path == 'root') ? '{"path": ""}' : '{"path": "'+path+'"}';

        commDropboxWrapper.WrapperWhole wrapperinstance = commDropboxService.PostResponse(endpoint, accToken, contentType, body);
        return JSON.serialize(wrapperinstance);
    }

    /***************************************************************************************************************
    **@Description: Generate link to download the file.                                                           **
    **                                                                                                            **
    **@Parameter file_path: Path of the file to be downloaded.                                                    **
    **                                                                                                            **
    **@Return String with download link of the file                                                               **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String DwnldFile(String file_path) {
        // System.debug('@@@DwnldFile Method @@@');
        String accToken = getAccessToken();
        String endpoint = 'https://api.dropboxapi.com/2/files/get_temporary_link';
        String cntntTyp = 'application/json';
        String body     = '{"path" :"'+file_path+'"}';

        commDropboxWrapper.WrapperWhole wrapperinstance = commDropboxService.PostResponse(endpoint, accToken, cntntTyp, body);
        return wrapperinstance.link;
    }

    /***************************************************************************************************************
    **@Description: Delete the file or folder.                                                                    **
    **                                                                                                            **
    **@Parameter file_path: Path of the file/folder to be downloaded.                                             **
    **                                                                                                            **
    **@Return String to state wether the file/folder is deleted or not.                                           **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String DeleteFile(String file_path) {
        // System.debug('@@@DeleteFile Method @@@');
        String accToken = getAccessToken();
        String endpoint = 'https://api.dropboxapi.com/2/files/delete_v2';
        String cntntTyp = 'application/json';
        String body     = '{"path": "'+file_path+'"}';

        commDropboxWrapper.WrapperWhole wrapperinstance = commDropboxService.PostResponse(endpoint, accToken, cntntTyp, body);
        if(wrapperinstance.status == 200){
            return 'Successfully Deleted';
        }
        return 'File not deleted successfully';
    }

    /***************************************************************************************************************
    **@Description: Create folder in current directory(folder).                                                   **
    **                                                                                                            **
    **@Parameter folder_path: Path of the folder to be created.                                                   **
    **                                                                                                            **
    **@Return JSON String with details of the uploaded file/folder.                                               **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String CreateFolder(String folder_path){
        // System.debug('@@@CreateFolder Method @@@');
        String accToken    = getAccessToken();
        String endpoint    = 'https://api.dropboxapi.com/2/files/create_folder_v2';
        String contentType = 'application/json';
        String body        = '{"path": "'+folder_path+'", "autorename": true}';

        commDropboxWrapper.WrapperWhole wrapper = commDropboxService.PostResponse(endpoint, accToken, contentType, body);
        return JSON.serialize(wrapper);
    }

    /***************************************************************************************************************
    **@Description: Upload file to current directory(folder).                                                     **
    **                                                                                                            **
    **@Parameter fileBody: Body of the file.                                                                      **
    **@Parameter dirPath: Path of the file.                                                                       **
    **                                                                                                            **
    **@Return JSON String with details of the uploaded file.                                                      **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String FileUpload( String fileBody, String dirPath) {
        // System.debug('@@@FileUpload Method @@@');
        String accToken    = getAccessToken();
        String endpoint    = 'https://content.dropboxapi.com/2/files/upload';

        commDropboxWrapper.WrapperFileList wrapper = commDropboxService.UploadFile(endpoint, accToken, dirPath, fileBody);
        return JSON.serialize(wrapper);
    }    
}