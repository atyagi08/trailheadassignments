global with sharing class ContactStatusChangeScheduler implements Schedulable{

  global void execute(SchedulableContext sc) {
    ContactStatusChange csc = new ContactStatusChange(); 
    database.executebatch(csc);
  }
}