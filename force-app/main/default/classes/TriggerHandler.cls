public class TriggerHandler {
    
    public static Boolean isFirstTime           = true;								//Static variable to stop trigger recursion
    static List<Contact> contactUpdateList		= new List<Contact>();				//List which will be updated
    static Set<Id> accountIdSet				    = new Set<Id>();					//Id Set of Accounts whose records are inserted
    static Set<Id> validSequenceSet 	        = new Set<Id>();					//Id Set of Contact Records whose Sequence Number lies in valid range
    static List<Integer> validRangeList         = new List<Integer>();				//List of Valid Range of Sequence Number associated with each Account
    static List<Integer> validSeqList 	        = new List<Integer>();				//List of Valid Sequence Number of inserted records associated with each Account
    static List<Account> accountList 			= new List<Account>();				//List of Account whose records are inserted
    
    //-------------------------------------------Insert Method which will be called on Insert Trigger-------------------------------------------
    public static void insertMethod(List<Contact> triggerNew, Map<Id, Contact> triggerNewMap){

        contactUpdateList.clear();
        accountIdSet.clear();
        accountList.clear();
        
        //Initializing value of accountIdSet and accountList
        for(Contact c : triggerNew){ accountIdSet.add(c.AccountId); }        
        accountList = [SELECT Id, (SELECT LastName, Sequence_Number__c, AccountId FROM Contacts ORDER BY Id) FROM Account WHERE Id in :accountIdSet];
        List<Account> accList = [SELECT Id, (SELECT LastName, Sequence_Number__c, AccountId FROM Contacts ORDER BY Sequence_Number__c) FROM Account WHERE Id in :accountIdSet];
        
        //Settng Sequence Number of Inserted Contacts
        for(Account acc : accountList){            
            validRangeList.clear();
            validSeqList.clear();
            validSequenceSet.clear();

            for(Integer i=1; i<=acc.Contacts.size(); i++){                              //Creating list of all valid Sequence Numbers
                validRangeList.add(i);
            }            
            for(Contact c : triggerNew){                                                //Creating Id Set of contacts with valid Sequence Number
                if(c.AccountId == acc.Id && validRangeList.contains(Integer.valueOf(c.Sequence_Number__c))  && c.Sequence_Number__c.scale() ==  0){
                    validSequenceSet.add(c.Id);
                }
            }            
            //Priority 1 - Updating records whose Sequence Number lies in the valid range and are the new inserted records
            List<Contact> contactList = new List<Contact>();                            //List of contacts whose Swquence Number lies in valid range
            for(Contact c : acc.Contacts){
                if(triggerNewMap.containsKey(c.Id)  && validSequenceSet.contains(c.Id) ){
                    contactList.add(c);
                }
            }
            Contact[] tempList = new Contact[validRangeList.size()];	
            for(Contact con : contactList){
                if(tempList.get(Integer.valueOf(con.Sequence_Number__c)-1) == null){
                    tempList.add(Integer.valueOf(con.Sequence_Number__c)-1, con);
                    tempList.remove(Integer.valueOf(con.Sequence_Number__c));
                }
                else{
                    for(Integer k=Integer.valueOf(con.Sequence_Number__c) - 1; k<tempList.size(); k++){
                         if(tempList.get(k) != null){
                            tempList.get(k).Sequence_Number__c += 1;
                        }
                         else{
                            break;
                        }
                    }
                    tempList.add(Integer.valueOf(con.Sequence_Number__c)-1, con);
                    if(tempList.get(Integer.valueOf(con.Sequence_Number__c)) == null){ tempList.remove(Integer.valueOf(con.Sequence_Number__c)); }
                }
            }
            //Updating the Vaild Sequence List for the account
            for(Integer k=0; k<tempList.size(); k++){
                if(tempList.get(k) != null){
                    contactUpdateList.add(tempList.get(k));
                    validSeqList.add(Integer.valueOf(tempList.get(k).Sequence_Number__c));
                }                        
            }            
            //Priority 2 - Updating records which were already available before inserting the new records            
            Integer countPrevious = 1, countManager = 1;
            contactList.clear();
            for(Contact c : acc.Contacts){                                              //List of contacts which were available with null Sequence Number
                if( !triggerNewMap.containsKey(c.Id) && c.Sequence_Number__c == null ){
                    contactList.add(c);
                }
            }
            List<Contact> list1 = new List<Contact>();
            for(Account a : accList){       
                if(a.Id == acc.Id){
                    list1 = a.Contacts; break;
                }                        //Contact list ordered by Sequemce Number
            }                
            for(Contact c : list1){                                                     //List of contacts which were available with not null Sequence Number
                if(!triggerNewMap.containsKey(c.Id) && c.Sequence_Number__c != null){
                    contactList.add(c);
                }
            }
            for(Integer k=0; k<contactList.size(); k++){
                while(validSeqList.contains(countManager)){
                    ++countManager;
                }
                contactList.get(k).Sequence_Number__c = countManager++;
                contactUpdateList.add(contactList.get(k));
                validSeqList.add(Integer.valueOf(contactList.get(k).Sequence_Number__c));             
                if(k == contactList.size()-1){
                    countPrevious = Integer.valueOf(contactList.get(k).Sequence_Number__c) + 1;
                }
            }            
            //Priority 3 - Updating records which does not have valid sequence number and are inserted
            for(Contact con : acc.Contacts){
                if(triggerNewMap.containsKey(con.Id) && !validSequenceSet.contains(con.Id)){
                    while(validSeqList.contains(countPrevious)){
                        ++countPrevious;
                    }
                    con.Sequence_Number__c = countPrevious++;
                    contactUpdateList.add(con);
                }	  
            }	
        }
        
        update contactUpdateList;                                                       //Updating the final update list with appropriate Sequence Number
        TriggerHandler.isFirstTime = true;        
    }
    
    //-------------------------------------------Delete Method which will be called on Delete Trigger-------------------------------------------
    public static void deleteMethod(List<Contact> triggerOld){

        contactUpdateList.clear();
        accountIdSet.clear();
        accountList.clear();
        
        //Initializing value of accountIdSet and accountList
        for(Contact c : triggerOld){
            accountIdSet.add(c.AccountId);
        }
        accountList = [SELECT (SELECT Sequence_Number__c, LastName FROM Contacts ORDER BY Sequence_Number__c) FROM Account WHERE Id in :accountIdSet];
        
        //Setting the Sequence Number
        for(Account acc : accountList){
            Integer i = 0;
            for(Contact con : acc.Contacts){            
                con.Sequence_Number__c = ++i;
                contactUpdateList.add(con);            
            }
        }
        
        update contactUpdateList;                                                       //Updating the final update list with appropriate Sequence Number
        TriggerHandler.isFirstTime = true;
    }
    
    //-------------------------------------------Undelete Method which will be called on Undelete Trigger-------------------------------------------
    public static void undeleteMethod(List<Contact> triggerNew){ 

        contactUpdateList.clear();
        accountIdSet.clear();
        accountList.clear();       
        
        //Initializing value of accountIdSet and accountList
        for(Contact c : triggerNew){
            accountIdSet.add(c.AccountId);
        }                   
        accountList = [SELECT Id, (SELECT Id, Sequence_Number__c, LastName FROM Contacts ORDER BY Id) FROM Account WHERE Id in :accountIdSet];
        
        //Setting the Sequence Number of the records
        for(Account acc : accountList){            
            Integer counter = 0;
            Set<Id> idSet = new Set<Id>();												//Id Set undeleted contacts which are child of the corrosponding account
            for(Contact c : triggerNew){                                                //Setting value of idSet and counter
                if(acc.Id == c.AccountId){ 
                    ++counter;
                    idSet.add(c.Id);
                }
            }
            counter = (acc.Contacts.size() - counter);                                 //Calculating the last sequence number of the contacts associated with the Account            
            for(Contact con : acc.Contacts){
                if(idSet.contains(con.Id) ){
                    con.Sequence_Number__c = ++counter;
                    contactUpdateList.add(con);
                }                            
            }
        }
        
        update contactUpdateList;                                                      //Updating the final update list with appropriate Sequence Number
        TriggerHandler.isFirstTime = true;
    }
    
    //-------------------------------------------Update Method which will be called on Update Trigger-------------------------------------------
    public static void updateMethod(List<Contact> triggerNew, List<Contact> triggerOld, Map<Id, Contact> triggerNewMap, Map<Id, Contact> triggerOldMap){

        contactUpdateList.clear();
        accountIdSet.clear();
        accountList.clear();
        List<Contact> reparentedContactList = new List<Contact>();								        //List of reparented contacts for this parent 
        
        for(Contact c : triggerNew){                                                                    //Setting the value of Account Id Set 
            accountIdSet.add(c.AccountId);
        }                                   
        for(Contact c : triggerOld){                                                                    //Setting the value of Account Id Set
            accountIdSet.add(c.AccountId);
        }       
        //Creating a List of Accounts whose contact records are updated
        accountList = [SELECT Id, (SELECT Sequence_Number__c, LastName FROM Contacts ORDER BY Id) FROM Account WHERE Id in :accountIdSet];
        List<Account> accList = [SELECT Id, (SELECT Sequence_Number__c, LastName FROM Contacts ORDER BY Sequence_Number__c) FROM Account WHERE Id in :accountIdSet];        
        
        for(Id tempId : triggerNewMap.keySet()){                                                        //Creating a list of reparented contacts 
            if( triggerNewMap.get(tempId).AccountId != triggerOldMap.get(tempId).AccountId ){
                reparentedContactList.add(triggerNewMap.get(tempId));
            }
        }        
        //Setting sequence number of records whose parent is removed
        List<Contact> nullContactList = [SELECT Sequence_Number__c, LastName FROM Contact WHERE AccountId = null AND Id in :triggerNewMap.keySet()];
        for( Contact c : nullContactList){
            c.Sequence_Number__c = null;
            contactUpdateList.add(c);
        }
        //Setting sequence Numeber of contact records 
        for(Account acc : accountList){            
            validRangeList.clear();
            validSeqList.clear();
            validSequenceSet.clear();

            Integer flag = 0;            
            for(Integer i=1; i<=acc.Contacts.size(); i++){
                validRangeList.add(i);
            }		//Creating list of all valid Sequence Numbers            
            for(Contact c : triggerNew){ 
                if(c.AccountId == acc.Id && validRangeList.contains(Integer.valueOf(c.Sequence_Number__c)) && c.Sequence_Number__c.scale() ==  0 ){
                    validSequenceSet.add(c.Id);
                }	
            }            
            for(Contact c : triggerNew){
                if(c.AccountId == acc.Id){
                    flag = 1; break;
                }
            }            
            Set<Id> idSet = new Set<Id>();																//Id Set of records which are reparented            
            for(Contact c : reparentedContactList){
                if(c.AccountId == acc.Id){
                    idSet.add(c.Id);
                }
            }
            
            if(flag == 1){                
                //Priority 1 - Setting Sequence Number of updated records whose Sequence Number lies in valid range                
                List<Contact> contactList = new List<Contact>();										//List of records which are updated but not reparented
                for(Contact c : acc.Contacts){
                    if(triggerNewMap.containsKey(c.Id) && !idSet.contains(c.Id) && validRangeList.contains(Integer.valueOf(c.Sequence_Number__c)) && c.Sequence_Number__c.scale() ==  0){
                        contactList.add(c);
                    }
                }
                Contact[] tempListInteger = new Contact[validRangeList.size()];
                for(Integer k=0; k<contactList.size(); k++){
                    if(tempListInteger.get(Integer.valueOf(contactList.get(k).Sequence_Number__c)-1) == null){
                        tempListInteger.add(Integer.valueOf(contactList.get(k).Sequence_Number__c)-1, contactList.get(k));
                        tempListInteger.remove(Integer.valueOf(contactList.get(k).Sequence_Number__c));
                    }
                    else{
                        for(Integer m=Integer.valueOf(contactList.get(k).Sequence_Number__c) - 1; m<tempListInteger.size(); m++){
                            if(tempListInteger.get(m) != null){
                                tempListInteger.get(m).Sequence_Number__c += 1;
                                }
                            else{
                                break;
                            }
                        }
                        tempListInteger.add(Integer.valueOf(contactList.get(k).Sequence_Number__c)-1, contactList.get(k));
                        if(tempListInteger.get(Integer.valueOf(contactList.get(k).Sequence_Number__c)) == null){
                            tempListInteger.remove(Integer.valueOf(contactList.get(k).Sequence_Number__c));
                        }
                    }                    
                }
                for(Integer k=0; k<tempListInteger.size(); k++){
                    if(tempListInteger.get(k) != null){
                        contactUpdateList.add(tempListInteger.get(k));
                        validSeqList.add(Integer.valueOf(tempListInteger.get(k).Sequence_Number__c));
                    }                        
                }
                
                //Priority 2 - Setting Sequence Number of records whose updated Sequence Number is invalid and is not null and is not out of valid range
                contactList.clear();
                for(Contact c : acc.Contacts){
                    if(triggerNewMap.containsKey(c.Id) && !idSet.contains(c.Id) && !validSequenceSet.contains(c.Id) && c.Sequence_Number__c != null && c.Sequence_Number__c <= validRangeList.size()){
                        contactList.add(c);
                    }
                }
                for(Integer k=0; k<contactList.size(); k++){				
                    //Getting the old value of the record with invalid Sequence Number
                    for(Contact c : triggerOld){
                        if(c.LastName == contactList.get(k).LastName){
                            contactList.get(k).Sequence_Number__c = c.Sequence_Number__c;
                            break;
                        }
                    }
                    Integer countManager = Integer.valueOf(contactList.get(k).Sequence_Number__c);
                    while(validSeqList.contains(countManager)){
                        ++countManager;
                    }
                    contactList.get(k).Sequence_Number__c = countManager++;
                    contactUpdateList.add(contactList.get(k));
                    validSeqList.add(Integer.valueOf(contactList.get(k).Sequence_Number__c));								
                }                
                //Priority 3, 4, 5 - Setting Sequence Number of records whose Sequence Number were not updated, updated Sequence Number is null, updated Sequence Number is out of valid range
                Integer countManager = 1;                
                contactList.clear();
                List<Contact> list1 = new List<Contact>();
                for(Account a : accList){
                    if(a.Id == acc.Id){
                        list1 = a.Contacts;
                        break;
                    }
                }
                for(Contact c : list1){
                    if(!triggerNewMap.containsKey(c.Id)){
                        contactList.add(c);
                    }
                }
                for(Contact c : acc.Contacts){
                    if(triggerNewMap.containsKey(c.Id) && !idSet.contains(c.Id) && c.Sequence_Number__c == null ){
                        contactList.add(c);
                    }
                }
                for(Contact c : acc.Contacts){
                    if(triggerNewMap.containsKey(c.Id) && !idSet.contains(c.Id) && c.Sequence_Number__c > validRangeList.size() ){
                        contactList.add(c);
                    }
                }
                for(Contact c : contactList){
                    while(validSeqList.contains(countManager)){
                        ++countManager;
                    }
                    c.Sequence_Number__c = countManager++;
                    contactUpdateList.add(c);
                    validSeqList.add(Integer.valueOf(c.Sequence_Number__c));                    
                }
                //Priority 6 - Setting Sequence Number of Reparentd records                
                contactList.clear();																
                for(Contact c : acc.Contacts){                                                                      //List of reparented records
                    if(idSet.contains(c.Id) ){
                        contactList.add(c);
                    }
                }                   
                List<Contact> reverseContactList = new List<Contact>();                                             //List of reparented records in desc order
                for(Integer k=contactList.size()-1; k>=0; k--){
                    reverseContactList.add(contactList.get(k));
                }      
                Integer cnt = validRangeList.size();
                for(Contact c : reverseContactList){
                    while(validSeqList.contains(cnt)){
                        --cnt;
                    }
                    c.Sequence_Number__c = cnt--;
                    contactUpdateList.add(c);
                }
            }
            //Setting Sequence number of contact records of Account from which child are reaparented to another account and no other record is updated
            else{
                Integer countManager = 1;
                List<Contact> contactList = new List<Contact>();
                for(Account a : accList){
                    if(a.Id == acc.Id){
                        contactList = a.Contacts;
                        break;
                    }
                }
                for(Contact c : contactList){
                    c.Sequence_Number__c = countManager++;
                    contactUpdateList.add(c);
                }
            }
        } 
        
        update contactUpdateList;                                                                       //Updating the final update list with appropriate Sequence Number
        TriggerHandler.isFirstTime = true;
    }    
}