/**
**@Description: Apex Wrapper Class for integration with Box
**
**@Author: Akhil Tyagi
**@Date: 20 January 2020
**
**@ChangeLog:
**21 January 2020: Upadated WrapperWhole class.
**23 January 2020: Upadated WrapperFileList class.
***/

public class commBoxWrapper {

    //Wrapper Class for Access Token
    public class WrapperToken {
        public String  access_token{ get; set; }
        public String  refresh_token{ get; set; }
        public Integer expires_in{ get; set; }
    }

    //Wrapper Class for File/Folder details
    public class WrapperFileList {
        public String id{get; set; }
        public String name{get; set; }
        public String type{get; set; }
        public List<WrapperFileList> entries{ get; set; }

        public WrapperFileList(){ }            
    }

    //Wrapper class for list of details of all Files/Folder in current folder
    public class WrapperItems {
        public List<WrapperFileList> entries{ get; set; }
    }

    //Wrapper Calss for filel/folder and webContentLink details
    public class WrapperWhole {
        public WrapperItems item_collection{ get; set; }
        public String location{ get; set; }
    }
}