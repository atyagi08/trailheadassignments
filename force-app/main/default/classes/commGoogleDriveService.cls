/**
**@Description: Apex Service Class for integration with Google Drive
**
**@Author: Akhil Tyagi
**@Date: 07 January 2020
**
**@ChangeLog:
**
***/

public with sharing class commGoogleDriveService {

    /***************************************************************************************************************
    **@Description: Query Access Token from custom setting if available.                                          **
    **                                                                                                            **
    **@Parameter:                                                                                                 **
    **                                                                                                            **
    **@Return String with access token                                                                            **
    ***************************************************************************************************************/
    
    public static String AccessToken() {
        // System.debug('@@@AccessToken Method@@@');
        CommunityIntegrationTokens__c itc;
        User usr = [SELECT Username FROM User WHERE Id = :UserInfo.getUserId()];
        // System.debug('@@@Username ==>> ' + usr);
        List<CommunityIntegrationTokens__c> itcList = [SELECT Access_Token__c, Refresh_Token__c, Expire_Time__c FROM CommunityIntegrationTokens__c WHERE UserName__c = :usr.Username];
        if(itcList.isEmpty()) {
            return 'AuthorizationRequired';
        }
        commGoogleDriveWrapper.WrapperToken token;
        itc = itcList[0];
        String accessToken        = itc.Access_Token__c;
        String refreshToken       = itc.Refresh_Token__c;
        CommunityIntegrationKey__mdt keys = [SELECT Client_Id__c, Client_Secret__c FROM CommunityIntegrationKey__mdt WHERE MasterLabel = :usr.Username AND Cloud_Name__c = 'GoogleDrive'];
        String clientId                   = keys.Client_Id__c;
        String clientSecret               = keys.Client_Secret__c;

        if(itc.Access_Token__c !=null && System.now() < itc.Expire_Time__c) {            //When accesss token is not expired
            // System.debug('@@@Time Not Expired');
            return accessToken;
        } else if(itc.Access_Token__c !=null) {                                            //When access token is expired
            // System.debug('@@@Time Expired');
            String messageBody = 'refresh_token='+refreshToken+'&client_id='+clientId+'&client_secret='+clientSecret+'&grant_type=refresh_token';
            String endpoint = 'https://www.googleapis.com/oauth2/v4/token';
            HttpRequest request = new HttpRequest();
            request.setMethod('POST');
            request.setEndpoint(endpoint);
            request.setHeader('Content-type', 'application/x-www-form-urlencoded');
            request.setHeader('Content-length', String.valueOf(messageBody.length()));
            request.setBody(messageBody);
            request.setTimeout(60*1000);
            HttpResponse response = new Http().send(request);
            // System.debug('@@@Access Token after expire time === \n' + response.getBody());
            token = (commGoogleDriveWrapper.WrapperToken)JSON.deserialize(response.getBody(), commGoogleDriveWrapper.WrapperToken.class);
            itc.Expire_Time__c  = System.now().addSeconds(token.expires_in);
            itc.Access_Token__c = token.access_token;
            accessToken         = String.valueOf(token.access_token);
            update itc;
        }
        return accessToken;
    }

    /***************************************************************************************************************
    **@Description: Generate Access Token for first time authorization and create custom setting for it.          **
    **                                                                                                            **
    **@Parameter code: Code used to generate access token.                                                        **
    **                                                                                                            **
    **@Return String with access token                                                                            **
    ***************************************************************************************************************/

    public static String AuthUser(String code) {
        // System.debug('@@@AuthUser Method@@@');
        String accessToken = '';
        User usr = [SELECT Username FROM User WHERE Id = :UserInfo.getUserId()];
        CommunityIntegrationKey__mdt keys = [SELECT Client_Id__c, Client_Secret__c, Redirect_URl__c FROM CommunityIntegrationKey__mdt WHERE MasterLabel = :usr.Username AND Cloud_Name__c = 'GoogleDrive'];
        String clientId                   = keys.Client_Id__c;
        String clientSecret               = keys.Client_Secret__c;
        String redirectURI                = keys.Redirect_URl__c;
        // System.debug('@@@keys == >> ' + keys);

        commGoogleDriveWrapper.WrapperToken token;
        String messageBody = 'code=' + code +
        '&client_id=' + clientId +
        '&client_secret=' + clientSecret +
        '&redirect_uri=' + redirectURI +
        '&grant_type=authorization_code';
        // System.debug('@@@MessageBody == \n' + messageBody);
        HttpRequest request = new HttpRequest();                                        //First authorization without access token
        request.setMethod('POST');
        request.setEndpoint('https://accounts.google.com/o/oauth2/token');
        request.setHeader('Content-type', 'application/x-www-form-urlencoded');
        request.setHeader('Content-length', String.valueOf(messageBody.length()));
        request.setBody(messageBody);
        request.setTimeout(60*1000);
        HttpResponse response = new Http().send(request);
        // System.debug('@@@Access Token on first authorization === \n' + response.getBody());
        token = (commGoogleDriveWrapper.WrapperToken)JSON.deserialize(response.getBody(), commGoogleDriveWrapper.WrapperToken.class);
        CommunityIntegrationTokens__c itc = new CommunityIntegrationTokens__c();
        itc.UserName__c      = usr.Username;
        itc.SetupOwnerId     = UserInfo.getUserId();
        itc.Expire_Time__c   = System.now().addSeconds(token.expires_in);
        itc.Access_Token__c  = token.access_token;
        itc.Refresh_Token__c = token.refresh_token;
        insert itc;
        accessToken = String.valueOf(token.access_token);
        // System.debug('@@@Access Token ==>> ' + accessToken);
        return accessToken;
    }

    /***************************************************************************************************************
    **@Description: Method for all GET Requests.                                                                  **
    **                                                                                                            **
    **@Parameter endpoint: Endpoint for the request.                                                              **
    **@Parameter accToken: Access Token.                                                                          **
    **                                                                                                            **
    **@Return JSON String of type commGoogleDriveWrapper.WrapperWhole                                             **
    ***************************************************************************************************************/

    public Static commGoogleDriveWrapper.WrapperWhole GetResponse(String endpoint, String accToken){
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Accept', 'application/json');
        HttpResponse response = new Http().send(request);
        // System.debug('@@@GetResponse Method ResponseBody === \n'+response.getBody());
        commGoogleDriveWrapper.WrapperWhole wrapperinstance = (commGoogleDriveWrapper.WrapperWhole)JSON.deserialize(response.getBody(), commGoogleDriveWrapper.WrapperWhole.class);
        return wrapperinstance;
    }

    /***************************************************************************************************************
    **@Description: Method for all POST Requests.                                                                 **
    **                                                                                                            **
    **@Parameter endpoint: Endpoint for the request.                                                              **
    **@Parameter accToken: Access Token.                                                                          **
    **@Parameter body: Request body.                                                                              **
    **@Parameter contentType: Value of header Content-type.                                                       **
    **                                                                                                            **
    **@Return JSON String of type commGoogleDriveWrapper.WrapperFileList                                          **
    ***************************************************************************************************************/

    public Static commGoogleDriveWrapper.WrapperFileList PostResponse(String endpoint, String accToken, String contentType, String body){
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Content-type', contentType);
        request.setHeader('Content-length', String.valueOf(body.length()));
        request.setBody(body);
        request.setTimeout(60 * 1000);  
        HttpResponse response = new Http().send(request);
        // System.debug('@@@Service Class PostResponse Method === \n'+response.getBody());
        commGoogleDriveWrapper.WrapperFileList wrapperinstance = (commGoogleDriveWrapper.WrapperFileList)JSON.deserialize(response.getBody(), commGoogleDriveWrapper.WrapperFileList.class);
        return wrapperinstance;
    }

    /***************************************************************************************************************
    **@Description: Method for all DELETE Requests.                                                               **
    **                                                                                                            **
    **@Parameter endpoint: Endpoint for the request.                                                              **
    **@Parameter accToken: Access Token.                                                                          **
    **                                                                                                            **
    **@Return Integer with status code                                                                            **
    ***************************************************************************************************************/

    public Static Integer DeleteResponse(String endpoint, String accToken){
        HttpRequest request = new HttpRequest();
        request.setMethod('DELETE');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Accept', 'application/json');
        HttpResponse response = new Http().send(request);
        // System.debug('@@@Service Class DeleteResponse Method === \n'+response.getBody());
        return response.getStatusCode();
    }
}