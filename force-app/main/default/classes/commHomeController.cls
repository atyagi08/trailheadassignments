public class commHomeController {
	
    @AuraEnabled
    public static String GetImage() {
        User usr = [SELECT Id, Username, Name, FullPhotoUrl, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        //System.debug(JSON.serialize(usr));
        return JSON.serialize(usr);
    }
    
    @AuraEnabled
    public static String SetProfile(String fileBody, String fileName, String fileType, String userId) {
        fileBody   	  = fileBody.substringAfter('base64,');
        Blob flBdy 	  = EncodingUtil.base64Decode(fileBody);
        String commId = null;
        ConnectApi.BinaryInput fileUpload = new ConnectApi.BinaryInput(flBdy, fileType, fileName);  
        ConnectApi.Photo photoProfile  	  = ConnectApi.UserProfiles.setPhoto(commId, userId, fileUpload);
        return 'SUCCESS';
    }
    
    @AuraEnabled
    public static String ProfrileUrl(String userId) {
        User usr = [SELECT FullPhotoUrl FROM User WHERE Id = : userId];
        return JSON.serialize(usr);
    }
}