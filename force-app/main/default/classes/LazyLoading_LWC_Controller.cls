public without sharing class LazyLoading_LWC_Controller {
    
    public class ColumnWrapper {
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public String label;  
        @AuraEnabled
        public Schema.DisplayType type;  
        @AuraEnabled
        public Boolean sortable;  
    }

    public class ResponseWrapper{
        @AuraEnabled
        public List< sObject > recordsList;
        @AuraEnabled
        public List< ColumnWrapper > columnsList;
        @AuraEnabled
        public Integer count;
    }

    public class SelectOpts {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;

        public SelectOpts( String value, String label ) {
            this.label = label;
            this.value = value;
        }
    }

    @AuraEnabled
    public static List<SelectOpts> getObjectList() {
        
        List<SelectOpts> objectList        = new List<SelectOpts>();
        Map<string, SObjectType> objectMap = Schema.getGlobalDescribe();
        for( String key : objectMap.keySet() ) {
            objectList.add( new SelectOpts( objectMap.get(key).getDescribe().getName(), objectMap.get(key).getDescribe().getLabel() ) );
        }
        System.debug( '@@@Object List ==>> ' + objectList.size() + ' ==> \n' + objectList );
        return objectList;
    }

    @AuraEnabled
    public static List< SelectOpts > getFieldsList( String objectName ) {

        List< SelectOpts > fieldsList       = new List< SelectOpts >();
        Map<string, SObjectField> fieldsMap = Schema.getGlobalDescribe().get( objectName ).getDescribe().fields.getMap();
        for(String key : fieldsMap.keySet()){
            if( !( fieldsMap.get(key).getDescribe().getType() == Schema.DisplayType.ADDRESS ) ){
                fieldsList.add( new SelectOpts( fieldsMap.get(key).getDescribe().getName(), fieldsMap.get(key).getDescribe().getLabel() ) );
            }
        }
        System.debug( '@@@Fileds List ==>> ' + fieldsList.size() + ' ==> \n' + fieldsList );
        return fieldsList;
    }

    @AuraEnabled
    public static ResponseWrapper recordsList( String objectName, String fields ) {

        ResponseWrapper response = new ResponseWrapper();
        String query             = 'SELECT ' + fields + ' FROM ' + objectName + ' ORDER BY ID LIMIT 20';
        response.recordsList     = Database.query( query );
        // System.debug( '@@@@ Records ==> ' + response.recordsList );
        
        response.count           = Database.countQuery( 'SELECT  COUNT() FROM ' + objectName );
        // System.debug( '@@@@ Records Count ==> ' + response.count );

        response.columnsList                = new List< ColumnWrapper >();
        List < String > fieldsString        = fields.deleteWhitespace().split( ',' );
        Map<string, SObjectField> fieldsMap = Schema.getGlobalDescribe().get( objectName ).getDescribe().fields.getMap();
        for(String key : fieldsMap.keySet()){
            if( !( fieldsMap.get(key).getDescribe().getType() == Schema.DisplayType.ADDRESS ) && 
                fieldsString.contains(fieldsMap.get(key).getDescribe().getName()) 
            ){
                ColumnWrapper clm = new ColumnWrapper();
                clm.fieldName     = fieldsMap.get(key).getDescribe().getName();
                clm.label         = fieldsMap.get(key).getDescribe().getLabel();
                clm.sortable      = fieldsMap.get(key).getDescribe().isSortable();
                clm.type          = fieldsMap.get(key).getDescribe().getType();
                response.columnsList.add( clm );
            }
        }
        // System.debug( '@@@@ Columns List ==> ' + response.columnsList );
        return response;
    }

    @AuraEnabled
    public static List< SObject > LoadRecords ( String objectName, String fields, String recordId ) {

        
        String query                = 'SELECT ' + fields + ' FROM ' + objectName + ' WHERE ID > :recordId ORDER BY ID LIMIT 20';
        List< SObject > recordsList = Database.query( query );
        // System.debug( '@@@@ Records ==> ' + recordsList );
        return recordsList;
    }
}