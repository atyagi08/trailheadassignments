public virtual class GenericController{
    
    private String pageName;
    
    public String getPageName(String url)
    {
        pageName='';
        if(url.indexOf('/apex/') != -1)
        {
            if(url.indexOf('?') != -1 )
            {
                pageName=url.subString(url.indexOf('/apex/')+6,url.indexOf('?'));
            }
            else
            {
                pageName=url.subString(url.indexOf('/apex/')+6);
            }
        }
        return pageName;
    }
}