@isTest
private class NotesAndAttachmentTest {

    @isTest
    static void testMethod01(){
        Account testAccount = new Account(Name='Test Account');
		insert testAccount;
		Test.StartTest();			
            Test.setCurrentPage(Page.NotesAndAttachments);
            ApexPages.StandardController stdController = new ApexPages.StandardController(testAccount);
            NotesAndAttachmentExtension ext = new NotesAndAttachmentExtension(stdController);
            String str = ext.getbase64();
            ext.setbase64('Hello how are you');
            ext.fileName = 'Test Attachment';
            ext.acc = testAccount;
            ext.saveFile();
        	Attachment a = [SELECT Name FROM Attachment WHERE Name = 'Test Attachment' LIMIT 1];
        	System.assertEquals('Test Attachment', a.Name);
		Test.StopTest();
	}
    
    @isTest
    static void testMethod02(){
		Account testAccount = new Account(Name='Test Account');
		insert testAccount;		
		Test.StartTest();			
            Test.setCurrentPage(Page.NotesAndAttachments);
            ApexPages.StandardController stdController = new ApexPages.StandardController(testAccount);
            NotesAndAttachmentExtension ext = new NotesAndAttachmentExtension(stdController);
            ext.setbase64('Hello how are you');
            ext.acc = testAccount;
            ext.saveFile();
        	System.assertEquals(0, [SELECT Name FROM Attachment WHERE Name = 'Unit Test Attachment' LIMIT 1].size());
		Test.StopTest();
	}

    @isTest
    static void testMethod03(){
		Account testAccount = new Account(Name='Test Account');
		insert testAccount;		
		Test.StartTest();			
            Test.setCurrentPage(Page.NotesAndAttachments);
            ApexPages.StandardController stdController = new ApexPages.StandardController(testAccount);
            NotesAndAttachmentExtension ext = new NotesAndAttachmentExtension(stdController);
            ext.setbase64('Hello how are you');
            ext.fileName = 'Test Attachment';
            ext.saveFile();
        	System.assertEquals(0, [SELECT Name FROM Attachment WHERE Name = 'Unit Test Attachment' LIMIT 1].size());
		Test.StopTest();
	}
}