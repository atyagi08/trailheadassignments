public class NotesAndAttachmentExtension{	
	transient String base64;																			//String which contains the body of the file
	public String getbase64(){ return base64; }
	public void setbase64(String base64){ this.base64 = base64; }
	public transient String fileName{ get; set; }														//String which contains the name of the file	
	public Account acc;																									
	
	public NotesAndAttachmentExtension(ApexPages.standardController controller){						//Constructor
        acc = (Account)controller.getRecord();
    }
	
	public void saveFile(){																				//Method to insert he file
		Attachment a = new Attachment(parentId = acc.Id, Body =  EncodingUtil.base64Decode(base64), name = fileName);
        try{ insert a; }
        catch(DmlException e){ }        
	}
}