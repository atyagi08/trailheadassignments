/**
**@Description: Apex Service Class for integration with Box
**
**@Author: Akhil Tyagi
**@Date: 20 January 2020
**
**@ChangeLog:
**21 January 2020: Created DeleteResponse and PostResponse Method.
**23 January 2020: Upadated PostResponse Method to upload file to Box.
***/

public class commBoxService {

    /***************************************************************************************************************
    **@Description: Query Access Token from custom setting if available.                                          **
    **                                                                                                            **
    **@Parameter:                                                                                                 **
    **                                                                                                            **
    **@Return String with access token                                                                            **
    ***************************************************************************************************************/

    public static String AccessToken() {
        // System.debug('@@@AccessToken Method@@@');
        User usr = [SELECT Username FROM User WHERE Id = :UserInfo.getUserId()];
        List<CommIntegrationTknBox__c> itcList = [SELECT Access_Token__c, Refresh_Token__c, Expire_Time__c 
                                                    FROM CommIntegrationTknBox__c 
                                                    WHERE UserName__c = :usr.Username];
        if(itcList.isEmpty()) {
            return 'AuthorizationRequired';
        }
        CommIntegrationTknBox__c itc = itcList[0];
        String accessToken           = itc.Access_Token__c;
        String refreshToken          = itc.Refresh_Token__c;

        if(itc.Access_Token__c !=null && System.now() < itc.Expire_Time__c) {
            return accessToken;
        } else if(itc.Access_Token__c !=null) {
            CommunityIntegrationKey__mdt keys = [SELECT Client_Id__c, Client_Secret__c 
                                                FROM CommunityIntegrationKey__mdt 
                                                WHERE MasterLabel = :usr.Username AND Cloud_Name__c = 'Box'];

            String messageBody = 'refresh_token=' + refreshToken +
                                 '&client_id=' + keys.Client_Id__c +
                                 '&client_secret=' + keys.Client_Secret__c +
                                 '&grant_type=refresh_token';

            HttpRequest request = new HttpRequest();
            request.setMethod('POST');
            request.setEndpoint('https://api.box.com/oauth2/token');
            request.setHeader('Content-type', 'application/x-www-form-urlencoded');
            request.setBody(messageBody);
            request.setTimeout(60*1000);
            HttpResponse response = new Http().send(request);
            
            commBoxWrapper.WrapperToken token = (commBoxWrapper.WrapperToken)JSON.deserialize(response.getBody(), commBoxWrapper.WrapperToken.class);
            itc.Expire_Time__c                = System.now().addSeconds(token.expires_in);
            itc.Access_Token__c               = token.access_token;
            itc.Refresh_Token__c              = token.refresh_token;
            accessToken                       = String.valueOf(token.access_token);
            update itc;
        }
        return accessToken;
    }

    /***************************************************************************************************************
    **@Description: Generate Access Token for first time authorization and create custom setting for it.          **
    **                                                                                                            **
    **@Parameter code: Code used to generate access token.                                                        **
    **                                                                                                            **
    **@Return String with access token                                                                            **
    ***************************************************************************************************************/

    public static String AuthUser(String code) {
        // System.debug('@@@AuthUser Method@@@');
        User usr = [SELECT Username FROM User WHERE Id = :UserInfo.getUserId()];
        CommunityIntegrationKey__mdt keys = [SELECT Client_Id__c, Client_Secret__c, Redirect_URl__c
                                            FROM CommunityIntegrationKey__mdt 
                                            WHERE MasterLabel = :usr.Username AND Cloud_Name__c = 'Box'];

        String messageBody = 'code=' + code +
                             '&client_id=' + keys.Client_Id__c +
                             '&client_secret=' + keys.Client_Secret__c +
                             '&redirect_uri=' + keys.Redirect_URl__c +
                             '&grant_type=authorization_code';

        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint('https://api.box.com/oauth2/token');
        request.setHeader('Content-type', 'application/x-www-form-urlencoded');
        request.setBody(messageBody);
        request.setTimeout(60*1000);
        HttpResponse response = new Http().send(request);

        commBoxWrapper.WrapperToken token = (commBoxWrapper.WrapperToken)JSON.deserialize(response.getBody(), commBoxWrapper.WrapperToken.class);
        CommIntegrationTknBox__c itc      = new CommIntegrationTknBox__c();
        itc.UserName__c                   = usr.Username;
        itc.SetupOwnerId                  = UserInfo.getUserId();
        itc.Access_Token__c               = token.access_token;
        itc.Refresh_Token__c              = token.refresh_token;
        itc.Expire_Time__c                = System.now().addSeconds(token.expires_in);
        insert itc;
        return String.valueOf(token.access_token);
    }

    /***************************************************************************************************************
    **@Description: Method for all GET Requests.                                                                  **
    **                                                                                                            **
    **@Parameter endpoint: Endpoint for the request.                                                              **
    **@Parameter accToken: Access Token.                                                                          **
    **                                                                                                            **
    **@Return JSON String of type commBoxWrapper.WrapperWhole                                                     **
    ***************************************************************************************************************/

    public Static commBoxWrapper.WrapperWhole GetResponse(String endpoint, String accToken) {
        // System.debug('@@@GetResponse Method@@@');
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Content-Type', 'application/json');
        HttpResponse response = new Http().send(request);

        commBoxWrapper.WrapperWhole wrapperinstance = new commBoxWrapper.WrapperWhole();
        if(response.getHeader('Location') != null) {
            wrapperinstance.location = response.getHeader('Location');
        } else {
            wrapperinstance = (commBoxWrapper.WrapperWhole)JSON.deserialize(response.getBody(), commBoxWrapper.WrapperWhole.class);
        }
        return wrapperinstance;
    }

    /***************************************************************************************************************
    **@Description: Method for all DELETE Requests.                                                               **
    **                                                                                                            **
    **@Parameter endpoint: Endpoint for the request.                                                              **
    **@Parameter accToken: Access Token.                                                                          **
    **                                                                                                            **
    **@Return Integer with status code			                                                                  **
    ***************************************************************************************************************/

    public Static Integer DeleteResponse(String endpoint, String accToken) {
        // System.debug('@@@DeleteResponse Method@@@');
        HttpRequest request = new HttpRequest();
        request.setMethod('DELETE');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Content-Type', 'application/json');
        HttpResponse response = new Http().send(request);
        
        return response.getStatusCode();
    }

    /***************************************************************************************************************
    **@Description: Method for all POST Requests.                                                                 **
    **                                                                                                            **
    **@Parameter endpoint: Endpoint for the request.                                                              **
    **@Parameter accToken: Access Token.                                                                          **
    **@Parameter body: Request body.                                                                              **
    **@Parameter contentType: Value of header Content-type.                                                       **
    **                                                                                                            **
    **@Return JSON String of type commBoxWrapper.WrapperFileList                                                  **
    ***************************************************************************************************************/

    public Static commBoxWrapper.WrapperFileList PostResponse(String endpoint, String accToken, String body, String contentType) {
        // System.debug('@@@PostResponse Method@@@');
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint(endpoint);
        request.setHeader('Authorization', 'Bearer ' + accToken);
        request.setHeader('Content-type', contentType);
        request.setHeader('Content-length', String.valueOf(body.length()));
        if(contentType.equals('application/json')) {
            request.setBody(body);
        } else {
            request.setBodyAsBlob(EncodingUtil.base64Decode(body));
        }
        request.setTimeout(60 * 1000);  
        HttpResponse response = new Http().send(request);

        commBoxWrapper.WrapperFileList wrapperinstance = (commBoxWrapper.WrapperFileList)JSON.deserialize(response.getBody(), commBoxWrapper.WrapperFileList.class);
        return wrapperinstance;
    }
}