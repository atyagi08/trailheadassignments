public class ComponentController {
	
    //Required Variables-----------------------------------------
    public String objectNameController{ get; set; }
    public List<SelectOption> fieldNamesController{ get; set; }    
    public Integer offset{ get; set; }
    public Integer recordPerPage{ get; set; }
    public Integer numberOfRecords{ get; set; }
    public String characterForSort{ get; set; }
    public String passQuery{ get; set; }
    public String sortField{ get; set; }
    public String toggleCounter{ get; set; }
    public Id RecordToDelete{ get; set; }    
    public Id recordId{ get; set; }
    public Boolean checked{ get; set; }
    public Map<Id, Boolean> checkboxState{ get; set; }
    
    
    //Private Variables-----------------------------------------
    private String query;   
    private String recordsToPass;
	private String sortDir;
	private Integer counter;
 	private List<String> columnList;    
    
    //Constructor-----------------------------------------
    public ComponentController(){ 
    	offset = 0;
        recordPerPage = 5;
        characterForSort = '';        
        checkboxState = new Map<Id, Boolean>();
        passQuery = '*';
        checked = false;
        sortDir = '';
        counter = 1;
        toggleCounter = '0';
    }
    
    //Method for query generation-----------------------------------------
    private String QueryGenerator(){
        
        String str;
        for(Integer i=0; i<fieldNamesController.size(); i++){
            if( i == 0 )
                str = ( 'SELECT ' + fieldNamesController.get(i).getValue() );
            else
                str += ( ', ' + fieldNamesController.get(i).getValue() );
        }
        str += ' FROM ' + objectNameController;
        return str;
    }
    
    //Method for column generation-----------------------------------------
    private List<String> columnListGenerator(){
        List<String> colList = new List<String>();
        for(SelectOption soption : fieldNamescontroller){
            colList.add(soption.getValue());
        }
        return colList;
    }
    
    //Method for getting records to dispay in the table-----------------------------------------
    public List<sObject> getretrieveRecords(){
        query = QueryGenerator();
        List<sObject> objList;
        if(checkboxState.size() == 0 ){
            objList = Database.query('SELECT Id FROM ' + objectNameController);
            numberOfRecords = objList.size();
            sortField = fieldNamesController.get(0).getValue();
            for(sObject temp : objList){
                checkboxState.put(temp.iD, false);
            }
        }        
        query += (' WHERE '+ sortField +' LIKE \''+characterForSort+'%\' ORDER BY '+ sortField +' LIMIT ' + recordPerPage + ' OFFSET ' + offset);
        if(toggleCounter == '1'){
            toggleCounter = '0';
            return toggleSort();
        }
        return Database.query(query);
    }
    
    //Method for Sort-----------------------------------------
    public List<sObject> toggleSort(){
        query = QueryGenerator();
       	sortDir = sortDir.equals('ASC') ? 'DESC' : 'ASC';
        String str = (sortField + ' ' + sortDir);
        offset = 0;
        query += (' WHERE '+ sortField +' LIKE \''+characterForSort+'%\' ORDER BY '+ str +' LIMIT ' + recordPerPage + ' OFFSET ' + offset);        
        return Database.query(query);
    }
    
    //Method for getting columns to display in the table-----------------------------------------
    public List<String> getretrieveColumns() {
        columnList = columnListGenerator();
        return columnList;
    }
    
    //Method on New Record Button-----------------------------------------
    public PageReference newRecord(){
        sObject testRec = Database.query('Select Id from '+ objectNameController +' Limit 1');
		String keyPrefix = String.valueOf(testRec.Id).subString(0, 3);
        PageReference page = new  PageReference('/'+keyPrefix+'/e');
        page.setRedirect(true);
        return page;
    }
    
    //Method on Delete Selected Button-----------------------------------------
    public void deleteSelectedRecords(){
        List<sObject> recList = new List<sObject>();
        Set<Id> idSet = new Set<Id>();
        for(Id temp : checkboxState.keySet()){
            if(checkboxState.get(temp) == true){
        		idSet.add(temp);
            }
        }
        for(sObject obj : Database.query('SELECT Id FROM '+objectNameController+' WHERE Id in :idSet')){
            recList.add(obj);
        }
        checked = false;
		delete recList;
    }
    
    //Method which directs to CSV download page-----------------------------------------
    public PageReference newRecordCSV(){
        PageReference page;
        passQuery += ('**'+objectNameController+'***');
        for(Integer i=0; i<fieldNamesController.size(); i++){
            if( i == 0 )
                passQuery += ( fieldNamesController.get(i).getValue() );
            else
                passQuery += ( ', ' + fieldNamesController.get(i).getValue() );
        }
        passQuery += '****';
        page = new  PageReference('/apex/csvDownloadPage?records='+passQuery);
        page.setRedirect(true);
        return page;
    }
    
    //Method for getting number of Records on Alphabet Filter-----------------------------------------
    public void NumerOfRecordsOnFilter(){
        List<sObject> objList = Database.query('SELECT Id FROM '+ objectNameController + ' WHERE '+ sortField +' LIKE \''+characterForSort+'%\'');
        numberOfRecords = objList.size();
    }
    
    //Method on header Checkbox-----------------------------------------
    public void headerCheckboxMethod(){
        String str = (sortField + ' ' + sortDir);
        List<sObject> objList = Database.query('SELECT Id FROM '+ objectNameController + ' WHERE '+ sortField +' LIKE \''+characterForSort+'%\' ORDER BY '+ str +' LIMIT ' + recordPerPage + ' OFFSET ' + offset);
        if( checked ){
            passQuery = '*';
            for(sObject temp : objList){
                checkboxState.remove(temp.Id);
                checkboxState.put(temp.Id, true);
                passQuery += (String.valueOf(temp.Id)+',');
            }
            checked = true;
        }
        else{
            for(sObject temp : objList){
                checkboxState.remove(temp.Id);
                checkboxState.put(temp.Id, false);
                passQuery = passQuery.remove(String.valueOf(recordId)+',');
            }
            checked = false;
        }
    }
    
    //Method to maintain Checkbox State-----------------------------------------
    public void checkboxStateMethod(){
        String str = (sortField + ' ' + sortDir);
        List<sObject> objList = Database.query('SELECT Id FROM '+ objectNameController + ' WHERE '+ sortField +' LIKE \''+characterForSort+'%\' ORDER BY '+ str +' LIMIT ' + recordPerPage + ' OFFSET ' + offset);
        if( checkboxState.get(recordId) == false ){ passQuery = passQuery.remove(String.valueOf(recordId)+','); }
        else{ passQuery += (String.valueOf(recordId)+','); }
        
        for(sObject tempId : objList){
            if( checkboxState.get(tempId.Id) == false ){
                checked = false;
                break;
            }
            else{ checked = true; }
        }
    }    
    
    //Method on Delete Action-----------------------------------------
    public void deleteRecord(){
        sObject obj = Database.query('SELECT Id FROM '+objectNameController+' WHERE Id = \''+RecordToDelete+'\'');           
        delete obj;
        
    }

    //Pagination methods-----------------------------------------
    //First Page-----------------------------------------
    public void Firstpage(){
        offset = 0;
        String str = (sortField + ' ' + sortDir);
        List<sObject> objList = Database.query('SELECT Id FROM '+ objectNameController + ' WHERE '+ sortField +' LIKE \''+characterForSort+'%\' ORDER BY '+ str +' LIMIT ' + recordPerPage + ' OFFSET ' + offset);
        for(sObject temp : objList){
            if(checkboxState.get(temp.Id) == false){
                checked = false;
                break;
            }
            checked = true;
        }
    }
    //Previous Page-----------------------------------------
	public void Previouspage(){ 
        offset -= recordPerPage;
    	String str = (sortField + ' ' + sortDir);
        List<sObject> objList = Database.query('SELECT Id FROM '+ objectNameController + ' WHERE '+ sortField +' LIKE \''+characterForSort+'%\' ORDER BY '+ str +' LIMIT ' + recordPerPage + ' OFFSET ' + offset);
        for(sObject temp : objList){
            if(checkboxState.get(temp.Id) == false){
                checked = false;
                break;
            }
            checked = true;
        }
    }
    //Next Page-----------------------------------------
    public void Nextpage(){
        offset += recordPerPage;
        String str = (sortField + ' ' + sortDir);
        List<sObject> objList = Database.query('SELECT Id FROM '+ objectNameController + ' WHERE '+ sortField +' LIKE \''+characterForSort+'%\' ORDER BY '+ str +' LIMIT ' + recordPerPage + ' OFFSET ' + offset);
        for(sObject temp : objList){
            if(checkboxState.get(temp.Id) == false){
                checked = false;
                break;
            }
            checked = true;
        }
    }
    //Last page-----------------------------------------
    public void Lastpage(){
        if((math.mod(numberOfRecords, recordPerPage)) == 0){
            offset = ((numberOfRecords / recordPerPage) * recordPerPage) - recordPerPage;
        }
        else{
            offset = (numberOfRecords / recordPerPage) * recordPerPage;
        }
        String str = (sortField + ' ' + sortDir);
        List<sObject> objList = Database.query('SELECT Id FROM '+ objectNameController + ' WHERE '+ sortField +' LIKE \''+characterForSort+'%\' ORDER BY '+ str +' LIMIT ' + recordPerPage + ' OFFSET ' + offset);
        for(sObject temp : objList){
            if(checkboxState.get(temp.Id) == false){
                checked = false;
                break;
            }
            checked = true;
        }
    }
    //Method for setting offset on slecting table size-----------------------------------------
    public void setOffset(){
        offset = 0;
    }
    
    //Boolean pagination methods-----------------------------------------
	//Has Previous Page-----------------------------------------
    public Boolean HasPreviousPage{ 
        get{ 
        	if(offset == 0 ){
            	return true;
        	}
        	return false;
          }
    
        set;
 	}
    //Has Next Page-----------------------------------------
    public Boolean HasNextPage{ 
        get{ 
        	if( (offset + recordPerPage) <= numberOfRecords  ){
            	return false;
        	}
        	return true;
          }
        set;
 	}

    
}