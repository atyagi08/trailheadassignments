@isTest
public with sharing class EmailRecordInsertionTestClass {
    
    @isTest
    static void Method01(){
        Test.startTest();
        Messaging.InboundEmail email       = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.plainTextBody  = 'Contact#LastName#Test_Run_Contact#Department#IT#Testing_Boolean__c#True##Account#Name#Test_Run_Account#Phone#7894561360';
        email.fromAddress    = 'atyagi.sre@gmail.com';
        EmailRecordInsertion emRecInsert = new EmailRecordInsertion();
        emRecInsert.handleInboundEmail(email, envelope);
        
        Contact con = [SELECT LastName, Department FROM Contact WHERE LastName = 'Test_Run_Contact'];
        Account acc = [SELECT Name, Phone FROM Account WHERE Name = 'Test_Run_Account'];
        System.assertEquals('Test_Run_Contact', con.LastName);      System.assertEquals('IT', con.Department);
        System.assertEquals('Test_Run_Account', acc.Name);          System.assertEquals('7894561360', acc.Phone);
        Test.stopTest();
    }

    @isTest
    static void Method02(){
        Test.startTest();
        Messaging.InboundEmail email       = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.plainTextBody  = 'Contactt#LastName#Test_Run_Contact#Department#IT#Testing_Boolean__c#True##Account#Name#Test_Run_Account#Phone#7894561360';
        email.fromAddress    = 'atyagi.sre@gmail.com';
        EmailRecordInsertion emRecInsert = new EmailRecordInsertion();
        emRecInsert.handleInboundEmail(email, envelope);
        
        List<Contact> con = [SELECT LastName, Department FROM Contact WHERE LastName = 'Test_Run_Contact'];
        Account acc = [SELECT Name, Phone FROM Account WHERE Name = 'Test_Run_Account'];
        System.assertEquals(0, con.size());
        System.assertEquals('Test_Run_Account', acc.Name);
        System.assertEquals('7894561360', acc.Phone);
        Test.stopTest();
    }

    @isTest
    static void Method03(){
        Test.startTest();
        Messaging.InboundEmail email       = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.plainTextBody  = 'Contact#LastName#Test_Run_Contact#Departmenttt#IT#Testing_Boolean__c#True##Account#Name#Test_Run_Account#Phone#7894561360';
        email.fromAddress    = 'atyagi.sre@gmail.com';
        EmailRecordInsertion emRecInsert = new EmailRecordInsertion();
        emRecInsert.handleInboundEmail(email, envelope);
        
        Contact con = [SELECT LastName, Department FROM Contact WHERE LastName = 'Test_Run_Contact'];
        Account acc = [SELECT Name, Phone FROM Account WHERE Name = 'Test_Run_Account'];
        System.assertEquals('Test_Run_Contact', con.LastName);      System.assertEquals(null, con.Department);
        System.assertEquals('Test_Run_Account', acc.Name);          System.assertEquals('7894561360', acc.Phone);
        Test.stopTest();
    }
    
    @isTest
    static void Method04(){
        Test.startTest();
        Messaging.InboundEmail email       = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.plainTextBody  = 'Contact#LastName#Test_Run_Cont#Testing_Boolean__c#True#Testing_Date__c#2019-11-14#Testing_DateTime__c#2019-11-14 01:02:03##Account#Name#Test_Run_Account#Phone#7894561360';
        email.fromAddress    = 'atyagi.sre@gmail.com';
        EmailRecordInsertion emRecInsert = new EmailRecordInsertion();
        emRecInsert.handleInboundEmail(email, envelope);
        
        Contact con = [SELECT LastName, Testing_Boolean__c, Testing_Date__c, Testing_DateTime__c FROM Contact WHERE LastName = 'Test_Run_Con'];
        Account acc = [SELECT Name, Phone FROM Account WHERE Name = 'Test_Run_Account'];
        System.assertEquals('Test_Run_Cont', con.LastName);
        System.assertEquals(Date.valueOf('2019-11-14'), con.Testing_Date__c);
        System.assertEquals(Datetime.valueOf('2019-11-14 01:02:03'), con.Testing_DateTime__c);
        System.assertEquals(true, con.Testing_Boolean__c);
        System.assertEquals('Test_Run_Account', acc.Name);
        System.assertEquals('7894561360', acc.Phone);
        Test.stopTest();
    }
    
    @isTest
    static void Method05(){
        Test.startTest();
        Messaging.InboundEmail email       = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.plainTextBody  = 'Contact#LastName#Test_Run_Con#Testing_Boolean__c#hello#Testing_Date__c#2019/11/14#Testing_DateTime__c#2019-11-1401:02:03##Account#Name#Test_Run_Account#Phone#7894561360';
        email.fromAddress    = 'atyagi.sre@gmail.com';
        EmailRecordInsertion emRecInsert = new EmailRecordInsertion();
        emRecInsert.handleInboundEmail(email, envelope);
        
        Contact con = [SELECT LastName, Testing_Boolean__c, Testing_Date__c, Testing_DateTime__c FROM Contact WHERE LastName = 'Test_Run_Con'];
        Account acc = [SELECT Name, Phone FROM Account WHERE Name = 'Test_Run_Account'];
        System.assertEquals('Test_Run_Con', con.LastName);
        System.assertEquals(null, con.Testing_Date__c);
        System.assertEquals(null, con.Testing_DateTime__c);
        System.assertEquals(false, con.Testing_Boolean__c);
        System.assertEquals('Test_Run_Account', acc.Name);
        System.assertEquals('7894561360', acc.Phone);
        Test.stopTest();
    }

    @isTest
    static void Method06(){
        Test.startTest();
        Messaging.InboundEmail email       = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.plainTextBody  = 'Contact#Testing_Boolean__c#True#Testing_Date__c#2019-11-14##Account#Name#Test_Run_Account#Phone#7894561360';
        email.fromAddress    = 'atyagi.sre@gmail.com';
        EmailRecordInsertion emRecInsert = new EmailRecordInsertion();
        emRecInsert.handleInboundEmail(email, envelope);
        
        List<Contact> con = [SELECT LastName FROM Contact WHERE LastName = 'Test_Run_Contact'];
        Account acc = [SELECT Name, Phone FROM Account WHERE Name = 'Test_Run_Account'];
        System.assertEquals(0, con.size());
        System.assertEquals('Test_Run_Account', acc.Name);
        System.assertEquals('7894561360', acc.Phone);
        Test.stopTest();
    }
}