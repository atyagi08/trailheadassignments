global with sharing class CSVIterator implements Iterator<String>, Iterable<String>{

    private String m_CSVData;                                   //fileBody

    public CSVIterator(String fileData){                        //Constructor
        m_CSVData = fileData;
    }

    global Boolean hasNext(){                                   //Implementation for hasNext Method
        return m_CSVData.length() > 1 ? true : false;
    }

    global String next(){                                       //Implementation for next method
        String row = '';
        Integer i = 1;
        while(i<=200 && !String.isEmpty(m_CSVData)){            //Getting 200 records from the file each time
            row += (m_CSVData.substringBefore('\n') + '\n');
            m_CSVData = m_CSVData.substringAfter('\n');
            i++;
        }
        return row;
    }

    global Iterator<String> Iterator(){                         //Implemetation of Iterator method
        return this;   
    }
}