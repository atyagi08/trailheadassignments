public class TestCustomLookupController {

    public class SelectOpts {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;

        public SelectOpts( String value, String label ) {
            this.label = label;
            this.value = value;
        }
    }

    @AuraEnabled
    public static List<SelectOpts> getObjectList() {

        List<SelectOpts> objectsList = new List<SelectOpts>{ new SelectOpts( '', '----None----' ) };
        Map<string, SObjectType> objectNamesMap = Schema.getGlobalDescribe();
        for( String key : objectNamesMap.keySet() ) {
            objectsList.add( new SelectOpts( objectNamesMap.get(key).getDescribe().getName(), objectNamesMap.get(key).getDescribe().getLabel() ) );
        }
        System.debug( '@@@Object List ==>> ' + objectsList.size() + ' ==> \n' + objectsList );
        return objectsList;
    }

    @AuraEnabled
    public static List<SelectOpts> getFieldsList( String objectName ) {

        List<SelectOpts> fieldsList = new List<SelectOpts>{ new SelectOpts( '', '----None----' ) };
        Map<string, SObjectField> fieldNamesMap = Schema.getGlobalDescribe().get( objectName ).getDescribe().fields.getMap();
        for(String key : fieldNamesMap.keySet()){
            if( !( fieldNamesMap.get(key).getDescribe().getType() == Schema.DisplayType.ADDRESS ) ){
                fieldsList.add( new SelectOpts( fieldNamesMap.get(key).getDescribe().getName(), fieldNamesMap.get(key).getDescribe().getLabel() ) );
            }
        }
        System.debug( '@@@Fields List ==>> ' + fieldsList.size() + ' ==> \n' + fieldsList );
        return fieldsList;
    }

}