public class MyPaginationController {

    public Integer offset{ get; set; }
    public Integer recordPerPage{ get; set; }
    public Integer numberOfRecords{ get; set; }
    public String characterForSort{ get; set; }    
    
    private String query;
    
    public MyPaginationController(){
        offset = 0;
        recordPerPage = 10;
        characterForSort = '';
        numberOfRecords = [SELECT COUNT() FROM Account];
    }
    
    public List<Account> getAccounts(){        
        query = 'SELECT Id, Name FROM Account WHERE Name LIKE \'' + characterForSort + '%\' ORDER BY Name LIMIT ' + recordPerPage + ' OFFSET ' + offset;        
        List<Account> accountRecords = Database.query(query);
        return accountRecords;
    }
    
    public void Firstpage(){ offset = 0; }    
    
    public void Previouspage(){ offset -= recordPerPage; }
    
    public void Nextpage(){ offset += recordPerPage; }
    
    public void Lastpage(){
        offset = (((numberOfRecords)/recordPerPage)*recordPerPage);
    }
    
    public Boolean HasPreviousPage{ 
        get{ 
        	if(offset == 0 ){
            	return true;
        	}
        	return false;
          }
        set;
 	}
    
    public Boolean HasPNextPage{ 
        get{ 
        	if(offset == (((numberOfRecords)/recordPerPage)*recordPerPage) ){
            	return true;
        	}
        	return false;
          }
        set;
 	}

    
}