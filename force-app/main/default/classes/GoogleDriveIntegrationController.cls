public with sharing class GoogleDriveIntegrationController {

    //Public Variables-------------------------------------------------------------------------------------------------
    public Boolean          loginButton{ get; set; }                                        //Variable to render Table
    public String           fileName{ get; set; }                                           //Uploaded file name
    public transient String fileBody{ get; set; }                                           //Uploaded file body
    public String           fileType{ get; set; }                                           //Uploaded file type
    public String           fileIdParam{ get; set; }                                        //Id of file to be downloaded
    public String           fileNameParam{ get; set; }                                      //Name of file to be downloaded
    public String           fileMimeTypeParam{ get; set; }                                  //MimeType of file to be downloaded
    public String           directoryIdParam{ get; set; }                                   //Id of folder to open
    public String           directoryNameParam{ get; set; }                                 //Name of folder to open
    public String           deleteFileId{ get; set; }                                       //Id of file/folder to be deleted
    public String           folderName{ get; set; }                                         //Created folder name
    public List<GoogleDriveIntegrationWrapper.WrapperFileList>  wrapperList{ get; set; }    //List of files and folders in current folder
    public List<GoogleDriveIntegrationWrapper.WrapperDirectory> directoryList{ get; set; }  //List of folders

    //Private Variables------------------------------------------------------------------------------------------------
    private String code ;
    private String accToken;
    private GoogleDriveIntegrationWrapper.WrapperWhole wrapperinstance;

    //Constructor------------------------------------------------------------------------------------------------------
    public GoogleDriveIntegrationController(){
        loginButton         = true;
        fileIdParam         = '';
        fileNameParam       = '';
        fileMimeTypeParam   = '';
        directoryIdParam    = '';
        directoryNameParam  = '';
        deleteFileId        = '';
        folderName          = 'New Folder';
        wrapperList         = new List<GoogleDriveIntegrationWrapper.WrapperFileList>();
        directoryList       = new List<GoogleDriveIntegrationWrapper.WrapperDirectory>();
    }

    //Method to authorize the user-------------------------------------------------------------------------------------
    public PageReference DriveAuth(){
        Integration_Key__mdt keys = [Select Client_Id__c, Redirect_URl__c FROM Integration_Key__mdt WHERE MasterLabel = 'GDrive Keys' ];
        PageReference page = new PageReference(GoogleDriveAuthUri (keys.Client_Id__c, keys.Redirect_URl__c));
        return page ;
    }

    //Method to create url for authorization page----------------------------------------------------------------------
    public String GoogleDriveAuthUri(String Clientkey, String redirect_uri){
        String client_id = EncodingUtil.urlEncode(Clientkey,'UTF-8');
        String uri       = EncodingUtil.urlEncode(redirect_uri,'UTF-8');
        String authuri   = '';
        authuri = 'https://accounts.google.com/o/oauth2/auth?'+
        'client_id='+client_id+
        '&response_type=code'+
        '&scope=https://www.googleapis.com/auth/drive'+
        '&redirect_uri='+uri+
        '&state=security_token%3D138r5719ru3e1%26url%3Dhttps://oa2cb.example.com/myHome&'+
        'access_type=offline';
        return authuri;
    }

    //Method will run on page load-------------------------------------------------------------------------------------
    public void PageLoad(){
        code = ApexPages.currentPage().getParameters().get('code');
        if(code != '' && code != null){                             //Get the access token once we have code
            AccessToken();
        }
    }

    //Method to get access token---------------------------------------------------------------------------------------
    public void AccessToken(){
        loginButton     = false;
        try{
            accToken = GoogleDriveIntegrationService.AccessToken(code);
            // System.debug('@@@Access Token === ' + accToken);
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    //Mehtod to get list of file and folders on first login------------------------------------------------------------
    public void RecordsList(){
        if(code != '' && code != null){
            String endpoint = 'https://www.googleapis.com/drive/v3/files?q=\'root\'+in+parents&orderBy=folder';
            try{
                wrapperinstance = GoogleDriveIntegrationService.GetResponse(endpoint, accToken);
                for(GoogleDriveIntegrationWrapper.WrapperFileList w : wrapperinstance.files){
                    wrapperList.add(w);
                }
                GoogleDriveIntegrationWrapper.WrapperDirectory wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
                wD.name   = 'Home';
                wD.selfId = 'root';
                directoryList.add(wD);
            }
            catch(Exception e){
                // System.debug(e.getMessage());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
        }
    }

    //Mehtod to open folder or download file---------------------------------------------------------------------------
    public Pagereference ClickOnFileFolder(){
        Pagereference page;
        GoogleDriveIntegrationWrapper.WrapperFileList wObject;
        if(fileMimeTypeParam.equals('application/vnd.google-apps.folder')){                     //Open folder
            String endpoint = 'https://www.googleapis.com/drive/v3/files?q=\''+fileIdParam+'\'+in+parents&orderBy=folder';
            try{
                wrapperinstance = GoogleDriveIntegrationService.GetResponse(endpoint, accToken);
                wrapperList.clear();
                for(GoogleDriveIntegrationWrapper.WrapperFileList w : wrapperinstance.files){
                    wrapperList.add(w);
                }
                GoogleDriveIntegrationWrapper.WrapperDirectory wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
                wD.name   = fileNameParam;
                wD.selfId = fileIdParam;
                directoryList.add(wD);
            }
            catch(Exception e){
                // System.debug(e.getMessage());
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
        }
        else{                                                                                   //Download file
            String endpoint = 'https://www.googleapis.com/drive/v3/files/'+fileIdParam+'?fields=webContentLink';
            try{
                wrapperinstance = GoogleDriveIntegrationService.GetResponse(endpoint, accToken);
                page = new Pagereference(wrapperinstance.webContentLink);
            }
            catch(Exception e){
                // System.debug(e.getMessage());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to download file.'));
            }
        }
        return page;
    }

    //Method to travers between folders by clicking thier name---------------------------------------------------------
    public void DirectoryMove(){
        Pagereference page;
        Integer flag = 0;
        for(Integer i=0; i<directoryList.size(); i++){
            if(flag == 1){
                directoryList.remove(i);
                if(i == directoryList.size()){
                    break;
                }
                --i;
            }
            if(directoryIdParam.equals(directoryList.get(i).selfId)){
                flag = 1;
            }
        }
        String endpoint = 'https://www.googleapis.com/drive/v3/files?q=\''+directoryIdParam+'\'+in+parents&orderBy=folder';
        try{
            wrapperinstance = GoogleDriveIntegrationService.GetResponse(endpoint, accToken);
            wrapperList.clear();
            for(GoogleDriveIntegrationWrapper.WrapperFileList w : wrapperinstance.files){
                wrapperList.add(w);
            }
        }
        catch(Exception e){
            // System.debug(e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    //Mehtod to delete file--------------------------------------------------------------------------------------------
    public void DeleteFile(){
        String endpoint = 'https://www.googleapis.com/drive/v3/files/'+deleteFileId;
        try{
            Integer statusCode = GoogleDriveIntegrationService.DeleteResponse(endpoint, accToken);
            if(statusCode == 204){
                for(Integer i=0; i<wrapperList.size(); i++){
                    if(deleteFileId.equals(wrapperList.get(i).id)){
                        wrapperList.remove(i);
                        break;
                    }
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to delete file. Status Code: '+statusCode));
            }
        }
        catch (Exception e){
            // System.debug(e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    //Method to create folder------------------------------------------------------------------------------------------
    public void CreateFolder(){
        GoogleDriveIntegrationWrapper.WrapperDirectory wD = directoryList.get(directoryList.size()-1);
        String endpoint = 'https://www.googleapis.com/drive/v3/files';
        String contentType = 'application/json';
        if(folderName.equals('')){
            folderName = 'Untitled';
        }
        String body = '{\"mimeType\": \"application/vnd.google-apps.folder\",\"name\": \"'+folderName+'\",\"parents\": [\"'+wD.selfId+'\"]}';
        GoogleDriveIntegrationWrapper.WrapperFileList wrapper;
        try{
            wrapper = GoogleDriveIntegrationService.PostResponse(endpoint, accToken, contentType, body);
            System.debug('@@@wrapper' + wrapper);
            for(Integer i=0; i<wrapperList.size(); i++){
                if(!wrapperList.get(i).mimeType.equals('application/vnd.google-apps.folder')){
                    wrapperList.add(i, wrapper);
                    break;
                }
            }
            if(wrapperList.size() == 0){
                wrapperList.add(wrapper);
            }
            folderName = 'New Folder';
        }
        catch(Exception e){
            // System.debug(e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    //Method to upload file--------------------------------------------------------------------------------------------
    public void FileUpload(){
        GoogleDriveIntegrationWrapper.WrapperDirectory wD = directoryList.get(directoryList.size()-1);
        String boundary = '----------9889464542212';
        String delimiter = '\r\n--' + boundary + '\r\n';
        String close_delim = '\r\n--' + boundary + '--';
        String bodyEncoded = EncodingUtil.base64Encode(Blob.valueOf(fileBody));
        String endpoint = 'https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart';
        String contentType = 'multipart/mixed; boundary=\"' + boundary + '\"';
        String body = '';
        body = delimiter + 
        'Content-Type: application/json\r\n\r\n' +
        '{\"name\": \"' + filename + '\",' + '\"mimeType\": \"' + filetype + '\",' + '\"parents\":[\"'+ wD.selfId +'\"]}' +
        delimiter +
        'Content-Type: ' + filetype + '\r\n' +
        'Content-Transfer-Encoding: base64\r\n' + '\r\n' +
        bodyEncoded +
        close_delim;
        GoogleDriveIntegrationWrapper.WrapperFileList wrapper;
        try {
            wrapper = GoogleDriveIntegrationService.PostResponse(endpoint, accToken, contentType, body);
            wrapperList.add(wrapper);
        }
        catch (Exception e) {
            // System.debug(e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }
}