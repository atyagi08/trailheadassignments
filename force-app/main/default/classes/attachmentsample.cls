public class attachmentsample {

    public attachmentsample(ApexPages.StandardController controller){}
    
    public Attachment myfile;
    public List<Attachment> attachList{ get; set; }
    
    public Attachment getmyfile(){
        myfile = new Attachment();
        return myfile;
    }
   
    public Pagereference Savedoc(){
        String accid = System.currentPagereference().getParameters().get('id');
        Attachment a = new Attachment(parentId = accid, name=myfile.name, body = myfile.body);
        PageReference page = new PageReference('https://playful-goat-j284gd-dev-ed.my.salesforce.com/'+accid); 
        // insert the attachment
        insert a;
        return page;
    }
    
    public void SavedocMultiple(){
        System.debug('In SavedocMultiple Method =========== ');
        System.debug(attachList[0].name);
        //List<Attachment> attList = new List<Attachment>();
        //String accid = System.currentPagereference().getParameters().get('id');
        //for(Attachment at : attachList){
        //    Attachment a = new Attachment(parentId = accid, name=at.name, body = at.body);
        //    attList.add(a);
        //}
        //PageReference page = new PageReference('https://playful-goat-j284gd-dev-ed.my.salesforce.com/'+accid);         
        // insert the attachment
        //insert attList;
    }
}