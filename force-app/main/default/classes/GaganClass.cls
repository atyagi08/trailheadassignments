public class GaganClass{

    // Variables required for Sorting.
    public String soql {get;set;}
    public List <Account> AccountList1 = New List <Account>();
    public String soqlsort {get;set;}
    public List <Account> AccountList2 = New List <Account>();
    public Integer Size{get; set;}
    public List<SelectOption> paginationoption{get; set;}
 
    // Method for Constructor is used for Test Class.
    public GaganClass(){
        Size = 10;
        paginationoption = new List<SelectOption>();
        paginationoption.add(new SelectOption('5', '5'));
        paginationoption.add(new SelectOption('10', '10'));
        paginationoption.add(new SelectOption('15', '15'));
        paginationoption.add(new SelectOption('20', '20'));       
    }
	
    //List of SelectOptions for Page Size
    public List<SelectOption> getPaginationValue(){ return paginationoption; }
    
	// List used in to display the table in VF page.
	public List<Account> getAccounts() { return con.getRecords(); }

	// instantiate the StandardSetController from a query locator
	public ApexPages.StandardSetController con {
		get {
			if(con == null) {
				// String Query to have a list of cases for a respective End-user.
				soql = 'SELECT Name, Industry, Phone, Description FROM Account';

				// Passing the String array to a list with Selected field sorting.
				AccountList1 = Database.query(soql + ' order by ' + sortField + ' ' + sortDir ); 

				// setting values of List in StandardSetController.
				con = new ApexPages.StandardSetController(AccountList1);

				// sets the number of records in each page set
				con.setPageSize(Size);
			}
			return con;
        }
        set;
    }

    // indicates whether there are more records after the current page set.
    public Boolean hasNext { get { return con.getHasNext(); } set; }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }
    
    // returns the page size of the current page set
    public Integer pageSize { get { return con.getPageSize(); } set; }

    // returns the page number of the current page set
    public Integer pageNumber { get { return con.getPageNumber(); } set; }
    
    

    // returns the first page of records
    public void first() { con.first(); }

    // returns the last page of records
    public void last() { con.last(); }

    // returns the previous page of records
    public void previous() { con.previous(); }

    // returns the next page of records
    public void next() { con.next(); }
    
    //return pageSize
    public void acnt(){ con.setPageSize(Size); }
    
   //Toggles the sorting of query from asc<-->desc
    public void toggleSort() {
		// simply toggle the direction
		sortDir = sortDir.equals('asc') ? 'desc' : 'asc';

		// run the query again for sorting other columns
		soqlsort = 'SELECT Name, Industry, Phone, Description FROM Account'; 

		// Adding String array to a List array
		AccountList2 = Database.query(soqlsort + ' order by ' + sortField + ' ' + sortDir);
        
		// Adding Caselist to Standard Pagination controller variable
		con = new ApexPages.StandardSetController(AccountList2);
		con.setPageSize(Size);
    }

    // the current sort direction. defaults to asc
    public String sortDir {
        // To set a Direction either in ascending order or descending order.
        get{ 
			if (sortDir == null){  
				sortDir = 'asc'; 
			} 
			return sortDir;
		}
        set;
    }

    // the current field to sort by. defaults to last name
    public String sortField {
        // To set a Field for sorting.
		get{ 
			if (sortField == null){
				sortField = 'Name'; 
			} 
		return sortField;  
		}
        set;
    } 
}