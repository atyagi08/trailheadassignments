global class EmailRecordInsertion implements Messaging.InboundEmailHandler{
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope){
        sObject obj;                                                                //variable to store single record detail
        String objectDetail                 = '';                                   //single object detail
        String objectName                   = '';                                   //single object name
        String fieldName                    = '';                                   //variable for fieldname for each record
        String fieldValue                   = '';                                   //variable for fieldvalue for each fielname
        String errorDetails                 = '';                                   //String of all the errors during the insertion
        List<sObject> objectList		    = new List<sObject>();                  //List which will be inserted
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();   //Result of the E-mail Service
        Map<string, SObjectField> fieldsMap = new Map<String, SObjectField>();      //fieldmap for each object
        List<String> objectDetailList 	    = email.plainTextBody.split('##');      //List of all object details which will be inserted

        for(Integer i=0; i<objectDetailList.size(); i++){                           //Creating objectList which will be inserted
            objectDetail = objectDetailList[i];
            objectName   = objectDetail.substringBefore('#').trim();
            try{
                obj = Schema.getGlobalDescribe().get(objectName).newSObject();      //Creating an object of type objectName
            }
            catch (Exception e){
                errorDetails += (e.getTypeName() + '. ' + e.getMessage() + '.\n\t' + objectName + ' not found. Check the spelling of object API Name\n\n');
                continue;
            }
            fieldsMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();   //Getting all the fields for the object
            objectDetail = objectDetail.substringAfter('#');
            while(objectDetail.length() > 1){                                                       //inserting value of fields into the specified fields
                fieldName  = objectDetail.substringBefore('#').trim();
                fieldValue = objectDetail.substringAfter('#').substringBefore('#').trim();
                Schema.DisplayType fieldType;                                                       //Getting fieldtype for fieldname
                try{
                    fieldType = fieldsMap.get(fieldName).getDescribe().getType();
                }
                catch(Exception e){
                    errorDetails += (e.getMessage() + '.\n\t' + 'Check API Name of field \"' + fieldName + '\" for record number ' + (i+1) + '\n');
                    objectDetail = objectDetail.substringAfter('#').substringAfter('#');
                    continue;
                }

                if( fieldType == Schema.DisplayType.DATE ){                                         //Setting value for Date Type fields
                    try{
                        obj.put(fieldName, Date.valueOf(fieldValue));
                    }
                    catch(Exception e){
                        errorDetails += (e.getMessage() + '.\n\t' + 'Check value of field ' + fieldName + ' for record number ' + (i+1) + '\n');
                    }
                }
                else if( fieldType == Schema.DisplayType.DATETIME ){                                //Setting value for DateTime Type fields
                    try{
                        obj.put(fieldName, Datetime.valueOf(fieldValue));
                    }
                    catch(Exception e){
                        errorDetails += (e.getMessage() + '.\n\t' + 'Check value of field ' + fieldName + ' for record number ' + (i+1) + '\n');
                    }
                }
                else if( fieldType == Schema.DisplayType.BOOLEAN ){                                 //Setting value for Boolean Type fields
                    if(fieldValue.equalsIgnoreCase('true') || fieldValue.equalsIgnoreCase('false') || fieldValue == ''){
                        obj.put(fieldName, Boolean.valueOf(fieldValue));
                    }
                    else{
                        errorDetails += ('Acceptable value for Boolean field is \"TRUE/FALSE\".\n\t Check value of field ' + fieldName + ' for record number ' + (i+1) + '\n');
                    }
                }
                else{                                                                               //Setting value of all other type of fields
                    obj.put(fieldName, fieldValue);
                }
                objectDetail = objectDetail.substringAfter('#').substringAfter('#');
            }
            objectList.add(obj);
            errorDetails += errorDetails.length() > 1 ? '\n' : '';
        }

        Integer totalRecords = 0, failedRecords = 0;
        Database.SaveResult[] svResultList = Database.insert(objectList, false);                    //Acheiving partial insertion
        for(Database.SaveResult svResult : svResultList){
            ++totalRecords;
            if(!svResult.isSuccess()){
                ++failedRecords;
                for(Database.Error err : svResult.getErrors()) {
                    errorDetails += (err.getStatusCode() + ': ' + err.getMessage() + ' at record number: ' + totalRecords + '\n');
                }
            }
        }
        errorDetails += ('\nTotal Records: ' + objectDetailList.size() + '\nInserted Records: ' + (totalRecords - failedRecords));

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();                 //Sending status email to the sender
        mail.setToAddresses(new List<String>{email.fromAddress});
        mail.setSenderDisplayName('Salesforce Support');
        mail.setSubject('Status: Records Insertion');
        if(!errorDetails.substringBetween('\n', ' ').equals('Total')){
            mail.setPlainTextBody(errorDetails);
        }
        else {
            mail.setPlainTextBody('Records inserted successfully.\nTotal Records: ' + objectDetailList.size() + '\nInserted Records: ' + (totalRecords - failedRecords));
        }
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        result.success = true;
        return result;
    }
}