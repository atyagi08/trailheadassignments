/**
**@Description: Apex Wrapper Class for integration with Dropbox
**
**@Author: Akhil Tyagi
**@Date: 16 January 2020    
**
**@ChangeLog:
**20 January 2020: Upadated WrapperWhole class.
***/

public class commDropboxWrapper {

    //Wrapper Class for Access Token
    public class WrapperToken{
        public String  access_token{ get; set; }
    }

    //Wrapper Class for file details
    public class WrapperFileList{
        public String  id{ get; set; }
        public String  tag{ get; set; }
        public String  name{ get; set; }
        public String  path_lower{ get; set; }
        public Boolean is_downloadable{ get; set; }
        
        public WrapperFileList(){ }            
    }

    //Wrapper Calss for file/folder and webContentLink details
    public class WrapperWhole{
        public String                link{ get; set; }
        public Integer               status{ get; set; }
        public WrapperFileList       metadata{ get; set; }
        public List<WrapperFileList> entries{ get; set; }
    }
}