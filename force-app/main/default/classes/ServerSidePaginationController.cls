/**
**@Description: Apex Controller Class for ServerSidePagination Component
**
**@Author: Akhil Tyagi
**@Date: 13 December 2019    
**
**@ChangeLog:
**18 December 2019: Changed the class for pagination using offset
**19 December 2019: Changed the class for unlimited pagination using Id field of the Account Object
**26 December 2019: Changed the class for unlimited pagination using Id field of the Account Object
***/

public class ServerSidePaginationController {

    /***************************************************************************************************************
    **@Description: Create a list of accounts which are to be sent to the lightning component.                    **
    **                                                                                                            **
    **@Parameter paramString: JSON String of required parameters.                                                 **
    **                                                                                                            **
    **@Return JSON String with list of account, count of total accounts and count of filter accounts              **
    ***************************************************************************************************************/

    @AuraEnabled    
    public static String getAccounts(String paramString){
        Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(paramString);
        String order                 = String.valueOf(paramMap.get('order'));
        String recordId              = String.valueOf(paramMap.get('recordId'));
        String fieldName             = String.valueOf(paramMap.get('fieldName'));
        String fieldValue            = String.valueOf(paramMap.get('fieldValue'));
        String objectName            = String.valueOf(paramMap.get('objectName'));
        String buttonLabel           = String.valueOf(paramMap.get('buttonLabel'));
        Integer pageSize             = Integer.valueOf(paramMap.get('pageSize'));
        // System.debug('@@@getAccounts Method:\n@@@Page Size === ' + pageSize + '\n@@@Field Name === ' + fieldName);
        // System.debug('\n@@@Record Id === ' + recordId + '\n@@@Field Value === ' + fieldValue);
        // System.debug('\n@@@Sort Order === ' + order + '\n@@@Button Label === ' + buttonLabel);
        Integer totalAccount  = Database.countQuery('SELECT COUNT() FROM ' + objectName);
        List<Account> accList = RecordsSorting.SortingMethod(pageSize, fieldName, recordId, fieldValue, objectName, order, buttonLabel);
        return JSON.serialize(new bhotHard(accList, totalAccount));
    }

    /***************************************************************************************************************
    **@Description: Delete single or multiple records                                                             **
    **                                                                                                            **
    **@Parameter idDelete: List of id of records which needs to be deleted.                                       **
    **                                                                                                            **
    **@Return String                                                                                              **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String deleteAccount(List<String> idDelete){
        List<Account> acc = [SELECT Id, Name FROM Account WHERE Id IN :idDelete];
        delete acc;
        return null;
    }

    /***************************************************************************************************************
    **@Description: Wrapper class for creating JSON of records                                                    **
    **                                                                                                            **
    **@Author: Akhil Tyagi                                                                                        **
    **@Date: 13 December 2019                                                                                     **
    **                                                                                                            **
    **@Changelog:                                                                                                 **
    ***************************************************************************************************************/

    public class bhotHard{
        List<Account> accList;                                          //List of required accounts
        Integer totalAccount;                                           //Count of total account records
        public bhotHard(List<Account> accList, Integer totalAccount){
            this.accList       = accList;
            this.totalAccount  = totalAccount;
        }
    }
}