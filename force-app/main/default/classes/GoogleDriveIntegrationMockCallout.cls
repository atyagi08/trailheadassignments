@isTest
global class GoogleDriveIntegrationMockCallout implements HttpCalloutMock{
    
    global HTTPResponse respond(HTTPRequest request) {
        // System.debug('@@@@@@@@@@@@@@@@@@@MockCalloutClass'); 
        if(request.getMethod().equals('GET')){                                          //Response for GET Methods
            String body = '';
            body = '{\n'+
            '\"webContentLink\": \"web_Content_Link\",\n'+
            '\"files\": [\n'+
                '{\n\"name\": \"File_1\",\n\"mimeType\": \"text/csv\",\n\"id\": \"id_1\"\n},\n'+
                '{\n\"name\": \"File_2\",\n\"mimeType\": \"text/csv\",\n\"id\": \"id_2\"\n},\n'+
                '{\n\"name\": \"Folder_1\",\n\"mimeType\": \"application/vnd.google-apps.folder\",\n\"id\": \"id_3\"\n}\n'+
            ']\n}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(body);
            response.setStatus('OK');
            response.setStatusCode(200);
            // System.debug('@@@MockCalloutClassGET === \n'+response.getBody());
            return response;
        }
        else if(request.getMethod().equals('DELETE')){                                  //Response for DELETE Methods
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('');
            response.setStatus('OK');
            response.setStatusCode(204);
            // System.debug('@@@MockCalloutClassDELETE === \n'+response.getBody());
            return response;
        }                                                                               //Response for POST(Not Access Token) Methods
        else if(request.getMethod().equals('POST') && !request.getHeader('Content-type').equals('application/x-www-form-urlencoded')){
            String body = '';
            body = '{\n'+
                '\"name\": \"File/Folder_1\",\n\"mimeType\": \"file/folder\",\n\"id\": \"id_4\"'+
            '\n}';
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(body);
            response.setStatus('OK');
            response.setStatusCode(200);
            // System.debug('@@@MockCalloutClassPOST === \n'+response.getBody());
            return response;
        }
        String body = '';                                                               //Response for POST(Access Token) Methods
        body = '{\n'+
            '\"access_token\": \"acc_token\",\n\"refresh_token\": \"ref_token\",\n\"expires_in\": \"3600\"'+
        '\n}';
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(body);
        response.setStatus('OK');
        response.setStatusCode(200);
        // System.debug('@@@MockCalloutClassPOST === \n'+response.getBody());
        return response;
    }
}