public class ClientSidePagination {

    public class ResponseWrapper {
        @AuraEnabled
        public List<SelectOpts> objectList;
        @AuraEnabled
        public List<SelectOpts> pageSizeOptions;  
    }
    
    public class SelectOpts {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;

        public SelectOpts( String value, String label ) {
            this.label = label;
            this.value = value;
        }
    }

    @AuraEnabled
    public static ResponseWrapper getObjectList() {
        ResponseWrapper response = new ResponseWrapper();
        response.objectList      = new List<SelectOpts>();
        response.pageSizeOptions = new List<SelectOpts>();

        Map<string, SObjectType> objectNamesMap = Schema.getGlobalDescribe();
        for( String key : objectNamesMap.keySet() ) {
            response.objectList.add( new SelectOpts( objectNamesMap.get(key).getDescribe().getName(), objectNamesMap.get(key).getDescribe().getLabel() ) );
        }
        System.debug( '@@@Object List ==>> ' + response.objectList.size() + ' ==> \n' + response.objectList );
        response.pageSizeOptions.add( new SelectOpts( '5', '5' ) );
        response.pageSizeOptions.add( new SelectOpts( '10', '10' ) );
        response.pageSizeOptions.add( new SelectOpts( '15', '15' ) );
        response.pageSizeOptions.add( new SelectOpts( '20', '20' ) );
        response.pageSizeOptions.add( new SelectOpts( '50', '50' ) );
        response.pageSizeOptions.add( new SelectOpts( '100', '100' ) );
        return response;
    }

    @AuraEnabled
    public static List<SelectOpts> getFieldsList( String objectName ) {
        List<SelectOpts> fieldsList = new List<SelectOpts>();
        Map<string, SObjectField> fieldNamesMap = Schema.getGlobalDescribe().get( objectName ).getDescribe().fields.getMap();
        for(String key : fieldNamesMap.keySet()){
            if( !( fieldNamesMap.get(key).getDescribe().getType() == Schema.DisplayType.ADDRESS ) ){
                fieldsList.add( new SelectOpts( fieldNamesMap.get(key).getDescribe().getName(), fieldNamesMap.get(key).getDescribe().getLabel() ) );
            }
        }
        return fieldsList;
    }

    @AuraEnabled
    public static List<sObject> recordsList( String objectName, String fields ) {
        
        // String query = 'SELECT ' + fields + 'FROM ' + objectName + ' WHERE ID > :recordId ORDER BY ID ';
        String query = 'SELECT ' + fields + ' FROM ' + objectName + ' ORDER BY ID';
        System.debug( '@@@@ Query ==>> ' + query );
        List<sObject> objList = Database.query( query );
        System.debug( '@@@@ Records ==> ' + objList );
        return objList;
    }

    @AuraEnabled
    public static String DeleteRecord( String recordId, String objectName ){
        String query = 'SELECT Id FROM ' + objectName + ' WHERE ID = :recordId';
        List<sObject> rcrdList = Database.query( query );
        delete rcrdList;
        return 'Success';
    }
}