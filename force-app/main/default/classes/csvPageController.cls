public class csvPageController {
    
    //Private Variables----------------------------------------------------------
    private String str;    
    private List<String> column;
    private List<sObject> tempList;
    
    //Variables Required on CSV Page---------------------------------------------
    public List<sObject> retrieveRecordsCSV{ get; set; }
    public List<String> retrieveColumns{ get; set; }
    public String query{ get; set; }
    
    //Constructor----------------------------------------------------------------
    public csvPageController(){
        query = ApexPages.currentPage().getParameters().get('records');        
    }
    
    //Method to set values of required variables---------------------------------
    public pageReference fetch(){ 
        column = new List<String>();
        tempList = new List<sObject>();
        Set<Id> idSet = new Set<Id>();
		
        //Retrieve Records---------------------------------
        String allId = query.substringBetween('*', '**');
        String objectName = query.substringBetween('**', '***');
        String allFields = query.substringBetween('***', '****');
        while( allId.length() > 0 ){
            String requiredId = allId.substringBefore(',');
            idSet.add(requiredId);
            allId = allId.remove(requiredId + ',');
        }
        System.debug('SELECT '+ allFields +' FROM '+ objectName +' WHERE Id in :idSet');
        for(sObject obj : Database.query('SELECT '+ allFields +' FROM '+ objectName +' WHERE Id in :idSet')){
            tempList.add(obj);
        }
        retrieveRecordsCSV = tempList;
        
        //Retrieve Columns---------------------------------
        str = query.substringBetween('***', '****');
        str += ', ';
        while(str.length() > 0){
            column.add(str.substringBefore(','));
            str = str.remove(str.substringBefore(',') + ', ');
        }
        retrieveColumns = column;
        //PageReference page = new PageReference('/apex/DescribeCallAssignment');
        //return page;
        return null;
    }    
}