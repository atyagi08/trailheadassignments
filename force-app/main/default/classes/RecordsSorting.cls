/**
**@Description: Class for Sorting on server side
**
**@Author: Akhil Tyagi
**@Date: 24 December 2019    
**
**@ChangeLog:
**26 December 2019: Updated class for sorting field of datatype Integer and Boolean
***/

public class RecordsSorting {

    /***************************************************************************************************************
    **@Description: Create a list of sorted accounts for first, previous, next and last page                      **
    **                                                                                                            **
    **@Parameter pageSize: Current Page Size                                                                      **
    **@Parameter fieldName: Name of field to be sorted                                                            **
    **@Parameter recordId: Id of record which will be used to fetch the previous/next page records.               **
    **@Parameter fieldValue: Value of record which will be used to fetch the previous/next page records           **
    **@Parameter order: Current sort order                                                                        **
    **@Parameter buttonLabel: Which button is clicked                                                             **
    **                                                                                                            **
    **@Return List<Account>                                                                                       **
    ***************************************************************************************************************/

    public static List<Account> SortingMethod(Integer pageSize, String fieldName, String recordId, String fieldValue, String objectName, String order, String buttonLabel) {

        String qry                 = 'SELECT Id, Name, AccountNumber, NumberOfEmployees, Phone FROM ' + objectName + ' WHERE ';
        String query               = '';
        String checkId             = recordId;
        Integer count              = 0;
        List<Account> accList1     = new List<Account>();
        List<Account> accList2     = new List<Account>();
        List<Account> accList3     = new List<Account>();
        List<Account> accListFinal = new List<Account>();
        Schema.DisplayType fldType = fieldDataType('Account', fieldName);

        if (order.equals('ASC')) {                                                              //Ascending Sort
            if (buttonLabel.equals('First')) {                                                  //First Page
                // Query for Null records
                query    = qry + fieldName + ' = null ORDER BY ' + fieldName + ' ASC, Id ASC LIMIT ' + pageSize;
                accList1 = Database.query(query);
                count    = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
                if (count != 0) {
                    // Query for Not Null records
                    query    = qry + fieldName + ' != null ORDER BY ' + fieldName + ' ASC, Id ASC LIMIT ' + count;
                    accList2 = Database.query(query);
                }
                for (Account a : accList1) {
                    accListFinal.add(a);
                }
                for (Account a : accList2) {
                    accListFinal.add(a);
                }
            } else if (buttonLabel.equals('Previous')) {                                        //Previous Page
                if (fieldValue != null) {                                                       //When first value of current field is not null
                    if (Schema.DisplayType.INTEGER == fldType) {                                //Integer Type Field
                        Integer fldVal = Integer.valueOf(fieldValue);
                        // Query for Not Null and Equal Field Value records
                        query          = qry + fieldName + ' = :fldVal AND Id < :checkId ORDER BY Id ASC LIMIT ' + pageSize;
                        accList1       = Database.query(query);
                        count          = pageSize - accList1.size();
                        // Query for Not Null records
                        query          = qry + fieldName + ' < :fldVal ORDER BY ' + fieldName + ' DESC NULLS LAST, Id DESC LIMIT ' + count;
                        accList2       = Database.query(query);
                        count          = count - accList2.size();
                    } else if (Schema.DisplayType.BOOLEAN == fldType) {                         //Boolean Type Field
                        Boolean fldVal = Boolean.valueOf(fieldValue);
                        // Query for Not Null and Equal Field Value records
                        query          = qry + fieldName + ' = :fldVal AND Id < :checkId ORDER BY Id ASC LIMIT ' + pageSize;
                        accList1       = Database.query(query);
                        count          = pageSize - accList1.size();
                        if (count != 0 && fldVal == true) {
                            // Query for False records
                            query    = qry + fieldName + ' = false ORDER BY Id DESC LIMIT ' + count;
                            accList2 = Database.query(query);
                            count    = count - accList2.size();
                        }
                    } else {                                                                    //String compatible Type Fields
                        // Query for Not Null and Equal Field Value records
                        query    = qry + fieldName + ' = \'' + fieldValue + '\' AND Id < :checkId ORDER BY Id ASC LIMIT ' + pageSize;
                        accList1 = Database.query(query);
                        count    = pageSize - accList1.size();
                        // Query for Not Null records
                        query    = qry + fieldName + ' < \'' + fieldValue + '\' ORDER BY ' + fieldName + ' DESC NULLS LAST, Id DESC LIMIT ' + count;
                        accList2 = Database.query(query);
                        count    = count - accList2.size();
                    }
                    checkId = '';
                } else {
                    count    = pageSize;
                }

                if (Schema.DisplayType.INTEGER == fldType && fieldValue != null) {              //Integer type field with current value as null
                    query    = qry + fieldName + ' = null ORDER BY Id DESC LIMIT ' + count;
                    System.debug('@@@Query for Null records In Integer Type Case ==>> \n' + query);
                    accList3 = Database.query(query);
                } else {
                    query    = qry + fieldName + ' = null AND Id < :checkId ORDER BY Id DESC LIMIT ' + count;
                    System.debug('@@@Query for Null records In Other Type Case==>> \n' + query);
                    accList3 = Database.query(query);
                }

                for (Integer i=accList3.size()-1; i>=0; i--) {
                    accListFinal.add(accList3[i]);
                }
                for (Integer i=accList2.size()-1; i>=0; i--) {
                    accListFinal.add(accList2[i]);
                }
                for (Account a : accList1) {
                    accListFinal.add(a);
                }
            } else if (buttonLabel.equals('Next')) {                                            //Next Page
                if (fieldValue == null) {                                                       //When current field value is null
                    // Query for Null records
                    query    = qry + fieldName + ' = null AND Id > :checkId ORDER BY ' + fieldName + ' ASC, Id ASC LIMIT ' + pageSize;
                    accList1 = Database.query(query);
                    count    = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
                    if (count != 0) {
                        // Query for Not Null records
                        query    = qry + fieldName + ' != null ORDER BY ' + fieldName + ' ASC, Id ASC LIMIT ' + count;
                        accList2 = Database.query(query);
                    }
                } else {
                    if (Schema.DisplayType.INTEGER == fldType) {                                //Integer Type Fields
                        Integer fldVal = Integer.valueOf(fieldValue);
                        // Query for Not Null and equal Field Value records
                        query    = qry + fieldName + ' = :fldVal AND Id > :checkId ORDER BY Id ASC LIMIT ' + pageSize;
                        accList1 = Database.query(query);
                        count    = pageSize - accList1.size();
                        // Query for Not Null and Not Equal Field Value records
                        query    = qry + fieldName + ' > :fldVal ORDER BY ' + fieldName + ' ASC, Id ASC LIMIT ' + count;
                        accList2 = Database.query(query);
                    } else if (Schema.DisplayType.BOOLEAN == fldType) {                         //Boolean Type Fields
                        Boolean fldVal = Boolean.valueOf(fieldValue);
                        // Query for Not Null and equal Field Value records
                        query    = qry + fieldName + ' = :fldVal AND Id > :checkId ORDER BY Id ASC LIMIT ' + pageSize;
                        accList1 = Database.query(query);
                        count    = pageSize - accList1.size();
                        if (count !=0 && fldVal == false) {
                            // Query for True records
                            query    = qry + fieldName + ' = true ORDER BY Id ASC LIMIT ' + count;
                            accList2 = Database.query(query);
                        }
                    } else {                                                                    //String compatible Type Fields
                        // Query for Not Null and equal Field Value records
                        query    = qry + fieldName + ' = \'' + fieldValue +'\' AND Id > :checkId ORDER BY Id ASC LIMIT ' + pageSize;
                        accList1 = Database.query(query);
                        count    = pageSize - accList1.size();
                        if (count != 0) {
                            // Query for Not Null and Not Equal Field Value records
                            query    = qry + fieldName + ' > \'' + fieldValue + '\' ORDER BY ' + fieldName + ' ASC, Id ASC LIMIT ' + count;
                            accList2 = Database.query(query);
                        }
                    }
                }
                for(Account a : accList1){
                    accListFinal.add(a);
                }
                for(Account a : accList2){
                    accListFinal.add(a);
                }
            } else if (buttonLabel.equals('Last')) {                                            //Last Page
                Integer totalAccount = Database.countQuery('SELECT COUNT() FROM Account');
                pageSize = ( Math.mod(totalAccount, pageSize) == 0 ) ? pageSize : Math.mod(totalAccount, pageSize);
                // Query for Last Page records
                query    = 'SELECT Id, Name, AccountNumber, NumberOfEmployees, Phone FROM Account ORDER BY ' + fieldName + ' DESC NULLS LAST, Id DESC LIMIT ' + pageSize + ' OFFSET 0';
                accList1 = Database.query(query);
                for (Integer i=accList1.size()-1; i>=0; i--) {
                    accListFinal.add(accList1[i]);
                }
            }
        } else if (order.equals('DESC')) {                                                      //Descending Sort
            if (buttonLabel.equals('First')) {                                                  //First Page
                // Query for Not Null records
                query    = qry + fieldName + ' != null ORDER BY ' + fieldName + ' DESC, Id DESC LIMIT ' + pageSize;    
                accList1 = Database.query(query);
                count    = pageSize - accList1.size();
                if (count != 0) {
                    // Query for Null records
                    query    = qry + fieldName + ' = null ORDER BY Id DESC LIMIT ' + count;
                    accList2 = Database.query(query);
                }
                for (Account a : accList1) {
                    accListFinal.add(a);
                }
                for (Account a : accList2) {
                    accListFinal.add(a);
                }
            } else if (buttonLabel.equals('Previous')) {                                        //Previous Page
                if (fieldValue == null) {                                                       //Current field value is null
                    // Query for Null records
                    query    = qry + fieldName + ' = null AND Id > :checkId ORDER BY Id ASC LIMIT ' + pageSize;
                    accList1 = Database.query(query);
                    count    = pageSize - accList1.size();
                    // Query for Not Null records
                    query    = qry + fieldName + ' != null ORDER BY ' + fieldName + ' ASC, Id ASC LIMIT ' + count;
                    accList2 = Database.query(query);
                } else {
                    if (Schema.DisplayType.INTEGER == fldType) {                                //Integer Type Fields
                        Integer fldVal = Integer.valueOf(fieldValue);
                        // Query for Not Null and Field Value Equal records
                        query    = qry + fieldName + ' = :fldVal AND Id > :checkId ORDER BY Id ASC LIMIT ' + pageSize;
                        accList1 = Database.query(query);
                        count = pageSize - accList1.size();
                        // Query for Not Null and Field Value Not Equal records
                        query    = qry + fieldName + ' > :fldVal ORDER BY ' + fieldName +' ASC, Id ASC LIMIT ' + count;
                        accList2 = Database.query(query);
                    } else if (Schema.DisplayType.BOOLEAN == fldType) {                         //Boolean Type Fields
                        Boolean fldVal = Boolean.valueOf(fieldValue);
                        // Query for Not Null and Field Value Equal records
                        query    = qry + fieldName + ' = :fldVal AND Id > :checkId ORDER BY Id ASC LIMIT ' + pageSize;
                        accList1 = Database.query(query);
                        count = pageSize - accList1.size();
                        if (count != 0 && fldVal == false) {
                            // Query for True records
                            query    = qry + fieldName + ' = true ORDER BY Id ASC LIMIT ' + count;
                            accList2 = Database.query(query);
                        }
                    } else {                                                                    //String compatible Type Fields
                        // Query for Not Null and Field Value Equal records
                        query    = qry + fieldName + ' = \'' + fieldValue + '\' AND Id > :checkId ORDER BY Id ASC LIMIT ' + pageSize;
                        accList1 = Database.query(query);
                        count = pageSize - accList1.size();
                        // Query for Not Null and Field Value Not Equal records
                        query    = qry + fieldName + ' > \'' + fieldValue + '\' ORDER BY ' + fieldName +' ASC, Id ASC LIMIT ' + count;
                        accList2 = Database.query(query);
                    }
                }
                for (Integer i=accList2.size()-1; i>=0; i--) {
                    accListFinal.add(accList2[i]);
                }
                for (Integer i=accList1.size()-1; i>=0; i--) {
                    accListFinal.add(accList1[i]);
                }
            } else if (buttonLabel.equals('Next')) {                                            //Next Page
                if (fieldValue != null) {                                                       //Current field value is not null
                    if (Schema.DisplayType.INTEGER == fldType) {                                //Integer Type Fields
                        Integer fldVal = Integer.valueOf(fieldValue);
                        // Query for Not Null and Field Value Equal records
                        query    = qry + fieldName + ' = :fldVal AND Id < :checkId ORDER BY Id DESC LIMIT ' + pageSize;
                        accList1 = Database.query(query);
                        count    = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
                        if (count != 0) {
                            // Query for Not Null and Field Value Not Equal records
                            query    = qry + fieldName + ' < :fldVal AND ' + fieldName + ' != null ORDER BY ' + fieldName + ' DESC, Id DESC LIMIT ' + count;
                            accList2 = Database.query(query);
                        }
                        count = pageSize - accList1.size() - accList2.size();
                    } else if (Schema.DisplayType.BOOLEAN == fldType) {                         //Boolean Type Fields
                        Boolean fldVal = Boolean.valueOf(fieldValue);
                        // Query for Not Null and Field Value Equal records
                        query    = qry + fieldName + ' = :fldVal AND Id < :checkId ORDER BY Id DESC LIMIT ' + pageSize;
                        accList1 = Database.query(query);
                        count    = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
                        if (count != 0 && fldVal == true) {
                            // Query for False records
                            query    = qry + fieldName + ' = false ORDER BY ' + fieldName + ' DESC, Id DESC LIMIT ' + count;
                            accList2 = Database.query(query);
                        }
                        count = pageSize - accList1.size() - accList2.size();
                    } else {                                                                    //String compatible Type Fields
                        // Query for Not Null and Field Value Equal records
                        query    = qry + fieldName + ' = \'' + fieldValue + '\' AND Id < :checkId ORDER BY Id DESC LIMIT ' + pageSize;
                        accList1 = Database.query(query);
                        count    = (accList1.size() == pageSize) ? 0 : pageSize - accList1.size();
                        if (count != 0) {
                            // Query for Not Null and Field Value Not Equal records
                            query    = qry + fieldName + ' < \'' + fieldValue + '\' AND ' + fieldName + ' != null ORDER BY ' + fieldName + ' DESC, Id DESC LIMIT ' + count;
                            accList2 = Database.query(query);
                        }
                        count = pageSize - accList1.size() - accList2.size();
                    }
                     
                    if (count != 0) {
                        // Query for Null records
                        query    = qry + fieldName + ' = null ORDER BY Id DESC LIMIT ' + count;
                        accList3 = Database.query(query);
                    }
                    for (Account a : accList1) {
                        accListFinal.add(a);
                    }
                    for (Account a : accList2) {
                        accListFinal.add(a);
                    }
                    for (Account a : accList3) {
                        accListFinal.add(a);
                    }
                } else {
                    // Query for Null records
                    query    = qry + fieldName +' = null AND Id < :checkId ORDER BY Id DESC LIMIT ' + pageSize;
                    accList1 = Database.query(query);
                    for(Account a : accList1){ accListFinal.add(a); }
                }
            } else if (buttonLabel.equals('Last')) {                                            //Last Page
                Integer totalAccount = Database.countQuery('SELECT COUNT() FROM Account');
                pageSize = ( Math.mod(totalAccount, pageSize) == 0 ) ? pageSize : Math.mod(totalAccount, pageSize);
                // Query for Null Records
                query    = qry + fieldName + ' = null ORDER BY Id ASC LIMIT ' + pageSize + ' OFFSET 0';
                accList1 = Database.query(query);
                count    = pageSize - accList1.size();
                // Query for Not Null Records
                query    = qry + fieldName + ' != null ORDER BY ' + fieldName + ' ASC, Id ASC LIMIT ' + count;
                accList2 = Database.query(query);
                for (Integer i=accList2.size()-1; i>=0; i--) {
                    accListFinal.add(accList2[i]);
                }
                for (Integer i=accList1.size()-1; i>=0; i--) {
                    accListFinal.add(accList1[i]);
                }
            }
        }
        return accListFinal;
    }

    /***************************************************************************************************************
    **@Description: Return data type of field                                                                     **
    **                                                                                                            **
    **@Parameter objectName: Name of the object.                                                                  **
    **@Parameter fieldName: Name of the field.                                                                    **
    **                                                                                                            **
    **@Return Schema.DisplayType                                                                                              **
    ***************************************************************************************************************/

    public static Schema.DisplayType fieldDataType(String objectName, String fieldName) {
        Map<String, Schema.SObjectType> schemaMap       = Schema.getGlobalDescribe();
        Schema.SObjectType objectSchemaMap              = schemaMap.get(objectName);
        Map<String, Schema.SObjectField> objectFieldMap = objectSchemaMap.getDescribe().fields.getMap();
        Schema.DisplayType fldDataType                  = objectFieldMap.get(fieldName).getDescribe().getType();
        return fldDataType;
    }
}