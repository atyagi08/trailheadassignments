@isTest
public with sharing class GoogleDriveIntegrationTestClass {

    //-------------------------------------DriveAuth and GoogleDriveAuthUri Cases------------------------------------
    //Case-1 Method for authorization
    @isTest
    static void DriveAuth01(){
        Integration_Key__mdt keys = [Select Client_Id__c, Redirect_URl__c FROM Integration_Key__mdt WHERE MasterLabel = 'GDrive Keys' ];
        System.debug(keys);
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.DriveAuth();
        Test.stopTest();
    }

    //-------------------------------------PageLoad Method Cases------------------------------------
    //Case-1 No code
    @isTest
    static void PageLoad01(){
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.PageLoad();
        Test.stopTest();
    }

    //Case-1 With code
    @isTest
    static void PageLoad02(){
        Test.startTest();
            ApexPages.currentPage().getParameters().put('code', 'code_value');
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.PageLoad();
        Test.stopTest();
    }

    //-------------------------------------AccessToken Method Cases------------------------------------
    //Case-1 No code, throws exception
    @isTest
    static void AccessToken01(){
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.AccessToken();
        Test.stopTest();
    }

    //Case-2 Getting access token
    @isTest
    static void AccessToken02(){
        Datetime dt = System.now().addSeconds(3600);
        Integration_Tokens__c itc = new Integration_Tokens__c(Access_Token__c='access_token', Refresh_Token__c='refresh_token', Expire_Time__c=dt, Name__c='GDrive');
        insert itc;
        Test.startTest();
            ApexPages.currentPage().getParameters().put('code', 'code_value');
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.AccessToken();
        Test.stopTest();
    }

    //-------------------------------------RecordsList Method Cases------------------------------------
    //Case-1 No code available
    @isTest
    static void RecordList01(){
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.PageLoad();
            gdic.RecordsList();
            System.assertEquals(0, gdic.wrapperList.size());
            System.assertEquals(0, gdic.directoryList.size());
        Test.stopTest();
    }

    //Case-2 With code but throwing exception
    @isTest
    static void RecordList02(){
        Test.startTest();
            ApexPages.currentPage().getParameters().put('code', 'code_value');
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.PageLoad();
            gdic.RecordsList();
            System.assertEquals(0, gdic.wrapperList.size());
            System.assertEquals(0, gdic.directoryList.size());
        Test.stopTest();
    }

    //Case-3 Getting file and folder list
    @isTest
    static void RecordList03(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveIntegrationMockCallout());
        Datetime dt = System.now().addSeconds(3600);
        Integration_Tokens__c itc = new Integration_Tokens__c(Access_Token__c='access_token', Refresh_Token__c='refresh_token', Expire_Time__c=dt, Name__c='GDrive');
        insert itc;
        ApexPages.currentPage().getParameters().put('code', 'code_value');
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.PageLoad();
            gdic.RecordsList();
            System.assertEquals(3, gdic.wrapperList.size());
            System.assertEquals('File_1', gdic.wrapperList[0].name);    System.assertEquals('id_1', gdic.wrapperList[0].id);    System.assertEquals('text/csv', gdic.wrapperList[0].mimeType);
            System.assertEquals('File_2', gdic.wrapperList[1].name);    System.assertEquals('id_2', gdic.wrapperList[1].id);    System.assertEquals('text/csv', gdic.wrapperList[1].mimeType);
            System.assertEquals('Folder_1', gdic.wrapperList[2].name);  System.assertEquals('id_3', gdic.wrapperList[2].id);    System.assertEquals('application/vnd.google-apps.folder', gdic.wrapperList[2].mimeType);
            System.assertEquals(1, gdic.directoryList.size());
            System.assertEquals('Home', gdic.directoryList[0].name);    System.assertEquals('root', gdic.directoryList[0].selfId);
        Test.stopTest();
    }

    //-------------------------------------ClickOnFileFolder Method Cases------------------------------------
    //Case-1 Getting exeption when opening folder
    static void Folder01(){
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.fileMimeTypeParam = 'application/vnd.google-apps.folder';
            gdic.ClickOnFileFolder();
            System.assertEquals(0, gdic.wrapperList.size());
            System.assertEquals(0, gdic.directoryList.size());
        Test.stopTest();
    }
    
    //Case-2 Opening foler successfully
    @isTest
    static void Folder02(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveIntegrationMockCallout());
        Datetime dt = System.now().addSeconds(3600);
        Integration_Tokens__c itc = new Integration_Tokens__c(Access_Token__c='access_token', Refresh_Token__c='refresh_token', Expire_Time__c=dt, Name__c='GDrive');
        insert itc;
        ApexPages.currentPage().getParameters().put('code', 'code_value');
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.fileMimeTypeParam = 'application/vnd.google-apps.folder';
            gdic.fileIdParam = 'fileId';
            gdic.fileNameParam = 'Folder';
            gdic.PageLoad();
            gdic.ClickOnFileFolder();
            System.assertEquals(3, gdic.wrapperList.size());
            System.assertEquals('File_1', gdic.wrapperList[0].name);    System.assertEquals('id_1', gdic.wrapperList[0].id);    System.assertEquals('text/csv', gdic.wrapperList[0].mimeType);
            System.assertEquals('File_2', gdic.wrapperList[1].name);    System.assertEquals('id_2', gdic.wrapperList[1].id);    System.assertEquals('text/csv', gdic.wrapperList[1].mimeType);
            System.assertEquals('Folder_1', gdic.wrapperList[2].name);  System.assertEquals('id_3', gdic.wrapperList[2].id);    System.assertEquals('application/vnd.google-apps.folder', gdic.wrapperList[2].mimeType);
            System.assertEquals(1, gdic.directoryList.size());
            System.assertEquals('Folder', gdic.directoryList[0].name);    System.assertEquals('fileId', gdic.directoryList[0].selfId);
        Test.stopTest();
    }

    //Case-3 Getting exception when downloading file
    @isTest
    static void File01(){
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.fileMimeTypeParam = 'Folder';
            gdic.ClickOnFileFolder();
            System.assertEquals(0, gdic.wrapperList.size());
            System.assertEquals(0, gdic.directoryList.size());
        Test.stopTest();
    }

    //Case-4 Fiel downloading successfully
    @isTest
    static void File02(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveIntegrationMockCallout());
        Datetime dt = System.now().addSeconds(3600);
        Integration_Tokens__c itc = new Integration_Tokens__c(Access_Token__c='access_token', Refresh_Token__c='refresh_token', Expire_Time__c=dt, Name__c='GDrive');
        insert itc;
        ApexPages.currentPage().getParameters().put('code', 'code_value');
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.fileMimeTypeParam = 'Folder';  
            gdic.fileIdParam = 'fileId';
            gdic.PageLoad();
            gdic.ClickOnFileFolder();
            System.assertEquals(0, gdic.wrapperList.size());
            System.assertEquals(0, gdic.directoryList.size());
        Test.stopTest();
    }

    //-------------------------------------DirectoryMove Method Cases------------------------------------
    //Case-1 Getting exception when moving in directory
    @isTest
    static void DirectotyMove01(){
        List<GoogleDriveIntegrationWrapper.WrapperDirectory> dirList = new List<GoogleDriveIntegrationWrapper.WrapperDirectory>();
        GoogleDriveIntegrationWrapper.WrapperDirectory wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Home';
        wD.selfId = 'root';
        dirList.add(wD);
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.directoryList = dirList;
            gdic.directoryIdParam = 'root';
            gdic.DirectoryMove();
            System.assertEquals(1, gdic.directoryList.size());
            System.assertEquals('Home', gdic.directoryList[0].name);        System.assertEquals('root', gdic.directoryList[0].selfId);
            System.assertEquals(0, gdic.wrapperList.size());
        Test.stopTest();
    }

    //Successfully traversing directory
    @isTest
    static void DirectotyMove02(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveIntegrationMockCallout());
        Datetime dt = System.now().addSeconds(3600);
        Integration_Tokens__c itc = new Integration_Tokens__c(Access_Token__c='access_token', Refresh_Token__c='refresh_token', Expire_Time__c=dt, Name__c='GDrive');
        insert itc;
        ApexPages.currentPage().getParameters().put('code', 'code_value');
        List<GoogleDriveIntegrationWrapper.WrapperDirectory> dirList = new List<GoogleDriveIntegrationWrapper.WrapperDirectory>();
        GoogleDriveIntegrationWrapper.WrapperDirectory wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Home';
        wD.selfId = 'root';
        dirList.add(wD);
        wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Folder_1';
        wD.selfId = 'id_1';
        dirList.add(wD);
        wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Folder_2';
        wD.selfId = 'id_2';
        dirList.add(wD);
        wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Folder_3';
        wD.selfId = 'id_3';
        dirList.add(wD);
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.directoryList = dirList;
            gdic.directoryIdParam = 'id_1';
            gdic.PageLoad();
            System.assertEquals(4, gdic.directoryList.size());
            gdic.DirectoryMove();
            System.assertEquals(2, gdic.directoryList.size());
            System.assertEquals('Home', gdic.directoryList[0].name);        System.assertEquals('root', gdic.directoryList[0].selfId);
            System.assertEquals('Folder_1', gdic.directoryList[1].name);    System.assertEquals('id_1', gdic.directoryList[1].selfId);
            System.assertEquals(3, gdic.wrapperList.size());
            System.assertEquals('File_1', gdic.wrapperList[0].name);    System.assertEquals('id_1', gdic.wrapperList[0].id);    System.assertEquals('text/csv', gdic.wrapperList[0].mimeType);
            System.assertEquals('File_2', gdic.wrapperList[1].name);    System.assertEquals('id_2', gdic.wrapperList[1].id);    System.assertEquals('text/csv', gdic.wrapperList[1].mimeType);
            System.assertEquals('Folder_1', gdic.wrapperList[2].name);  System.assertEquals('id_3', gdic.wrapperList[2].id);    System.assertEquals('application/vnd.google-apps.folder', gdic.wrapperList[2].mimeType);
        Test.stopTest();
    }

    //-------------------------------------DeleteFile Method Cases------------------------------------
    //Case-1 Not deleteing file successfully
     @isTest
    static void DeleteFile01(){
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.deleteFileId = 'id_4';
            gdic.DeleteFile();
        Test.stopTest();
    }

    //Case-2 Deleting file successfully
    @isTest
    static void DeleteFile02(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveIntegrationMockCallout());
        Datetime dt = System.now().addSeconds(3600);
        Integration_Tokens__c itc = new Integration_Tokens__c(Access_Token__c='access_token', Refresh_Token__c='refresh_token', Expire_Time__c=dt, Name__c='GDrive');
        insert itc;
        ApexPages.currentPage().getParameters().put('code', 'code_value');
        List<GoogleDriveIntegrationWrapper.WrapperFileList> fileList = new List<GoogleDriveIntegrationWrapper.WrapperFileList>();
        GoogleDriveIntegrationWrapper.WrapperFileList wF = new GoogleDriveIntegrationWrapper.WrapperFileList();
        wF.name = 'Folder_1';
        wF.id = 'id_1';
        wF.mimeType = 'folder';
        fileList.add(wF);
        wF = new GoogleDriveIntegrationWrapper.WrapperFileList();
        wF.name = 'Folder_2';
        wF.id = 'id_2';
        wF.mimeType = 'folder';
        fileList.add(wF);
        wF = new GoogleDriveIntegrationWrapper.WrapperFileList();
        wF.name = 'File_1';
        wF.id = 'id_3';
        wF.mimeType = 'file';
        fileList.add(wF);
        wF = new GoogleDriveIntegrationWrapper.WrapperFileList();
        wF.name = 'File_2';
        wF.id = 'id_4';
        wF.mimeType = 'file';
        fileList.add(wF);
        wF = new GoogleDriveIntegrationWrapper.WrapperFileList();
        wF.name = 'File_3';
        wF.id = 'id_5';
        wF.mimeType = 'file';
        fileList.add(wF);
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.deleteFileId = 'id_4';
            gdic.wrapperList = fileList;
            gdic.PageLoad();
            System.assertEquals(5, gdic.wrapperList.size());
            gdic.DeleteFile();
            System.assertEquals(4, gdic.wrapperList.size());
            System.assertEquals('Folder_1', gdic.wrapperList[0].name);  System.assertEquals('id_1', gdic.wrapperList[0].id);    System.assertEquals('folder', gdic.wrapperList[0].mimeType);
            System.assertEquals('Folder_2', gdic.wrapperList[1].name);  System.assertEquals('id_2', gdic.wrapperList[1].id);    System.assertEquals('folder', gdic.wrapperList[1].mimeType);
            System.assertEquals('File_1', gdic.wrapperList[2].name);    System.assertEquals('id_3', gdic.wrapperList[2].id);    System.assertEquals('file', gdic.wrapperList[2].mimeType);
            System.assertEquals('File_3', gdic.wrapperList[3].name);    System.assertEquals('id_5', gdic.wrapperList[3].id);    System.assertEquals('file', gdic.wrapperList[3].mimeType);
        Test.stopTest();
    }

    //-------------------------------------CreateFolder Method Cases------------------------------------
    //Case-1 Exception in creating folder
    @isTest
    static void CreateFolder01(){
        List<GoogleDriveIntegrationWrapper.WrapperDirectory> dirList = new List<GoogleDriveIntegrationWrapper.WrapperDirectory>();
        GoogleDriveIntegrationWrapper.WrapperDirectory wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Home';
        wD.selfId = 'root';
        dirList.add(wD);
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.directoryList = dirList;
            gdic.folderName = 'File/Folder_1';
            gdic.CreateFolder();
            System.assertEquals(0, gdic.wrapperList.size());
            System.assertEquals(1, gdic.directoryList.size());
        Test.stopTest();
    }

    //Case-2 Creating folder in an empty folder
    @isTest
    static void CreateFolder02(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveIntegrationMockCallout());
        Datetime dt = System.now().addSeconds(3600);
        Integration_Tokens__c itc = new Integration_Tokens__c(Access_Token__c='access_token', Refresh_Token__c='refresh_token', Expire_Time__c=dt, Name__c='GDrive');
        insert itc;
        ApexPages.currentPage().getParameters().put('code', 'code_value');
        List<GoogleDriveIntegrationWrapper.WrapperDirectory> dirList = new List<GoogleDriveIntegrationWrapper.WrapperDirectory>();
        GoogleDriveIntegrationWrapper.WrapperDirectory wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Home';
        wD.selfId = 'root';
        dirList.add(wD);
        wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Folder_1';
        wD.selfId = 'id_1';
        dirList.add(wD);
        wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Folder_2';
        wD.selfId = 'id_2';
        dirList.add(wD);
        wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Folder_3';
        wD.selfId = 'id_3';
        dirList.add(wD);
        List<GoogleDriveIntegrationWrapper.WrapperFileList> fileList = new List<GoogleDriveIntegrationWrapper.WrapperFileList>();
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.directoryList = dirList;
            gdic.folderName = 'File/Folder_1';
            gdic.wrapperList = fileList;
            gdic.PageLoad();
            gdic.CreateFolder();
            System.assertEquals(1, gdic.wrapperList.size());
            System.assertEquals('File/Folder_1', gdic.wrapperList[0].name);  System.assertEquals('id_4', gdic.wrapperList[0].id);
        Test.stopTest();
    }

    //Case-3 Creating folder in folder which is not empty
    @isTest
    static void CreateFolder03(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveIntegrationMockCallout());
        Datetime dt = System.now().addSeconds(3600);
        Integration_Tokens__c itc = new Integration_Tokens__c(Access_Token__c='access_token', Refresh_Token__c='refresh_token', Expire_Time__c=dt, Name__c='GDrive');
        insert itc;
        ApexPages.currentPage().getParameters().put('code', 'code_value');
        List<GoogleDriveIntegrationWrapper.WrapperDirectory> dirList = new List<GoogleDriveIntegrationWrapper.WrapperDirectory>();
        GoogleDriveIntegrationWrapper.WrapperDirectory wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Home';
        wD.selfId = 'root';
        dirList.add(wD);
        List<GoogleDriveIntegrationWrapper.WrapperFileList> fileList = new List<GoogleDriveIntegrationWrapper.WrapperFileList>();
        GoogleDriveIntegrationWrapper.WrapperFileList wF = new GoogleDriveIntegrationWrapper.WrapperFileList();
        wF.name = 'Folder_1';
        wF.id = 'id_1';
        wF.mimeType = 'application/vnd.google-apps.folder';
        fileList.add(wF);
        wF = new GoogleDriveIntegrationWrapper.WrapperFileList();
        wF.name = 'File_1';
        wF.id = 'id_2';
        wF.mimeType = 'file';
        fileList.add(wF);
        wF = new GoogleDriveIntegrationWrapper.WrapperFileList();
        wF.name = 'File_2';
        wF.id = 'id_3';
        wF.mimeType = 'file';
        fileList.add(wF);
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.directoryList = dirList;
            gdic.folderName = 'File/Folder_1';
            gdic.wrapperList = fileList;
            gdic.PageLoad();
            gdic.CreateFolder();
            System.assertEquals(4, gdic.wrapperList.size());
            System.assertEquals('Folder_1', gdic.wrapperList[0].name);  System.assertEquals('id_1', gdic.wrapperList[0].id);
            System.assertEquals('File/Folder_1', gdic.wrapperList[1].name);  System.assertEquals('id_4', gdic.wrapperList[1].id);
            System.assertEquals('File_1', gdic.wrapperList[2].name);  System.assertEquals('id_2', gdic.wrapperList[2].id);
            System.assertEquals('File_2', gdic.wrapperList[3].name);  System.assertEquals('id_3', gdic.wrapperList[3].id);
        Test.stopTest();
    }

    //-------------------------------------FileUpload Method Cases------------------------------------
    //Case-1 Exception in file Uploading
    @isTest
    static void FileUpload01(){
        List<GoogleDriveIntegrationWrapper.WrapperDirectory> dirList = new List<GoogleDriveIntegrationWrapper.WrapperDirectory>();
        GoogleDriveIntegrationWrapper.WrapperDirectory wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Home';
        wD.selfId = 'root';
        dirList.add(wD);
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.directoryList = dirList;
            gdic.fileBody = 'Hello, how are you?\nI am fine.\nThank You.';
            gdic.fileName = 'File/Folder_1';
            gdic.fileType = 'file';
            gdic.FileUpload();
            System.assertEquals(0, gdic.wrapperList.size());
        Test.stopTest();
    }

    //Case-2 File uploaded successfully
    @isTest
    static void FileUpload02(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveIntegrationMockCallout());
        Datetime dt = System.now().addSeconds(3600);
        Integration_Tokens__c itc = new Integration_Tokens__c(Access_Token__c='access_token', Refresh_Token__c='refresh_token', Expire_Time__c=dt, Name__c='GDrive');
        insert itc;
        ApexPages.currentPage().getParameters().put('code', 'code_value');
        List<GoogleDriveIntegrationWrapper.WrapperDirectory> dirList = new List<GoogleDriveIntegrationWrapper.WrapperDirectory>();
        GoogleDriveIntegrationWrapper.WrapperDirectory wD = new GoogleDriveIntegrationWrapper.WrapperDirectory();
        wD.name = 'Home';
        wD.selfId = 'root';
        dirList.add(wD);
        Test.startTest();
            GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
            gdic.directoryList = dirList;
            gdic.fileBody = 'Hello, how are you?\nI am fine.\nThank You.';
            gdic.fileName = 'File/Folder_1';
            gdic.fileType = 'file';
            gdic.PageLoad();
            gdic.FileUpload();
            System.assertEquals(1, gdic.wrapperList.size());
            System.assertEquals('File/Folder_1', gdic.wrapperList[0].name);  System.assertEquals('id_4', gdic.wrapperList[0].id);
        Test.stopTest();
    }

    //-------------------------------------Service Class AccessToken Method Cases------------------------------------
    //Case-1 When access token is expired
    @isTest
    static void ServiceAccessToken01(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveIntegrationMockCallout());
        Datetime dt = System.now();
        Integration_Tokens__c itc = new Integration_Tokens__c(Access_Token__c='access_token', Refresh_Token__c='refresh_token', Expire_Time__c=dt, Name__c='GDrive');
        insert itc;
        Test.startTest();
            GoogleDriveIntegrationService.AccessToken('code_value');
            Integration_Tokens__c itc1 = [SELECT Access_Token__c FROM Integration_Tokens__c WHERE Name__c = 'GDrive'];
            System.assertEquals('acc_token', itc1.Access_Token__c);
        Test.stopTest();
    }

    //Case-2  Getting access token and refresh token for first authorization
    @isTest
    static void ServiceAccessToken02(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveIntegrationMockCallout());
        Integration_Tokens__c itc = new Integration_Tokens__c(Name__c='GDrive');
        insert itc;
        Test.startTest();
            GoogleDriveIntegrationService.AccessToken('code_value');
            Integration_Tokens__c itc1 = [SELECT Access_Token__c, Refresh_Token__c FROM Integration_Tokens__c WHERE Name__c = 'GDrive'];
            System.assertEquals('acc_token', itc1.Access_Token__c);
            System.assertEquals('ref_token', itc1.Refresh_Token__c);
        Test.stopTest();
    }
}