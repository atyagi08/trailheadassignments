/**
**@Description: Apex Controller Class for communityBox Component
**
**@Author: Akhil Tyagi
**@Date: 20 January 2020
**
**@ChangeLog:
**21 January 2020: Created DwnldFile, DeleteFile and CreateFolder Method
**23 January 2020: Created FileUpload and base64EncodeFile Method
***/

public class commBoxController {

    /***************************************************************************************************************
    **@Description: Generate Access Token or Authorization URL based on condition.                                **
    **                                                                                                            **
    **@Parameter:                                                                                                 **
    **                                                                                                            **
    **@Return String with access token or authorization URL                                                       **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String getAccessToken() {
        // System.debug('@@@getAccessToken Method @@@');
        String accessToken = commBoxService.AccessToken();
        if(accessToken != 'AuthorizationRequired') {
            return accessToken;
        }
        User usr                          = [SELECT Username FROM User WHERE Id = :UserInfo.getUserId()];
        CommunityIntegrationKey__mdt keys = [SELECT Client_Id__c, Redirect_URl__c 
                                            FROM CommunityIntegrationKey__mdt 
                                            WHERE MasterLabel = :usr.Username AND Cloud_Name__c = 'Box'];
                                            
        String client_id    = EncodingUtil.urlEncode(keys.Client_Id__c,'UTF-8');
        String redirect_uri = EncodingUtil.urlEncode(keys.Redirect_URl__c,'UTF-8');
        String pageUrl      = 'https://account.box.com/api/oauth2/authorize?'+
                              'client_id='+client_id+
                              '&redirect_uri='+redirect_uri+
                              '&response_type=code'+
                              '&state=Box';
        return pageUrl;
    }

    /***************************************************************************************************************
    **@Description: Generate Access Token for first time authorization.                                           **
    **                                                                                                            **
    **@Parameter code: Code used to generate access token                                                         **
    **                                                                                                            **
    **@Return String with access token                                                                            **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String AuthorizeUser(String code) {
        // System.debug('@@@AuthorizeUser Method @@@');
        return commBoxService.AuthUser(code);
    }

    /***************************************************************************************************************
    **@Description: Generate List of file and folders in current directory.                                       **
    **                                                                                                            **
    **@Parameter folderId: Id of the current directory(folder)                                                    **
    **                                                                                                            **
    **@Return JSON String with list of file and folder in current directory                                       **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String RecordsList(String folderId){
        // System.debug('@@@RecordsList Method @@@');
        String accToken    = getAccessToken();
        String endpoint    = 'https://api.box.com/2.0/folders/'+folderId;

        commBoxWrapper.WrapperWhole wrapperinstance = commBoxService.GetResponse(endpoint, accToken);
        commBoxWrapper.WrapperItems items = wrapperinstance.item_collection;
        return JSON.serialize(items);
    }

    /***************************************************************************************************************
    **@Description: Generate link to download the file.                                                           **
    **                                                                                                            **
    **@Parameter fileId: Id of the file to be downloaded.                                                         **
    **                                                                                                            **
    **@Return String with download link of the file                                                               **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String DwnldFile(String fileId) {
        // System.debug('@@@DwnldFile Method @@@');
        String accToken = getAccessToken();
        String endpoint = 'https://api.box.com/2.0/files/' + fileId + '/content';

        commBoxWrapper.WrapperWhole wrapperinstance = commBoxService.GetResponse(endpoint, accToken);
        return wrapperinstance.location;
    }

    /***************************************************************************************************************
    **@Description: Delete the file or folder.                                                                    **
    **                                                                                                            **
    **@Parameter fil_fldrId: Id of the file or folder to be deleted.                                              **
    **                                                                                                            **
    **@Return String to state wether the file/folder is deleted or not.                                           **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String DeleteFile(String fil_fldrId) {
        // System.debug('@@@DeleteFile Method @@@');
        String accToken = getAccessToken();
        String endpoint = 'https://api.box.com/2.0/' + fil_fldrId;

        Integer status = commBoxService.DeleteResponse(endpoint, accToken);
        if(status == 204){
            return 'Successfully Deleted';
        }
        return 'File not deleted successfully';
    }

    /***************************************************************************************************************
    **@Description: Create folder in current directory(folder).                                                   **
    **                                                                                                            **
    **@Parameter folder_name: Name of the folder to be created.                                                   **
    **@Parameter folder_id: Id of the current directory(folder).                                                  **
    **                                                                                                            **
    **@Return JSON String with details of the uploaded file/folder.                                               **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String CreateFolder(String folder_name, String folder_id){
        // System.debug('@@@CreateFolder Method @@@');
        String accToken    = getAccessToken();
        String endpoint    = 'https://api.box.com/2.0/folders';
        String contentType = 'application/json';
        String body        = '{'+
                                '"name": "'+folder_name+'",'+
                                '"parent": {'+
                                    '"id":"'+folder_id+'"'+
                                '}'+
                             '}';

        commBoxWrapper.WrapperFileList wrapper = commBoxService.PostResponse(endpoint, accToken, body, contentType);
        return JSON.serialize(wrapper);
    }

    /***************************************************************************************************************
    **@Description: Upload file to current directory(folder).                                                     **
    **                                                                                                            **
    **@Parameter fileBody: Body of the file.                                                                      **
    **@Parameter fileName: Name of the file.                                                                      **
    **@Parameter folder_id: Id of the current directory(folder).                                                  **
    **                                                                                                            **
    **@Return JSON String with details of the uploaded file.                                                      **
    ***************************************************************************************************************/

    @AuraEnabled
    public static String FileUpload( String fileBody, String fileName, String folderId) {
        // System.debug('@@@FileUpload Method @@@');
        String accToken         = getAccessToken();
        String endpoint         = 'https://upload.box.com/api/2.0/files/content?parent_id='+folderId;
        String boundary         = '--------------------------9fd09388d840fef1';
        String contentType      = 'multipart/form-data; boundary='+boundary;
        Blob   base64EncodeFile = base64EncodeFile(EncodingUtil.base64Decode(fileBody), fileName);
        String base64File       = EncodingUtil.base64Encode(base64EncodeFile);

        commBoxWrapper.WrapperFileList wrapper = commBoxService.PostResponse(endpoint, accToken, base64File, contentType);
        return JSON.serialize(wrapper);
    }

    /***************************************************************************************************************
    **@Description: Encode the uploaded file.                                                                     **
    **                                                                                                            **
    **@Parameter fileBody: Body of the file.                                                                      **
    **@Parameter fileName: Name of the file.                                                                      **
    **                                                                                                            **
    **@Return Body of the file as Blob.                                                                           **
    ***************************************************************************************************************/

    private static Blob base64EncodeFile(Blob file_body, String file_name) {
        // System.debug('@@@base64EncodeFileContent Method @@@');
        String boundary = '--------------------------9fd09388d840fef1';
        String header   = '--'+boundary+'\n'+
                          'Content-Disposition: form-data; name="file"; filename="'+file_name+'";\n'+
                          'Content-Type: application/octet-stream';
        String footer   = '--'+boundary+'--';

        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        while(headerEncoded.endsWith('=')) {
            header+=' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }

        String bodyEncoded = EncodingUtil.base64Encode(file_body);
        Blob   bodyBlob    = null;
        String last4Bytes  = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
 
        if(last4Bytes.endsWith('==')) {
            last4Bytes           = last4Bytes.substring(0,2) + '0K';
            bodyEncoded          = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob             = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        } else if(last4Bytes.endsWith('=')) {
            last4Bytes           = last4Bytes.substring(0,3) + 'N';
            bodyEncoded          = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            footer               = '\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob             = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        } else {
            footer               = '\r\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob             = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        }

        return bodyBlob;
    }

}