@isTest
private class TestSequenceNumberManagerOnContact {
    
    @testSetup
    static void setupTestMethod(){
        List<Account> accList = new List<Account>();
        for(Integer i=1; i<=5; i++){
            accList.add(new Account(Name='Account '+ i));
        }
        insert accList;
        List<Contact> conList = new List<Contact>();
        conList.add(new Contact(LastName='Contact 1', AccountId = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id));
        conList.add(new Contact(LastName='Contact 2', AccountId = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id));
        conList.add(new Contact(LastName='Contact 3', AccountId = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id));
        conList.add(new Contact(LastName='Contact 4', AccountId = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id));
        conList.add(new Contact(LastName='Contact 5', AccountId = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id));
        conList.add(new Contact(LastName='Contact 6', AccountId = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id));
        conList.add(new Contact(LastName='Contact 7', AccountId = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id));
        conList.add(new Contact(LastName='Contact 8', AccountId = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id));
        conList.add(new Contact(LastName='Contact 9', AccountId = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id));
        conList.add(new Contact(LastName='Contact 10', AccountId = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id));
        insert conList;
    }
    
    @isTest
    static void insertTestMethod(){
        Id idAccount;
        List<Contact> insertList = new List<Contact>();
        List<Integer> intList = new List<Integer>{1, 2, 3, 4};
        
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id;
        for(Integer i=11; i<=14; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id;
        for(Integer i=15; i<=18; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id;
        for(Integer i=19; i<=22; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 4'].Id;
        for(Integer i=23; i<=26; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 5'].Id;
        for(Integer i=27; i<=30; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
		Test.startTest();
        	insert insertList;        	
        	List<Integer> ckeckList = new List<Integer>();
            for(Contact c : [SELECT Sequence_Number__c FROM Contact WHERE AccountId = :idAccount]){
                ckeckList.add(Integer.valueOf(c.Sequence_Number__c));
            }
        	ckeckList.sort();
        	System.assertEquals(intList, ckeckList);
		Test.stopTest();
    }
    
    @isTest
    static void deleteTestMethod(){
        Id idAccount;
        List<Contact> deleteList = new List<Contact>();
        List<Contact> insertList = new List<Contact>();
        List<Integer> intList = new List<Integer>{1, 2, 3, 4, 5};
        
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id;
        for(Integer i=11; i<=14; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id;
        for(Integer i=15; i<=18; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id;
        for(Integer i=19; i<=22; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        insert insertList;
        
        for(Integer i=11; i<=12; i++){
            deleteList.add(Database.query('SELECT Id FROM Contact WHERE LastName = \'Contact '+i+'\''));
        }
        for(Integer i=15; i<=16; i++){
            deleteList.add(Database.query('SELECT Id FROM Contact WHERE LastName = \'Contact '+i+'\''));
        }
        for(Integer i=19; i<=20; i++){
            deleteList.add(Database.query('SELECT Id FROM Contact WHERE LastName = \'Contact '+i+'\''));
        }
        
		Test.startTest();
        	delete deleteList;
        	List<Integer> ckeckList = new List<Integer>();
        	idAccount = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id;
            for(Contact c : [SELECT Sequence_Number__c FROM Contact WHERE AccountId = :idAccount]){
                ckeckList.add(Integer.valueOf(c.Sequence_Number__c));
            }
        	ckeckList.sort();
        	System.assertEquals(intList, ckeckList);
		Test.stopTest();
    }
    
    @isTest
    static void undeleteTestMethod(){
        Id idAccount;
        List<Contact> deleteList = new List<Contact>();
        List<Contact> insertList = new List<Contact>();
        List<Contact> undeleteList = new List<Contact>();
        List<Integer> intList = new List<Integer>{4, 5, 6, 7};
        
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id;
        for(Integer i=11; i<=14; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id;
        for(Integer i=15; i<=18; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id;
        for(Integer i=19; i<=22; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        insert insertList;
        
        for(Integer i=11; i<=22; i++){
            deleteList.add(Database.query('SELECT Id FROM Contact WHERE LastName = \'Contact '+i+'\''));
        }
        
        for(Integer i=11; i<=22; i++){
            undeleteList.add(Database.query('SELECT LastName, Id FROM Contact WHERE LastName = \'Contact '+i+'\' ALL ROWS'));
        }
        
		Test.startTest();
        	delete deleteList;
        	undelete undeleteList;
        	List<Integer> ckeckList = new List<Integer>();
        	idAccount = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id;
            for(Contact c : [SELECT LastName, Sequence_Number__c FROM Contact WHERE AccountId = :idAccount]){
                if(c.LastName == 'Contact 11' || c.LastName == 'Contact 12' || c.LastName == 'Contact 13' || c.LastName == 'Contact 14'){
                    ckeckList.add(Integer.valueOf(c.Sequence_Number__c));
                }
            }
        	ckeckList.sort();
        	System.assertEquals(intList, ckeckList);
		Test.stopTest();
    }
    
    @isTest
    static void updateTestMethodCaseSimple(){
        Id idAccount;
        List<Contact> updateList = new List<Contact>();
        List<Contact> insertList = new List<Contact>();
        List<Integer> intList = new List<Integer>{1, 2, 3, 4, 5, 6, 7};
        
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id;
        for(Integer i=11; i<=14; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id;
        for(Integer i=15; i<=18; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id;
        for(Integer i=19; i<=22; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        insert insertList;
        
        for(Integer i=11; i<=22; i++){
            Contact c = Database.query('SELECT Id, FirstName FROM Contact WHERE LastName = \'Contact '+i+'\'');
            c.FirstName = 'Testing';
            updateList.add(c);
        }
        
		Test.startTest();
        	update updateList;
        	List<Integer> ckeckList = new List<Integer>();
        	idAccount = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id;
            for(Contact c : [SELECT Sequence_Number__c FROM Contact WHERE AccountId = :idAccount]){
                ckeckList.add(Integer.valueOf(c.Sequence_Number__c));
            }
        	ckeckList.sort();
        	System.assertEquals(intList, ckeckList);
		Test.stopTest();
    }
    
    @isTest
    static void updateTestMethodCaseReparenting(){
        Id idAccount;
        List<Contact> updateList = new List<Contact>();
        List<Contact> insertList = new List<Contact>();
        List<Integer> intListA2 = new List<Integer>{1, 2, 3};
        List<Integer> intListA3 = new List<Integer>{9, 10, 11, 12};
        
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id;
        for(Integer i=11; i<=14; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id;
        for(Integer i=15; i<=18; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id;
        for(Integer i=19; i<=22; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        insert insertList;
        
        for(Integer i=15; i<=18; i++){
            Contact c = Database.query('SELECT Id, AccountId FROM Contact WHERE LastName = \'Contact '+i+'\'');
            c.AccountId = idAccount;
            updateList.add(c);
        }
        
		Test.startTest();
        	update updateList;
        	List<Integer> ckeckList = new List<Integer>();
        	idAccount = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id;
            for(Contact c : [SELECT Sequence_Number__c FROM Contact WHERE AccountId = :idAccount]){
                ckeckList.add(Integer.valueOf(c.Sequence_Number__c));
            }
        	ckeckList.sort();
        	System.assertEquals(intListA2, ckeckList);
        	ckeckList.clear();
        	idAccount = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id;
        	for(Contact c : [SELECT Sequence_Number__c, LastName FROM Contact WHERE AccountId = :idAccount]){
                if(c.LastName == 'Contact 15' || c.LastName == 'Contact 16' || c.LastName == 'Contact 17' || c.LastName == 'Contact 18'){
                    ckeckList.add(Integer.valueOf(c.Sequence_Number__c));
                }
            }
        	ckeckList.sort();
        	System.assertEquals(intListA3, ckeckList);        	
		Test.stopTest();
    }
    
    @isTest
    static void updateTestMethodCaseRemovingParent(){
        Id idAccount;
        List<Contact> updateList = new List<Contact>();
        List<Contact> insertList = new List<Contact>();
        List<Integer> intListA2 = new List<Integer>{1, 2, 3};
        
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 1'].Id;
        for(Integer i=11; i<=14; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id;
        for(Integer i=15; i<=18; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        idAccount = [SELECT Id FROM Account WHERE Name = 'Account 3'].Id;
        for(Integer i=19; i<=22; i++){
            insertList.add(new Contact(LastName='Contact '+ i, AccountId = idAccount));
        }
        insert insertList;
        
        for(Integer i=15; i<=18; i++){
            Contact c = Database.query('SELECT Id, AccountId FROM Contact WHERE LastName = \'Contact '+i+'\'');
            c.AccountId = null;
            c.FirstName = 'Testing';
            updateList.add(c);
        }
        
		Test.startTest();
        	update updateList;
        	List<Integer> ckeckList = new List<Integer>();
        	idAccount = [SELECT Id FROM Account WHERE Name = 'Account 2'].Id;
            for(Contact c : [SELECT Sequence_Number__c FROM Contact WHERE AccountId = :idAccount]){
                ckeckList.add(Integer.valueOf(c.Sequence_Number__c));
            }
        	ckeckList.sort();
        	System.assertEquals(intListA2, ckeckList);
        	for(Contact c : [SELECT Sequence_Number__c, LastName FROM Contact WHERE Name LIKE 'Contact 1%']){
                if(c.LastName == 'Contact 15' || c.LastName == 'Contact 16' || c.LastName == 'Contact 17' || c.LastName == 'Contact 18'){
                    System.assertEquals(null, c.Sequence_Number__c);
                }
            }
		Test.stopTest();
    }
}