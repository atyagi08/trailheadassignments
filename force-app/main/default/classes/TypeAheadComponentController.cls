global  with sharing class TypeAheadComponentController {

    public String   objectName{ get; set; }                                     //Selected objectName
    public String   fieldName{ get; set; }                                      //Selected field Name
    public String   inputText{ get; set; }                                      //Selected record
    public Boolean  renderVariable{ get; set; }                                 //Variable to render pageBlock with record details
    public Id       recordId{ get; set; }                                       //Selected Record Id

    public TypeAheadComponentController(){                                      //Constructor
        objectName      = '';
        fieldName       = '';
        inputText       = '';
        renderVariable  = false;
    }

    @RemoteAction                                                               //Remote Action to compile the typeAhead List
    global static List<String> SearchListMethod(String objName, String fldName, String search){
        List<SObject> tempList  = Database.query('SELECT ' + fldName + ' FROM ' + objName + ' WHERE ' + fldName + ' LIKE \'' + search + '%\' ORDER BY ' + fldName);
        List<String> searchList = new List<String>();
        for(SObject obj : tempList){
            searchList.add(String.valueOf(obj).substringBetween('=',','));
        }
        return searchList;
    }

    public void RecordDetailsMethod(){                                          //Method to get the record details
        renderVariable  = true;
        inputText       = inputText.unescapeHtml4();

        try{
            recordId = Database.query('SELECT Id FROM '+ objectName + ' WHERE ' + fieldName + ' = \'' + inputText + '\' ORDER By ' + fieldName + ' LIMIT 1').Id;
        }
        catch (Exception e){
            renderVariable  = false;
            inputText       = '';
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No record found for the entered value.');
            ApexPages.addMessage(myMsg);           
        } 
    }
}