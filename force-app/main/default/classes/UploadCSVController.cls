public with sharing class UploadCSVController {

    //Public Variables--------------------------------------------------------------------------------------------------------------------
    public transient Blob    fileBody{ get; set; }                  //fileBody
    public transient String  fileName{ get; set; }                  //fileName
    public transient Integer fileSize{ get; set; }                  //fileSize

    //Private Variables-------------------------------------------------------------------------------------------------------------------
    String fileBodyAsString = '';                                   //String value of file Body
    String header           = '';                                   //String of fields in Contact Object's fields which are avaiable in CSV
    List<String> fieldNamesList;                                    //List of fields in Contact Object's fields which are avaiable in CSV

    //Constructor-------------------------------------------------------------------------------------------------------------------------
    public UploadCSVController(){
        fieldNamesList      = new List<String>();
    }

    //Method to check file size and whether the file body is readable---------------------------------------------------------------------
    public void ReadFromFile(){
		if(fileSize < 680760 && fileName.substringAfterLast('.').equalsIgnoreCase('csv')){      //Checking file size and extension
            try{
                fileBodyAsString = fileBody.toString();
                ReadCSVFile();
            }
            catch(exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error reading CSV file'));
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No File Chosen or,'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'File Size greater than 500 KB or,'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'File is not of CSV type'));
        }
    }

    //Method to read the file and create a contact list which will be inserted and check exceptions in CSV file---------------------------
    private void ReadCSVFile(){
        fieldNamesList.clear();
        header = fileBodyAsString.substringBefore('\n');
        fileBodyAsString = fileBodyAsString.substringAfter('\n');
        for(String str : header.split('\n')[0].split(',')){
            fieldNamesList.add(str.trim());
        }

        if(fieldNamesList[0].equalsIgnoreCase('LastName')){         //Calling batch to insert records
            ReadAndPopulateBatch readBatch = new ReadAndPopulateBatch(fieldNamesList, fileBodyAsString);
            Id batchId = Database.executeBatch(readBatch, 1);
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'First Column in CSV file is not LastName. Set the first column in CSV file as LastName'));
        }
    }
}