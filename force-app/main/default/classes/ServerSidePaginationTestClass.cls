@isTest
public class ServerSidePaginationTestClass {

    @isTest
    static void method01(){
        List<Account> accList = new List<Account>();
        Account a = new Account(Name='Alpha');  accList.add(a);
        a = new Account(Name='Beta');           accList.add(a);
        a = new Account(Name='Charlie');        accList.add(a);
        a = new Account(Name='Delta');          accList.add(a);
        a = new Account(Name='Echo');           accList.add(a);
        a = new Account(Name='Foxtrot');        accList.add(a);
        a = new Account(Name='Golf');           accList.add(a);
        a = new Account(Name='Hotel');          accList.add(a);
        a = new Account(Name='India');          accList.add(a);
        a = new Account(Name='Juliet');         accList.add(a);
        a = new Account(Name='Kilo');           accList.add(a);
        a = new Account(Name='Lima');           accList.add(a);
        a = new Account(Name='Mike');           accList.add(a);
        a = new Account(Name='November');       accList.add(a);
        a = new Account(Name='Oscar');          accList.add(a);
        a = new Account(Name='Papa');           accList.add(a);
        a = new Account(Name='Quebec');         accList.add(a);
        a = new Account(Name='Romeo');          accList.add(a);
        a = new Account(Name='Sierra');         accList.add(a);
        a = new Account(Name='Tango');          accList.add(a);
        a = new Account(Name='Uniform');        accList.add(a);
        a = new Account(Name='Victor');         accList.add(a);
        a = new Account(Name='Whiskey');        accList.add(a);
        a = new Account(Name='X-ray');          accList.add(a);
        a = new Account(Name='Yankee');         accList.add(a);
        a = new Account(Name='Zulu');           accList.add(a);
        a = new Account(Name='#Number');        accList.add(a);
        a = new Account(Name='#!gdhf');         accList.add(a);
        insert accList;

        Test.startTest();
        ServerSidePaginationController.getAccounts(10, '', '', '>');
        Test.stopTest();
    }

    @isTest
    static void method02(){
        List<Account> accList = new List<Account>();
        Account a = new Account(Name='Alpha');  accList.add(a);
        a = new Account(Name='Beta');           accList.add(a);
        a = new Account(Name='Charlie');        accList.add(a);
        a = new Account(Name='Delta');          accList.add(a);
        a = new Account(Name='Echo');           accList.add(a);
        a = new Account(Name='Foxtrot');        accList.add(a);
        a = new Account(Name='Golf');           accList.add(a);
        a = new Account(Name='Hotel');          accList.add(a);
        a = new Account(Name='India');          accList.add(a);
        a = new Account(Name='Juliet');         accList.add(a);
        a = new Account(Name='Kilo');           accList.add(a);
        a = new Account(Name='Lima');           accList.add(a);
        a = new Account(Name='Mike');           accList.add(a);
        a = new Account(Name='November');       accList.add(a);
        a = new Account(Name='Oscar');          accList.add(a);
        a = new Account(Name='Papa');           accList.add(a);
        a = new Account(Name='Quebec');         accList.add(a);
        a = new Account(Name='Romeo');          accList.add(a);
        a = new Account(Name='Sierra');         accList.add(a);
        a = new Account(Name='Tango');          accList.add(a);
        a = new Account(Name='Uniform');        accList.add(a);
        a = new Account(Name='Victor');         accList.add(a);
        a = new Account(Name='Whiskey');        accList.add(a);
        a = new Account(Name='X-ray');          accList.add(a);
        a = new Account(Name='Yankee');         accList.add(a);
        a = new Account(Name='Zulu');           accList.add(a);
        a = new Account(Name='#Number');        accList.add(a);
        a = new Account(Name='#!gdhf');         accList.add(a);
        insert accList;

        Test.startTest();
        ServerSidePaginationController.getAccounts(10, '', 'last', '>');
        Test.stopTest();
    }

    @isTest
    static void method03(){
        List<Account> accList = new List<Account>();
        Account a = new Account(Name='Alpha');  accList.add(a);
        a = new Account(Name='Beta');           accList.add(a);
        a = new Account(Name='Charlie');        accList.add(a);
        a = new Account(Name='Delta');          accList.add(a);
        a = new Account(Name='Echo');           accList.add(a);
        a = new Account(Name='Foxtrot');        accList.add(a);
        a = new Account(Name='Golf');           accList.add(a);
        a = new Account(Name='Hotel');          accList.add(a);
        a = new Account(Name='India');          accList.add(a);
        a = new Account(Name='Juliet');         accList.add(a);
        a = new Account(Name='Kilo');           accList.add(a);
        a = new Account(Name='Lima');           accList.add(a);
        a = new Account(Name='Mike');           accList.add(a);
        a = new Account(Name='November');       accList.add(a);
        a = new Account(Name='Oscar');          accList.add(a);
        a = new Account(Name='Papa');           accList.add(a);
        a = new Account(Name='Quebec');         accList.add(a);
        a = new Account(Name='Romeo');          accList.add(a);
        a = new Account(Name='Sierra');         accList.add(a);
        a = new Account(Name='Tango');          accList.add(a);
        a = new Account(Name='Uniform');        accList.add(a);
        a = new Account(Name='Victor');         accList.add(a);
        a = new Account(Name='Whiskey');        accList.add(a);
        a = new Account(Name='X-ray');          accList.add(a);
        a = new Account(Name='Yankee');         accList.add(a);
        a = new Account(Name='Zulu');           accList.add(a);
        a = new Account(Name='#Number');        accList.add(a);
        a = new Account(Name='#!gdhf');         accList.add(a);
        a = new Account(Name='!Exclamation');   accList.add(a);
        a = new Account(Name='Alpha1');         accList.add(a);
        insert accList;

        Test.startTest();
        ServerSidePaginationController.getAccounts(10, '', 'last', '>');
        Test.stopTest();
    }

    @isTest
    static void method04(){
        List<Account> accList = new List<Account>();
        Account a = new Account(Name='Alpha');  accList.add(a);
        a = new Account(Name='Beta');           accList.add(a);
        a = new Account(Name='Charlie');        accList.add(a);
        a = new Account(Name='Delta');          accList.add(a);
        a = new Account(Name='Echo');           accList.add(a);
        a = new Account(Name='Foxtrot');        accList.add(a);
        a = new Account(Name='Golf');           accList.add(a);
        a = new Account(Name='Hotel');          accList.add(a);
        a = new Account(Name='India');          accList.add(a);
        a = new Account(Name='Juliet');         accList.add(a);
        a = new Account(Name='Kilo');           accList.add(a);
        a = new Account(Name='Lima');           accList.add(a);
        a = new Account(Name='Mike');           accList.add(a);
        a = new Account(Name='November');       accList.add(a);
        a = new Account(Name='Oscar');          accList.add(a);
        a = new Account(Name='Papa');           accList.add(a);
        a = new Account(Name='Quebec');         accList.add(a);
        a = new Account(Name='Romeo');          accList.add(a);
        a = new Account(Name='Sierra');         accList.add(a);
        a = new Account(Name='Tango');          accList.add(a);
        a = new Account(Name='Uniform');        accList.add(a);
        a = new Account(Name='Victor');         accList.add(a);
        a = new Account(Name='Whiskey');        accList.add(a);
        a = new Account(Name='X-ray');          accList.add(a);
        a = new Account(Name='Yankee');         accList.add(a);
        a = new Account(Name='Zulu');           accList.add(a);
        a = new Account(Name='#Number');        accList.add(a);
        a = new Account(Name='#!gdhf');         accList.add(a);
        a = new Account(Name='!Exclamation');   accList.add(a);
        a = new Account(Name='Alpha1');         accList.add(a);
        insert accList;

        Test.startTest();
        ServerSidePaginationController.getAccounts(10, '', accList[10].Id + 'previous', '>');
        Test.stopTest();
    }

    @isTest
    static void method05(){
        List<Account> accList = new List<Account>();
        Account a = new Account(Name='Alpha');  accList.add(a);
        a = new Account(Name='Beta');           accList.add(a);
        a = new Account(Name='Charlie');        accList.add(a);
        a = new Account(Name='Delta');          accList.add(a);
        a = new Account(Name='Echo');           accList.add(a);
        a = new Account(Name='Foxtrot');        accList.add(a);
        a = new Account(Name='Golf');           accList.add(a);
        a = new Account(Name='Hotel');          accList.add(a);
        a = new Account(Name='India');          accList.add(a);
        a = new Account(Name='Juliet');         accList.add(a);
        a = new Account(Name='Kilo');           accList.add(a);
        a = new Account(Name='Lima');           accList.add(a);
        a = new Account(Name='Mike');           accList.add(a);
        a = new Account(Name='November');       accList.add(a);
        a = new Account(Name='Oscar');          accList.add(a);
        a = new Account(Name='Papa');           accList.add(a);
        a = new Account(Name='Quebec');         accList.add(a);
        a = new Account(Name='Romeo');          accList.add(a);
        a = new Account(Name='Sierra');         accList.add(a);
        a = new Account(Name='Tango');          accList.add(a);
        a = new Account(Name='Uniform');        accList.add(a);
        a = new Account(Name='Victor');         accList.add(a);
        a = new Account(Name='Whiskey');        accList.add(a);
        a = new Account(Name='X-ray');          accList.add(a);
        a = new Account(Name='Yankee');         accList.add(a);
        a = new Account(Name='Zulu');           accList.add(a);
        a = new Account(Name='#Number');        accList.add(a);
        a = new Account(Name='#!gdhf');         accList.add(a);
        a = new Account(Name='!Exclamation');   accList.add(a);
        a = new Account(Name='Alpha1');         accList.add(a);
        insert accList;

        List<String> strList = new List<String>();
        strList.add(accList[0].Id);
        strList.add(accList[5].Id);
        strList.add(accList[10].Id);
        strList.add(accList[15].Id);
        strList.add(accList[20].Id);
        Test.startTest();
        ServerSidePaginationController.deleteAccount(strList);
        Integer count = [SELECT COUNT() FROM Account];
        System.assertEquals(25, count);
        Test.stopTest();
    }
}