({
    
    /***************************************************************************************************************
    **@Description: Generate access token and fetch list of files and folders.                                    **
    ***************************************************************************************************************/
    
    doInit : function(component, event, helper) {
        // console.log('@@@doInit Method@@@');
		let url = new URL(window.location.href);
        let cod = url.searchParams.get("code");

        if(cod == null) {
            let action = component.get("c.getAccessToken");
            action.setCallback(this, function(response) {
                let status = response.getState();
                if(status === 'SUCCESS') {
					let result = response.getReturnValue();
                    // console.log(result);
                    if(result.includes('redirect_uri')) {
                        window.location.href = result;
                    }
                    helper.recordList(component, 'root');
                } else {
                    console.log('Error');
                }
            });
            $A.enqueueAction(action);
        } else {
            let action = component.get("c.AuthorizeUser");
            action.setParams({code: cod});
            action.setCallback(this, function(response) {
                let status = response.getState();
                if(status === 'SUCCESS') {
					// console.log(response.getReturnValue());
                    helper.recordList(component, 'root');
                } else {
                    console.log('Error');
                }
            });
            $A.enqueueAction(action);
        }

        let dir_list = [];
        let dir      = {name:"Home", id:"root", path_lower:""};
        dir_list[0]  = dir;
        // console.log(dir_list);
        component.set("v.dirList", dir_list);
    },
    
    /***************************************************************************************************************
    **@Description: Open folder on clicking the folder.                                                           **
    ***************************************************************************************************************/

    clickFolder : function(component, event, helper) {
        // console.log('@@@clickFolder Method@@@');
        let rcrdId   = event.target.id;
        let rcrdList = component.get("v.fil_fldrList");
        let rcrd     = rcrdList.filter(function(record) { 
            return record.id == rcrdId;
        });
        helper.recordList(component, rcrd[0].path_lower);
        let dir_List = component.get("v.dirList");
        let dir      = dir_List[dir_List.length-1];
        if(dir.id != rcrd[0].id) {
            helper.setDirectoryList(component, rcrd[0]);
        }
    },

    /***************************************************************************************************************
    **@Description: Download file on clicking of the file.                                                        **
    ***************************************************************************************************************/

    clickFile : function(component, event, helper) {
        // console.log('@@@clickFile Method@@@');
        let rcrdId   = event.target.id;
        let rcrdList = component.get("v.fil_fldrList");

        let rcrd = rcrdList.filter(function(record) { 
            return record.id == rcrdId;
        });
        if(rcrd[0].is_downloadable) {
            let action = component.get("c.DwnldFile");
            action.setParams({file_path: rcrd[0].path_lower});
            action.setCallback(this, function(response) {
                let status = response.getState();
                if(status === 'SUCCESS') {
                    let result = response.getReturnValue();
                    // console.log(result);
                    window.location.href = result;
                } else {
                    console.log('Error');
                }
            });
            $A.enqueueAction(action);
        } else {
            alert('File is not downloadable.');
        }
    },

    /***************************************************************************************************************
    **@Description: Change the folder on moving the directory.                                                    **
    ***************************************************************************************************************/

    chngDir : function(component, event, helper) {
        // console.log('@@@chngDir Method@@@');
        let dirId    = event.getSource().get('v.name');
        var dir_List = component.get("v.dirList");

        let dir = dir_List.filter(function(dir){
            return dir.id == dirId;
        });
        helper.recordList(component, dir[0].path_lower);

        let index = dir_List.findIndex(function(dir){
            return dir.id == dirId;
        });
        dir_List.splice(index+1, dir_List.length);
        component.set("v.dirList", dir_List);
    },

    /***************************************************************************************************************
    **@Description: Delete file/folder on clicking the file/folder.                                               **
    ***************************************************************************************************************/

    deleteFil_Fldr : function(component, event, helper) {
        // console.log('@@@deleteFil_Fldr Method@@@');
        let rcrdId   = event.target.id;
        let rcrdList = component.get("v.fil_fldrList");

        let rcrd = rcrdList.filter(function(record) { 
            return record.id == rcrdId;
        });
        let indx = rcrdList.findIndex(function(record) { 
            return record.id == rcrdId;
        });

        let action = component.get("c.DeleteFile");
        action.setParams({file_path: rcrd[0].path_lower});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result = response.getReturnValue();
                // console.log(result);
                if (result == 'Successfully Deleted') {
                    rcrdList.splice(indx, 1);
                    component.set("v.fil_fldrList", rcrdList);
                }
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    },

    /***************************************************************************************************************
    **@Description: Create new folder.                                                                            **
    ***************************************************************************************************************/

    folderCreate : function(component, event, helper) {
        let dir_List = component.get("v.dirList");
        let dir      = dir_List[dir_List.length-1];
        let name     = component.find("idCrtFldr").get('v.value');
        name         = (name == '') ? dir.path_lower+'/New Folder' : dir.path_lower+'/'+name;
        component.find("idCrtFldr").set('v.value', '');
        
        let action = component.get("c.CreateFolder");
        action.setParams ({folder_path: name});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result   = JSON.parse(response.getReturnValue());
                // console.log(result);
                let mtdata   = result.metadata;
                let rcrd     = {tag:'folder', path_lower:mtdata.path_lower, name:mtdata.name, is_downloadable:null, id:mtdata.id, isfile:false};
                let rcrdList = component.get("v.fil_fldrList");
                let index    = rcrdList.findIndex(function(obj){
                    return obj.tag != 'folder';
                });
                rcrdList.splice(index, 0, rcrd);
                component.set("v.fil_fldrList", rcrdList);
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    },

    /***************************************************************************************************************
    **@Description: Upload file.                                                                                  **
    ***************************************************************************************************************/

    uploadFile : function(component, event, helper) {
        var fileSize = 10000000;
        var file     = event.getSource().get("v.files")[0];
        var reader   = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function(e) {
            var fileBody   = reader.result;
            var base64Mark = 'base64,';
            var dataStart  = fileBody.indexOf(base64Mark) + base64Mark.length;  
            fileBody       = fileBody.substring(dataStart);
            if (fileBody.length > fileSize) {
                alert("File size is too large. File cannot be greater than 1MB.");
            } else {
                helper.fileUploadNew(component, fileBody, file.name);
            }
        }
    }
})