({

    /***************************************************************************************************************
    **@Description: Get list of files and folders in current directory(folder).                                   **
    ***************************************************************************************************************/

	recordList : function(component, fldrPath) {
        // console.log('@@@recordList Method@@@');
		let action = component.get("c.RecordsList");
		action.setParams({path: fldrPath});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result   = response.getReturnValue();
                // console.log(result);
                let rcrdList = JSON.parse(result).entries;
				this.setRecordList(component, rcrdList);
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    },
    
    /***************************************************************************************************************
    **@Description: Check type of record in recordList.                                                           **
    ***************************************************************************************************************/

	setRecordList : function(component, rcrdList) {
        // console.log('@@@setRecordList Method@@@');
        for(let a=0; a<rcrdList.length; a++) {
            let val = rcrdList[a].tag;
            rcrdList[a].isfile = (val == 'folder') ? false : true;
        }
        component.set("v.fil_fldrList", rcrdList);
    },
    
    /***************************************************************************************************************
    **@Description: Set directory list when folder is open.                                                       **
    ***************************************************************************************************************/
	
	setDirectoryList : function(component, rcrd) {
        // console.log('@@@setDirectoryList Method@@@');
        let dir_List = component.get("v.dirList");
        let dir      = {name: rcrd.name, id: rcrd.id, path_lower: rcrd.path_lower};
        dir_List.push(dir);
        component.set("v.dirList", dir_List);
    },

    /***************************************************************************************************************
    **@Description: Upload the file.                                                                              **
    ***************************************************************************************************************/

    fileUploadNew : function(component, body, name) {
        let dir_List = component.get("v.dirList");
        let dirPath  = dir_List[dir_List.length-1].path_lower;
        dirPath     += ('/'+name);

        let action = component.get("c.FileUpload");
        action.setParams ({fileBody: body, dirPath: dirPath});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result   = JSON.parse(response.getReturnValue());
                let rcrd     = {id:result.id, name:result.name, path_lower:result.path_lower, isfile:true, tag:'file', is_downloadable:result.is_downloadable};
                let rcrdList = component.get("v.fil_fldrList");
                rcrdList.push(rcrd);
                component.set("v.fil_fldrList", rcrdList);
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    }
})