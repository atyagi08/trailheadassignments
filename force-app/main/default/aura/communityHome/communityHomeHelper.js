({
    uploadProfile : function(component, body, name, type) {
        //console.log('@@@uploadProfile Method');
        let usrId  = component.get("v.userId");
        let action = component.get("c.SetProfile");
        action.setParams({fileBody :body, fileName :name, fileType :type, userId :usrId});        
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state == 'SUCCESS') {
                let result = response.getReturnValue();
                if(result == 'SUCCESS') {
                    this.refreshPhoto(component);
                } else {
                    console.log('ERROR');
                }
            } else {
                console.log('ERROR');
            }
        });        
        $A.enqueueAction(action);
    },
    
    refreshPhoto : function(component) {
        //console.log('@@@refreshPhoto Method');
        let usrId  = component.get("v.userId");        
        let action = component.get("c.ProfrileUrl");
        action.setParams({userId :usrId});        
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state == 'SUCCESS') {
                this.handleResponse(component, response);
            } else {
                console.log('ERROR');
            }
        });            
        $A.enqueueAction(action);
    },
    
    handleResponse : function (component, response) {
        //console.log('@@@handleResponse Method');
        var retVal   = response.getReturnValue();
        let photoUrl = JSON.parse(retVal).FullPhotoUrl;        
        if(photoUrl != component.get("v.photoUrl")) {
            component.set("v.photoUrl", photoUrl);
            var spinner = document.getElementById("idSpinner");
            $A.util.addClass(spinner, 'slds-hide');
        } else {
            window.setTimeout(function() {
                this.refreshPhoto(component);
            }, 5000);
        }
    },
    
})