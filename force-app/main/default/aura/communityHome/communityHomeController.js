({
    doInit : function(component, event, helper) {
        let action = component.get("c.GetImage");        
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state == 'SUCCESS') {
                let result 	 = response.getReturnValue();
                let name	 = JSON.parse(result).Name;
                let usrName	 = JSON.parse(result).Username;
                let recordId = JSON.parse(result).Id;                
                let photoUrl = JSON.parse(result).FullPhotoUrl;
                component.set("v.name", name);
                component.set("v.userId", recordId);
                component.set("v.userName", usrName);
                component.set("v.photoUrl", photoUrl);
                //console.log('@@@Record Id ==>> ' + recordId);
                //console.log('@@@photoUrl ==>> ' + photoUrl);
            } else {
                console.log('error');
            }
        });
        $A.enqueueAction(action);
    },
    
    upldImg : function(component, event, helper) {
        var element1 = document.getElementById("idProfile");
        $A.util.removeClass(element1, 'styleInline');
        $A.util.addClass(element1, 'styleNone');
        
        var element2 = document.getElementById("idUpload");
        $A.util.removeClass(element2, 'styleNone');
        $A.util.addClass(element2, 'styleInline');
    },
    
    prflImg : function(component, event, helper) {
        var element1 = document.getElementById("idProfile");
        $A.util.removeClass(element1, 'styleNone');
        $A.util.addClass(element1, 'styleInline');
        
        var element2 = document.getElementById("idUpload");
        $A.util.removeClass(element2, 'styleInline');
        $A.util.addClass(element2, 'styleNone');
    },
    
    upldPrfl : function(component, event, helper) {
        var spinner = document.getElementById("idSpinner");
        $A.util.removeClass(spinner, 'slds-hide');
        
        var maxStringSize = 1000000;
        var file = document.getElementById("file-input").files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function(e) {
            var attachmentbody = reader.result;
            if (attachmentbody.length > maxStringSize) {
                alert("File size is too large. File cannot be greater than 1MB.");
            } else {
                helper.uploadProfile(component, attachmentbody, file.name, file.type);
            }
        }    
    },
})