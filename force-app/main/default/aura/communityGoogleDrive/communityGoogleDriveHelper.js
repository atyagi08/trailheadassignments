({

    /***************************************************************************************************************
    **@Description: Get list of files and folders in current directory(folder).                                   **
    ***************************************************************************************************************/

    recordList : function(component) {
        let action = component.get("c.RecordsList");
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result = response.getReturnValue();
                let rcrdList = JSON.parse(result).files;
                this.setRecordList(component, rcrdList);
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    },

    /***************************************************************************************************************
    **@Description: Check type of record in recordList.                                                           **
    ***************************************************************************************************************/

    setRecordList : function(component, rcrdList) {
        for (let a=0; a<rcrdList.length; a++) {
            let val = rcrdList[a].mimeType;
            rcrdList[a].isfile = (val == 'application/vnd.google-apps.folder') ? false : true;
        }
        component.set("v.fil_fldrList", rcrdList);
        console.log(component.get("v.fil_fldrList"));
    },

    /***************************************************************************************************************
    **@Description: Set directory list when folder is open.                                                       **
    ***************************************************************************************************************/

    setDirectoryList : function(component, rcrd) {
        let dir_List = component.get("v.dirList");
        let dir = {name: rcrd.name, id: rcrd.id};
        dir_List.push(dir);
        component.set("v.dirList", dir_List);
    },

    /***************************************************************************************************************
    **@Description: Upload the file.                                                                              **
    ***************************************************************************************************************/

    fileUploadNew : function(component, body, name, type) {
        let dir_List = component.get("v.dirList");
        let rcrdList = component.get("v.fil_fldrList");
        let dir_Id   = dir_List[dir_List.length-1].id;

        let action = component.get("c.FileUpload");
        action.setParams ({fileName: name, fileBody: body, fileType: type, dirId: dir_Id});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result = JSON.parse(response.getReturnValue());
                console.log(result);
                let rcrd   = {id: result.id, name: result.name, mimeType: result.mimeType, isfile: true};
                rcrdList.push(rcrd);
                component.set("v.fil_fldrList", rcrdList);
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    }
})