({

    /***************************************************************************************************************
    **@Description: Generate access token and fetch list of files and folders.                                    **
    ***************************************************************************************************************/

    doInit : function(component, event, helper) {
        let url = new URL(window.location.href);
        let cod = url.searchParams.get("code");

        if(cod == null) {
            let action = component.get("c.getAccessToken");
            action.setCallback(this, function(response) {
                let status = response.getState();
                if(status === 'SUCCESS') {
                    let result = response.getReturnValue();
                    if(result.includes('redirect_uri')) {
                        window.location.href = result;
                    }
                    helper.recordList(component);
                } else {
                    console.log('Error');
                }
            });
            $A.enqueueAction(action);
        } else {
            let action = component.get("c.AuthorizeUser");
            action.setParams({code: cod});
            action.setCallback(this, function(response) {
                let status = response.getState();
                if(status === 'SUCCESS') {
                    helper.recordList(component);
                } else {
                    console.log('Error');
                }
            });
            $A.enqueueAction(action);
        }

        let dir_list = [];
        let dir = {name:"Home", id:"root"};
        dir_list[0] = dir;
        console.log(dir_list);
        component.set("v.dirList", dir_list);
    },

    /***************************************************************************************************************
    **@Description: Open folder on clicking the folder and download file on clicking the file.                    **
    ***************************************************************************************************************/

    clickFilFldr : function(component, event, helper) {
        let rcrdId   = event.target.id;
        let rcrdList = component.get("v.fil_fldrList");
        let rcrd     = rcrdList.filter(function(record) { 
            return record.id == rcrdId;
        });

        let action = component.get("c.ClickOnFileFolder");
        action.setParams({fileId: rcrd[0].id, fileType: rcrd[0].mimeType});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result = JSON.parse(response.getReturnValue());
                // console.log(result);
                if( result.files != null) {
                    let rcrdList = result.files;
                    helper.setRecordList(component, rcrdList);
                    let dir_List = component.get("v.dirList");
                    let dir      = dir_List[dir_List.length-1];
                    if(dir.id != rcrd[0].id) {
                        helper.setDirectoryList(component, rcrd[0]);
                    }
                } else if(result.files == null && result.webContentLink != null) {
                    window.location.href = result.webContentLink;
                } else {
                    alert('File is not downloadable');
                }
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    },

    /***************************************************************************************************************
    **@Description: Delete file/folder on clicking the file/folder.                                               **
    ***************************************************************************************************************/

    deleteFil_Fldr : function(component, event, helper) {
        let rcrdId   = event.target.id;
        let rcrdList = component.get("v.fil_fldrList");
        let index    = rcrdList.findIndex(function(obj) {
            return obj.id == rcrdId;
        });

        var action = component.get("c.DeleteFile");
        action.setParams({fileId: rcrdId});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result = response.getReturnValue();
                if(result == 'Successfully Deleted') {
                    rcrdList.splice(index, 1);
                    component.set("v.fil_fldrList", rcrdList);
                }
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    },

    /***************************************************************************************************************
    **@Description: Change the folder on moving the directory.                                                    **
    ***************************************************************************************************************/

    chngDir : function (component, event, helper) {
        var dirId    = event.getSource().get('v.name');
        var dir_List = component.get("v.dirList");
        let index    = dir_List.findIndex(function(dir){
            return dir.id == dirId;
        });

        let action = component.get("c.ClickOnFileFolder");
        action.setParams({fileId: dirId, fileType: 'application/vnd.google-apps.folder'});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result = JSON.parse(response.getReturnValue());
                // console.log(result);
                let rcrdList = result.files;
                helper.setRecordList(component, rcrdList);
                dir_List.splice(index+1, dir_List.length);
                component.set("v.dirList", dir_List);
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
        
    },

    /***************************************************************************************************************
    **@Description: Upload file.                                                                                  **
    ***************************************************************************************************************/

    uploadFile : function(component, event, helper) {
        var fileSize = 10000000;
        var file     = event.getSource().get("v.files")[0];
        var reader   = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function(e) {
            var fileBody   = reader.result;
            var base64Mark = 'base64,';
            var dataStart  = fileBody.indexOf(base64Mark) + base64Mark.length;
            fileBody       = fileBody.substring(dataStart);
            if (fileBody.length > fileSize) {
                alert("File size is too large. File cannot be greater than 1MB.");
            } else {
                console.log(file.name + '   ' + file.type);
                helper.fileUploadNew(component, fileBody, file.name, file.type);
            }
        }    
    },

    /***************************************************************************************************************
    **@Description: Create new folder.                                                                            **
    ***************************************************************************************************************/

    folderCreate : function(component, event, helper) {
        let dir_List = component.get("v.dirList");
        let rcrdList = component.get("v.fil_fldrList");
        let dir_Id   = dir_List[dir_List.length-1].id;
        let name = component.find("idCrtFldr").get('v.value');
        name = (name == '') ? 'New Folder' : name;
        component.find("idCrtFldr").set('v.value', '');
        
        let action = component.get("c.CreateFolder");
        action.setParams ({folderName: name, dirId: dir_Id});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result = JSON.parse(response.getReturnValue());
                // console.log(result);
                let rcrd   = {id: result.id, name: result.name, mimeType: result.mimeType, isfile: false};
                let index    = rcrdList.findIndex(function(obj){
                    return obj.mimeType != result.mimeType;
                });
                rcrdList.splice(index, 0, rcrd);
                component.set("v.fil_fldrList", rcrdList);
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    }
})