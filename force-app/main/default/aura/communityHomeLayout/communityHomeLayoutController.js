({
    doInit : function(component, event, helper) {
        let tab = component.find("idTabset");
        let url = new URL(window.location.href);
        let scp = url.searchParams.get("state");
        if(scp != null && scp.includes('google')) {
            tab.set("v.selectedTabId", 'gDrive');
        } else if(scp != null && scp.includes('dropbox')) {
            tab.set("v.selectedTabId", 'dropbox');
        } else if(scp != null && scp.includes('box')) {
            tab.set("v.selectedTabId", 'box');
        } else {
            tab.set("v.selectedTabId", 'home');
        }
    },

    logout : function (component, event, helper) {
        window.location.replace("https://brisktrailcomm-developer-edition.ap15.force.com/secur/logout.jsp?retUrl=https://brisktrailcomm-developer-edition.ap15.force.com/custom/s%2Flogin");  
    }
})