({

    /***************************************************************************************************************
    **@Description: Get list of files and folders in current directory(folder).                                   **
    ***************************************************************************************************************/

	recordList : function(component, fldrId) {
		// console.log('@@@recordList Method@@@');
		let action = component.get("c.RecordsList");
		action.setParams({folderId: fldrId});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result   = response.getReturnValue();
				let rcrdList = JSON.parse(result).entries;
				// console.log(rcrdList);
				this.setRecordList(component, rcrdList);
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    },
    
    /***************************************************************************************************************
    **@Description: Check type of record in recordList.                                                           **
    ***************************************************************************************************************/

	setRecordList : function(component, rcrdList) {
        // console.log('@@@setRecordList Method@@@');
        for(let a=0; a<rcrdList.length; a++) {
            let val = rcrdList[a].type;
            rcrdList[a].isfile = (val == 'folder') ? false : true;
        }
        component.set("v.fil_fldrList", rcrdList);
	},

    /***************************************************************************************************************
    **@Description: Set directory list when folder is open.                                                       **
    ***************************************************************************************************************/

	setDirectoryList : function(component, rcrd) {
        // console.log('@@@setDirectoryList Method@@@');
        let dir_List = component.get("v.dirList");
        let dir      = {name: rcrd.name, id: rcrd.id};
        dir_List.push(dir);
        component.set("v.dirList", dir_List);
    },

    /***************************************************************************************************************
    **@Description: Upload the file.                                                                              **
    ***************************************************************************************************************/

    fileUploadNew : function(component, body, name) {
        // console.log('@@@fileUploadNew Method@@@');
        let dir_List = component.get("v.dirList");
        let dirId  = dir_List[dir_List.length-1].id;

        let action = component.get("c.FileUpload");
        action.setParams ({fileBody: body, fileName: name, folderId: dirId});
        action.setCallback(this, function(response) {
            let status = response.getState();
            if(status === 'SUCCESS') {
                let result   = JSON.parse(response.getReturnValue());
                // console.log(result);
                let entry       = result.entries;
                let rcrd     = {id: entry[0].id, name: entry[0].name, isfile:true, type:'file'};
                let rcrdList = component.get("v.fil_fldrList");
                rcrdList.push(rcrd);
                component.set("v.fil_fldrList", rcrdList);
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    }
})