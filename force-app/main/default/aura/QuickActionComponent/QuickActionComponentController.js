({
    doInit : function(component, event, helper) {
        console.log( "@@@@ doInit QuickActionComponent Called..." );

        var pageReference = component.get( "v.pageReference" );
        if( !pageReference ){
            var pageReference = {
                "type"       : "standard__component",
                "attributes" : { componentName : "c__QuickActionComponent" },
                "state"      : { c__recordId : component.get( "v.recordId" ) }
            };
            component.find( "navService" ).generateUrl( pageReference )
            .then( $A.getCallback( function( url ) {
                var element = document.createElement('a');
                element.setAttribute( 'href', url );
                element.style.display = 'none';
                document.body.appendChild( element );
                element.click();
                document.body.removeChild( element );
            }), $A.getCallback( function( error ) {
                console.log( '@@@@ Error... @@@@' );
            }));
        } else {
            // component.set( "v.showLWC", true );
            component.set( "v.rcrdId", pageReference.state.c__recordId );
        }

    }
})