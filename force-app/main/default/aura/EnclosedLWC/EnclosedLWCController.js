({
    doInit : function( component, event, helper ) {
        console.log( "@@@@ doInit EnclosedLWC Called..." );
        var pageReference = component.get( "v.pageReference" );
        component.set( "v.recordId", pageReference.state.c__recordId );
    }
})