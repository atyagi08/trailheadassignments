({

    /***************************************************************************************************************
    **Description: Method on initialization to fetch the records                                                                    **
    ***************************************************************************************************************/

    doInit : function(component, event, helper) {
        component.set("v.offset", 0);
        component.set("v.pageNumber", 1);
        component.set("v.recordId", '');
        component.set("v.fieldValue", '');
        component.set("v.buttonLabel", "First");
        helper.fetchRecord(component, 0);
    },

    /***************************************************************************************************************
    **Description: Method to fire event for creating new record                                                   **
    ***************************************************************************************************************/

    createRecord : function (component, event, helper) {
        console.log('@@@In Create Record Method');
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Account"
        });
        createRecordEvent.fire();
    },

    /***************************************************************************************************************
    **Description: Method to fire event for editing the selected record                                           **
    ***************************************************************************************************************/

    editRecord : function (component, event, helper) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": event.target.id
        });
        editRecordEvent.fire();
    },

    /***************************************************************************************************************
    **Description: Method on pagination buttons                                                                   **
    ***************************************************************************************************************/

    pageTraverse : function(component, event, helper) {
        console.log('@@@In pageTraverse function ==>>');
        var offset      = component.get("v.offset");
        var totRec      = component.get("v.totalAccounts");
        var accList     = component.get("v.accountList");
        var pageSize    = component.get('v.crntPageSize');
        var pageNumb    = component.get("v.pageNumber");
        var recordId    = component.get("v.recordId");
        var sortField   = component.get("v.sortField");
        var fieldValue  = component.get("v.fieldValue");
        var buttonLabel = component.get("v.buttonLabel");

        if (event.getSource().get("v.label") == 'First') {                                  //First Button
            offset      = 0;
            pageNumb    = 1;
            recordId    = '';
            fieldValue  = '';
            buttonLabel =  "First";
            // console.log('@@@firstPage Offset === ' + component.get("v.offset"));
        } else if (event.getSource().get("v.label") == 'Previous') {                        //Previous Button
            --pageNumb;
            offset      = Number(offset) - Number(pageSize);
            recordId    = accList[0]['Id'];
            fieldValue  = accList[0][sortField];
            buttonLabel =  "Previous";
            // console.log('@@@previousPage Offset === ' + component.get("v.offset"));
        } else if (event.getSource().get("v.label") == 'Next') {                            //Next Button
            ++pageNumb;
            offset      = Number(offset) + Number(pageSize);
            recordId    = accList[Number(pageSize)-1]['Id'];
            fieldValue  = accList[Number(pageSize)-1][sortField];
            buttonLabel =  "Next";
            // console.log('@@@nextPage Offset === ' + component.get("v.offset"));
        } else if (event.getSource().get("v.label") == 'Last') {                            //Last Button
            offset      = (totRec % pageSize == 0) ? (Number(totRec) - Number(pageSize)) : (parseInt(Number(totRec) / Number(pageSize)) * Number(pageSize));
            pageNumb    = (Number(totRec) % Number(pageSize) == 0) ? (Number(totRec) / Number(pageSize)) : (parseInt(Number(totRec) / Number(pageSize)) + 1);
            recordId    = '';
            fieldValue  = '';
            buttonLabel =  "Last";
            // console.log('@@@lastPage Offset === ' + component.get("v.offset"));
        }

        component.set("v.offset", offset);
        component.set("v.pageNumber", pageNumb);
        component.set("v.recordId", recordId);
        component.set("v.fieldValue", fieldValue);
        component.set("v.buttonLabel", buttonLabel);
        helper.fetchRecord(component, offset);
    },

    /***************************************************************************************************************
    **Description: Method to show confirm modal for asking delete confirmation                                    **
    ***************************************************************************************************************/

    handleConfirmDialog : function(component, event, helper) {
        component.set('v.confirmModal', true);
        var deleteList = [];
        (event.target.getAttribute("data-id") == 'a-tag') ? deleteList.push(event.target.id) : deleteList = component.get("v.hardListCheck");
        component.set("v.deleteIdList", deleteList);
        // console.log(component.get("v.deleteIdList"));
    },

    /***************************************************************************************************************
    **Description: Method to modal buttons                                                                        **
    ***************************************************************************************************************/
    
    handleConfirmDialogAction : function(component, event, helper) {
        component.set('v.confirmModal', false);
        var deleteList = component.get("v.deleteIdList");
        if (event.getSource().get("v.label") == 'Delete') {                                 //On clicking delete button
            helper.deleteRecord(component, deleteList);
        }
        deleteList = [];
        component.set("v.deleteIdList", deleteList);
    },

    /***************************************************************************************************************
    **Description: Sort Method(With OFFSET)                                                                       **
    ***************************************************************************************************************/

    sortTable : function(component, event, helper) {
        component.set("v.offset", 0);
        component.set("v.pageNumber", 1);
        var fieldName =  event.currentTarget.id;
        var order = (component.get("v.sortField") != fieldName) ? 'ASC' : (component.get("v.sortOrder") == 'ASC') ? 'DESC' : 'ASC';
        component.set("v.sortField", fieldName);
        component.set("v.sortOrder", order);
        component.set("v.recordId", '');
        component.set("v.fieldValue", '');
        component.set("v.buttonLabel", "First");
        if (component.get("v.sortOrder") == 'ASC') {
            var element1 = component.find(fieldName+" Up");
            $A.util.removeClass(element1, 'styleNone');
            $A.util.addClass(element1, 'styleInline');
            var element2 = component.find(fieldName+" Dn");
            $A.util.removeClass(element2, 'styleInline');
            $A.util.addClass(element2, 'styleNone');
        } else {
            var element1 = component.find(fieldName+" Up");
            $A.util.removeClass(element1, 'styleInline');
            $A.util.addClass(element1, 'styleNone');
            var element2 = component.find(fieldName+" Dn");
            $A.util.removeClass(element2, 'styleNone');
            $A.util.addClass(element2, 'styleInline');
        }
        helper.fetchRecord(component, component.get("v.offset"));
    },

    /***************************************************************************************************************
    **Description: Method to fire on selecting/deselecting checkbox of a single record                            **
    ***************************************************************************************************************/

    checkboxState : function(component, event, helper) {
        let recordId     =  event.getSource().get("v.name");
        let checkboxList = component.get("v.hardListCheck");
        let found        = checkboxList.find(element => element == recordId);
        (found == undefined) ? checkboxList.push(recordId) : checkboxList.splice(checkboxList.indexOf(recordId), 1);
        component.set("v.hardListCheck", checkboxList);
        helper.headerCheck(component, component.get("v.accountList"), checkboxList);
        // console.log('@@@hardListCheck 5 ==>> ', component.get("v.hardListCheck"));
    },

    /***************************************************************************************************************
    **Description: Method to fire on selecting/deselecting header checkbox                                        **
    ***************************************************************************************************************/
    
    headerCheckboxState : function(component, event, helper) {
        let accList      = component.get("v.accountList");
        let checkboxList = component.get("v.hardListCheck");
        if (component.get("v.headerChecked") == true) {                                     //Selecting all records
            for (let i=0; i<accList.length; i++) {
                let found = checkboxList.find(element => element == accList[i].Id);
                if (found == undefined) {
                    checkboxList.push(accList[i].Id);
                    accList[i].ischecked = true;
                }
            }
        } else {                                                                            //Deselecting all records
            for (let i=0; i<accList.length; i++) {
                checkboxList.splice(checkboxList.indexOf(accList[i].Id), 1)
                accList[i].ischecked = false;
            }
        }
        component.set("v.accountList", accList);
        component.set("v.hardListCheck", checkboxList);
    },
    
})