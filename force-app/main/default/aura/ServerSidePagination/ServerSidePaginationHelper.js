({

    /***************************************************************************************************************
    **Description: Fetch records from Apex Controller                                                             **
    **                                                                                                            **
    **Parameter component: Component                                                                              **
    **Parameter start: Offset for disabling/enabling buttons                                                      **
    **Parameter idRecord: Id of the record which will to used to fetch the previous/next page records.            **
    **Parameter sign: Greater than or less than symbol                                                            **
    **                                                                                                            **
    ***************************************************************************************************************/

    fetchRecord : function(component, start) {
        var paramObject = {};
        paramObject.order       = component.get("v.sortOrder");
        paramObject.pageSize    = component.get("v.crntPageSize");
        paramObject.recordId    = component.get("v.recordId");
        paramObject.fieldName   = component.get("v.sortField");
        paramObject.fieldValue  = component.get("v.fieldValue");
        paramObject.objectName  = component.get("v.objectName");
        paramObject.buttonLabel = component.get("v.buttonLabel");
        // console.log(paramObject);

        var action   = component.get("c.getAccounts");
        action.setParams({paramString : JSON.stringify(paramObject)});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                let result = response.getReturnValue();
                let accList = JSON.parse(result).accList;
                let checkboxList = component.get("v.hardListCheck");
                // console.log('Result ',JSON.parse(result));
                for (let a=0; a<accList.length; a++) {
                    let found = checkboxList.find(element => element == accList[a].Id);
                    accList[a].ischecked = (found == undefined) ? false : true;
                }
                this.headerCheck(component, accList, checkboxList);
                component.set("v.accountList", accList);
                component.set("v.totalAccounts", JSON.parse(result).totalAccount);
                var totRec   = component.get("v.totalAccounts");
                var pageSize = component.get("v.crntPageSize");
                var totPages = (Number(totRec) % Number(pageSize) == 0) ? (Number(totRec) / Number(pageSize)) : (parseInt(Number(totRec) / Number(pageSize)) + 1);
                component.set("v.totalPages", totPages);
                this.buttonState(component, start, pageSize, totRec);
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });

        $A.enqueueAction(action);
    },

    /***************************************************************************************************************
    **Description: Maintaining pagination button states                                                           **
    **                                                                                                            **
    **Parameter component: Component                                                                              **
    **Parameter offset: Offset for disabling/enabling buttons                                                     **
    **Parameter pageSize: Current page size                                                                       **
    **Parameter totalRec: Total records                                                                           **
    **                                                                                                            **
    ***************************************************************************************************************/
    
    buttonState : function(component, offset, pageSize, totalRec) {
        if (offset == 0 && totalRec <= pageSize) {                                //When total records are less than page size
            component.set("v.buttonStateNxt", true);
            component.set("v.buttonStatePre", true);
        } else if ((offset == 0) && !(totalRec <= pageSize)) {                      //When total records are greater than page size
            component.set("v.buttonStateNxt", false);
            component.set("v.buttonStatePre", true);
        } else if ((Number(offset) + Number(pageSize)) >= Number(totalRec)) {       //Last page
            component.set("v.buttonStateNxt", true);
            component.set("v.buttonStatePre", false);
        } else {                                                                   //In middle pages
            component.set("v.buttonStateNxt", false);
            component.set("v.buttonStatePre", false);
        }
    },

    /***************************************************************************************************************
    **Description: Deleting single or selected records                                                            **
    **                                                                                                            **
    **Parameter component: Component                                                                              **
    **Parameter idList: List of id of records which will be deleted                                               **
    **                                                                                                            **
    ***************************************************************************************************************/

    deleteRecord : function(component, idList) {
            if (idList.length == 0) {
                alert('No Records Selected');
            } else {
                var action   = component.get("c.deleteAccount");
                action.setParams({idDelete : idList});
    
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log('Success ==>>');
                        this.fetchRecord(component, component.get("v.offset"));
                    } else {
                        console.log('Problem getting account, response state: ' + state);
                    }
                });
                
                $A.enqueueAction(action);
            }
    },

    /***************************************************************************************************************
    **Description: Maintaining header checkbox state                                                              **
    **                                                                                                            **
    **Parameter component: Component                                                                              **
    **Parameter accList: List of current records                                                                  **
    **Parameter checkboxList: List of records currently selected                                                  **
    **                                                                                                            **
    ***************************************************************************************************************/

    headerCheck : function(component, accList, checkboxList) {
        let count = 0;
        for (let a=0; a<accList.length; a++) {
            let found = checkboxList.find(element => element == accList[a].Id);
            (found == undefined) ? undefined : ++count;
        }
        (count == accList.length) ? component.set("v.headerChecked", true): component.set("v.headerChecked", false);
    },

})